dtmc

const k;
const n=10;

module die

	// local state
	s : [1..n+2] init 1;
	
	[] s<n -> 0.7999 : (s'=s+1) + 0.0001 : (s'=n+1)+ 0.2 : (s'=1);
        [] s=n -> 0.7999 : (s'=n+2) + 0.0001: (s'=n+1)+ 0.2: (s'=1);
	[] s>n -> (s'=s);
	
endmodule
