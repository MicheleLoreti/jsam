/*
 * Michele Loreti, Concurrency and Mobility Group
 * Universitą di Firenze, Italy
 * (C) Copyright 2013.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti
 */
package org.cmg.ml.sam.core.logic;


/**
 * @author loreti
 *
 */
public class Until<T> extends PathFormula<T> {

	public StateFormula<T> left;	
	public StateFormula<T> right;
	public Number limit;
	
	public Until( StateFormula<T> left, StateFormula<T> right) {
		this(left,null,right);
	}

	public Until(StateFormula<T> left, Number limit, StateFormula<T> right) {
		this.left = left;
		this.limit=limit;
		this.right = right;
	}

	@Override
	public <R> R accept(PathFormulaVisitor<T, R> v, T s) {
		return v.visitUntil(this,s);
	}

	@Override
	protected int _hashCode() {
		return 3*(left.hashCode()^right.hashCode());
	}
	
	@Override
	protected boolean _equals(PathFormula<?> path) {
		if (path instanceof Until<?>) {
			boolean toReturn = true;
			Until<?> until = (Until<?>) path;
			if (limit == null) {
				toReturn = (until.limit == null);
			} else {
				toReturn = limit.equals(until.limit);
			}
			return toReturn&&left._equals(((Until<?>) path).left)&&
					right._equals(((Until<?>) path).right);
		}
 		return false;
	}

	public Number getBound() {
		return limit;
	}

	public StateFormula<T> getLeft() {
		return left;
	}

	public StateFormula<T> getRight() {
		return right;
	}
		
}
