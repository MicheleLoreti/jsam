/**
 * 
 */
package org.cmg.ml.sam.carma;

/**
 * @author loreti
 *
 */
public interface CarmaPredicateEvaluation {

	public CarmaPredicate eval( CarmaStore localStore , Object ... values );
	
}
