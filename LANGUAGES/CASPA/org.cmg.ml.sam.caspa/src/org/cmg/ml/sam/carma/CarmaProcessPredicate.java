/**
 * 
 */
package org.cmg.ml.sam.carma;


/**
 * @author loreti
 *
 */
public interface CarmaProcessPredicate {

	public boolean eval( CarmaProcess p );
	
}
