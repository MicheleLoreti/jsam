/**
 * 
 */
package org.cmg.ml.sam.carma;

/**
 * @author loreti
 *
 */
public interface ComponentPredicate {

	public boolean eval(CarmaComponent c);
	
}
