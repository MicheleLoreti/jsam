package org.cmg.ml.sam.xtext.klaim.tests;

import com.google.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import org.cmg.ml.sam.core.sim.Measure;
import org.cmg.ml.sam.klaim.core.Net;
import org.cmg.ml.sam.klaim.sim.KlaimSimulator;
import org.cmg.ml.sam.xtext.klaim.KlaimInjectorProvider;
import org.cmg.ml.sam.xtext.klaim.klaim.Model;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(KlaimInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class TestParser {
  private static String classOutputFolder = "/tmp";
  
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Test
  public void testIsDeadlock() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("node test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Process() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("in( \"TEST\" )@self:1.0;\t");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("net:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("test::Process()");
      _builder.newLine();
      _builder.newLine();
      Model m = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(m);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testWhileTrueOut() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("node test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Process() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("while (true) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("out( \"TEST\" )@self:1.0;\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("formula testFormula = true;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("net:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("test::Process()");
      _builder.newLine();
      _builder.newLine();
      Model m = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(m);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testWhileTrueIn() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("node test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Process() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("int x = 0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("while (true) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("in( ?x )@self:1.0;\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("out( x+1 )@self:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("net:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("test::Process()||test::[ 0 ]");
      _builder.newLine();
      _builder.newLine();
      Model m = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(m);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testWhileConInOut() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("node test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Process() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("int x = 0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("while (x<2) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("in( ?x )@self:1.0;\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("out( x+1 )@self:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("net:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("test::Process()||test::[ 0 ]");
      _builder.newLine();
      _builder.newLine();
      Model m = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(m);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testPingPong() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("node ping;");
      _builder.newLine();
      _builder.append("node pong;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Ping() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("out( 0 )@pong:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("in( 1 )@self:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Ping();");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Pong() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("in( 0 )@self:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("out( 1 )@ping:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Pong();");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("net:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("ping::Ping() || pong::Pong()");
      _builder.newLine();
      _builder.newLine();
      Model m = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(m);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testAsFarAsItCan() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("logical next;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("node rg;");
      _builder.newLine();
      _builder.append("node n1 environment { next -> n2 };");
      _builder.newLine();
      _builder.append("node n2 environment { next -> n3 };");
      _builder.newLine();
      _builder.append("node n3 environment { next -> n1 };");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Participant() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("int id;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("in( ?id )@rg:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("out( id )@next:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("int received = id+1;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("while ( received > id ) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("in( ?received )@self:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("if (received<id) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("out( received )@next:1.0;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("out( \"FOLLOWER\" )@self:1.0;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Follower();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("} else {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("out( \"LEADER\" )@rg:1.0;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Remover();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Follower() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("int x;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("while (true) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("in( ?x )@self:1.0;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("out( x )@next:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process Remover() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("int x;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("while (true) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("in( ?x )@self:1.0;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}\t");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("measure leader = select t=[ \"LEADER\" ]@* in t;");
      _builder.newLine();
      _builder.append("measure follower = select t=[ \"FOLLOWER\" ]@* in t;");
      _builder.newLine();
      _builder.append("measure total = select t_follower=[ \"FOLLOWER\" ]@* ");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("and t_leader=[ \"LEADER\" ]@* in t_follower+t_leader;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("formula test1 = true;");
      _builder.newLine();
      _builder.append("formula test2 = false;");
      _builder.newLine();
      _builder.append("formula test3 = [ 1 ]@rg -> true;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("net:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rg::[ 1 ] || rg::[ 2 ] || rg::[ 3 ]");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("|| n1::Participant() || n2::Participant() || n3::Participant()");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.newLine();
      Model m = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(m);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCodeGeneration() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("node test;");
      _builder.newLine();
      _builder.append("node testbis;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("logical atesr;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("rate pippo = 0.35;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("function bool testFunction( bool x , int s , locality l ) {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("if (x) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("s = s+3;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("} \t\t\t\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("return x;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("process testProcess( ) {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("out( 2+3 )@self:pippo;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("eval( testProcess( ) )@self:pippo;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("net: test::[ 1 ]");
      _builder.newLine();
      Model m = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(m);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public void simulate(final Net start, final double limit) {
    HashMap<String, Measure<Net>> _hashMap = new HashMap<String, Measure<Net>>();
    this.simulate(_hashMap, start, limit);
  }
  
  public void simulate(final Map<String, Measure<Net>> measures, final Net start, final double limit) {
    KlaimSimulator simulator = new KlaimSimulator(start, measures);
    Map<Double, HashMap<String, Double>> data = simulator.simulate(100, 0.1, limit);
    double t = 0.0;
    do {
      {
        InputOutput.<String>print((("TIME " + Double.valueOf(t)) + "\n"));
        HashMap<String, Double> _get = data.get(Double.valueOf(t));
        String _plus = (_get + "\n");
        InputOutput.<String>print(_plus);
        t = (t + 0.1);
      }
    } while((t < limit));
  }
}
