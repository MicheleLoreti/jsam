package org.cmg.ml.sam.xtext.klaim.validation;

import java.util.Arrays;
import org.cmg.ml.sam.xtext.klaim.klaim.ACosExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.ASinExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.ATanExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.Call;
import org.cmg.ml.sam.xtext.klaim.klaim.CeilExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.Conjunction;
import org.cmg.ml.sam.xtext.klaim.klaim.ConstantDeclaration;
import org.cmg.ml.sam.xtext.klaim.klaim.CosExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.Disjunction;
import org.cmg.ml.sam.xtext.klaim.klaim.Expression;
import org.cmg.ml.sam.xtext.klaim.klaim.FalseExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.FloorExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.FunctionDeclaration;
import org.cmg.ml.sam.xtext.klaim.klaim.LogExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.LogicalLocality;
import org.cmg.ml.sam.xtext.klaim.klaim.MaxExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.MinExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.ModExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.MultiplicationDivision;
import org.cmg.ml.sam.xtext.klaim.klaim.Negation;
import org.cmg.ml.sam.xtext.klaim.klaim.NodeDeclaration;
import org.cmg.ml.sam.xtext.klaim.klaim.NumberLiteral;
import org.cmg.ml.sam.xtext.klaim.klaim.PowExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.ProcessDeclaration;
import org.cmg.ml.sam.xtext.klaim.klaim.Reference;
import org.cmg.ml.sam.xtext.klaim.klaim.ReferenceableElements;
import org.cmg.ml.sam.xtext.klaim.klaim.Relation;
import org.cmg.ml.sam.xtext.klaim.klaim.SelfExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.SinExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.StringLiteral;
import org.cmg.ml.sam.xtext.klaim.klaim.SummationSubtraction;
import org.cmg.ml.sam.xtext.klaim.klaim.TanExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.TrueExpression;
import org.cmg.ml.sam.xtext.klaim.klaim.TupleSelection;
import org.cmg.ml.sam.xtext.klaim.klaim.Type;
import org.cmg.ml.sam.xtext.klaim.klaim.Variable;
import org.cmg.ml.sam.xtext.klaim.validation.KlaimType;

@SuppressWarnings("all")
public class TypeInference {
  protected KlaimType _getType(final Expression e) {
    return KlaimType.VOID;
  }
  
  protected KlaimType _getType(final Disjunction d) {
    return KlaimType.BOOLEAN;
  }
  
  protected KlaimType _getType(final Conjunction c) {
    return KlaimType.BOOLEAN;
  }
  
  protected KlaimType _getType(final Negation n) {
    return KlaimType.BOOLEAN;
  }
  
  protected KlaimType _getType(final Relation r) {
    return KlaimType.BOOLEAN;
  }
  
  protected KlaimType _getType(final TrueExpression e) {
    return KlaimType.BOOLEAN;
  }
  
  protected KlaimType _getType(final SummationSubtraction e) {
    Expression _left = e.getLeft();
    KlaimType _type = this.getType(_left);
    Expression _right = e.getRight();
    KlaimType _type_1 = this.getType(_right);
    return this.getMax(_type, _type_1);
  }
  
  protected KlaimType _getType(final MultiplicationDivision m) {
    return KlaimType.ERROR;
  }
  
  protected KlaimType _getType(final FalseExpression e) {
    return KlaimType.BOOLEAN;
  }
  
  protected KlaimType _getType(final NumberLiteral n) {
    KlaimType _xifexpression = null;
    boolean _isIsDouble = n.isIsDouble();
    if (_isIsDouble) {
      _xifexpression = KlaimType.DOUBLE;
    } else {
      _xifexpression = KlaimType.INTEGER;
    }
    return _xifexpression;
  }
  
  protected KlaimType _getType(final StringLiteral s) {
    return KlaimType.STRING;
  }
  
  protected KlaimType _getType(final SelfExpression s) {
    return KlaimType.LOCALITY;
  }
  
  protected KlaimType _getType(final MinExpression m) {
    return KlaimType.ERROR;
  }
  
  protected KlaimType _getType(final MaxExpression m) {
    return KlaimType.ERROR;
  }
  
  protected KlaimType _getType(final ModExpression m) {
    return KlaimType.INTEGER;
  }
  
  protected KlaimType _getType(final LogExpression m) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final SinExpression m) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final CosExpression m) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final TanExpression m) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final ASinExpression m) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final ACosExpression m) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final ATanExpression m) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final FloorExpression f) {
    return KlaimType.INTEGER;
  }
  
  protected KlaimType _getType(final CeilExpression c) {
    return KlaimType.INTEGER;
  }
  
  protected KlaimType _getType(final PowExpression p) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final Reference r) {
    KlaimType _xblockexpression = null;
    {
      ReferenceableElements _ref = r.getRef();
      this.getType(_ref);
      _xblockexpression = KlaimType.ERROR;
    }
    return _xblockexpression;
  }
  
  protected KlaimType _getType(final TupleSelection ts) {
    return KlaimType.DOUBLE;
  }
  
  protected KlaimType _getType(final Call r) {
    KlaimType _xblockexpression = null;
    {
      Reference _function = r.getFunction();
      ReferenceableElements f = _function.getRef();
      KlaimType _switchResult = null;
      boolean _matched = false;
      if (!_matched) {
        if (f instanceof ProcessDeclaration) {
          _matched=true;
          _switchResult = KlaimType.PROCESS;
        }
      }
      if (!_matched) {
        if (f instanceof FunctionDeclaration) {
          _matched=true;
          _switchResult = KlaimType.ERROR;
        }
      }
      if (!_matched) {
        _switchResult = KlaimType.ERROR;
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
  
  protected KlaimType _getType(final LogicalLocality l) {
    return KlaimType.LOCALITY;
  }
  
  protected KlaimType _getType(final NodeDeclaration n) {
    return KlaimType.LOCALITY;
  }
  
  protected KlaimType _getType(final Variable v) {
    KlaimType _xblockexpression = null;
    {
      Type _type = v.getType();
      this.getType(_type);
      _xblockexpression = KlaimType.ERROR;
    }
    return _xblockexpression;
  }
  
  protected KlaimType _getType(final ConstantDeclaration c) {
    KlaimType _xblockexpression = null;
    {
      Type _type = c.getType();
      this.getType(_type);
      _xblockexpression = KlaimType.ERROR;
    }
    return _xblockexpression;
  }
  
  /**
   * def dispatch KlaimType getType( TypeExpression e ) {
   * var result = e.type.type
   * for (m:e.modifiers) {
   * switch m {
   * case TypeModifier::ARRAYM: result = KlaimType::getArray(result)
   * case TypeModifier::LISTM: result = KlaimType::getArray(result)
   * }
   * }
   * result
   * }
   */
  protected KlaimType _getType(final Type t) {
    KlaimType _switchResult = null;
    if (t != null) {
      switch (t) {
        case KBOOL:
          _switchResult = KlaimType.BOOLEAN;
          break;
        case KINT:
          _switchResult = KlaimType.INTEGER;
          break;
        case KDOUBLE:
          _switchResult = KlaimType.DOUBLE;
          break;
        case KSTRING:
          _switchResult = KlaimType.STRING;
          break;
        case KLOCALITY:
          _switchResult = KlaimType.LOCALITY;
          break;
        case KPROCESS:
          _switchResult = KlaimType.PROCESS;
          break;
        case KVOID:
          _switchResult = KlaimType.VOID;
          break;
        default:
          _switchResult = KlaimType.ERROR;
          break;
      }
    } else {
      _switchResult = KlaimType.ERROR;
    }
    return _switchResult;
  }
  
  public KlaimType getMax(final KlaimType t1, final KlaimType t2) {
    boolean _contains = t1.contains(t2);
    if (_contains) {
      return t1;
    }
    boolean _contains_1 = t2.contains(t1);
    if (_contains_1) {
      return t2;
    }
    return KlaimType.ERROR;
  }
  
  public KlaimType getType(final Object m) {
    if (m instanceof ACosExpression) {
      return _getType((ACosExpression)m);
    } else if (m instanceof ASinExpression) {
      return _getType((ASinExpression)m);
    } else if (m instanceof ATanExpression) {
      return _getType((ATanExpression)m);
    } else if (m instanceof Call) {
      return _getType((Call)m);
    } else if (m instanceof CeilExpression) {
      return _getType((CeilExpression)m);
    } else if (m instanceof Conjunction) {
      return _getType((Conjunction)m);
    } else if (m instanceof ConstantDeclaration) {
      return _getType((ConstantDeclaration)m);
    } else if (m instanceof CosExpression) {
      return _getType((CosExpression)m);
    } else if (m instanceof Disjunction) {
      return _getType((Disjunction)m);
    } else if (m instanceof FalseExpression) {
      return _getType((FalseExpression)m);
    } else if (m instanceof FloorExpression) {
      return _getType((FloorExpression)m);
    } else if (m instanceof LogExpression) {
      return _getType((LogExpression)m);
    } else if (m instanceof LogicalLocality) {
      return _getType((LogicalLocality)m);
    } else if (m instanceof MaxExpression) {
      return _getType((MaxExpression)m);
    } else if (m instanceof MinExpression) {
      return _getType((MinExpression)m);
    } else if (m instanceof ModExpression) {
      return _getType((ModExpression)m);
    } else if (m instanceof MultiplicationDivision) {
      return _getType((MultiplicationDivision)m);
    } else if (m instanceof Negation) {
      return _getType((Negation)m);
    } else if (m instanceof NodeDeclaration) {
      return _getType((NodeDeclaration)m);
    } else if (m instanceof NumberLiteral) {
      return _getType((NumberLiteral)m);
    } else if (m instanceof PowExpression) {
      return _getType((PowExpression)m);
    } else if (m instanceof Reference) {
      return _getType((Reference)m);
    } else if (m instanceof Relation) {
      return _getType((Relation)m);
    } else if (m instanceof SelfExpression) {
      return _getType((SelfExpression)m);
    } else if (m instanceof SinExpression) {
      return _getType((SinExpression)m);
    } else if (m instanceof StringLiteral) {
      return _getType((StringLiteral)m);
    } else if (m instanceof SummationSubtraction) {
      return _getType((SummationSubtraction)m);
    } else if (m instanceof TanExpression) {
      return _getType((TanExpression)m);
    } else if (m instanceof TrueExpression) {
      return _getType((TrueExpression)m);
    } else if (m instanceof TupleSelection) {
      return _getType((TupleSelection)m);
    } else if (m instanceof Variable) {
      return _getType((Variable)m);
    } else if (m instanceof Expression) {
      return _getType((Expression)m);
    } else if (m instanceof Type) {
      return _getType((Type)m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(m).toString());
    }
  }
}
