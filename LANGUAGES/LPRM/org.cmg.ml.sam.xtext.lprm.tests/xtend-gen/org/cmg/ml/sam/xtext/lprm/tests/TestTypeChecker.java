package org.cmg.ml.sam.xtext.lprm.tests;

import com.google.inject.Inject;
import org.cmg.ml.sam.xtext.lprm.LprSpecificationInjectorProvider;
import org.cmg.ml.sam.xtext.lprm.lprSpecification.LprSpecificationPackage;
import org.cmg.ml.sam.xtext.lprm.lprSpecification.Model;
import org.cmg.ml.sam.xtext.lprm.validation.LprSpecificationValidator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(LprSpecificationInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class TestTypeChecker {
  @Inject
  @Extension
  private ParseHelper<Model> _parseHelper;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Test
  public void checkNotTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c = not 2;");
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _not = LprSpecificationPackage.eINSTANCE.getNot();
          TestTypeChecker.this._validationTestHelper.assertError(it, _not, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: expected BOOLEAN_TYPE is INT_TYPE");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkAndLeftTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = 2 and true;");
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _and = LprSpecificationPackage.eINSTANCE.getAnd();
          TestTypeChecker.this._validationTestHelper.assertError(it, _and, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: expected BOOLEAN_TYPE is INT_TYPE");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkAndRightTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = true and 2;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _and = LprSpecificationPackage.eINSTANCE.getAnd();
          TestTypeChecker.this._validationTestHelper.assertError(it, _and, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: expected BOOLEAN_TYPE is INT_TYPE");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkOrLeftTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = 2 or true;");
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _or = LprSpecificationPackage.eINSTANCE.getOr();
          TestTypeChecker.this._validationTestHelper.assertError(it, _or, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: expected BOOLEAN_TYPE is INT_TYPE");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkOrRightTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = true or 2;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _or = LprSpecificationPackage.eINSTANCE.getOr();
          TestTypeChecker.this._validationTestHelper.assertError(it, _or, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: expected BOOLEAN_TYPE is INT_TYPE");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkSumLeftTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = true + 2;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _plus = LprSpecificationPackage.eINSTANCE.getPlus();
          TestTypeChecker.this._validationTestHelper.assertError(it, _plus, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: BOOLEAN_TYPE is not a valid type for arithmetic operations");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkSumRightTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = 2 + true;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _plus = LprSpecificationPackage.eINSTANCE.getPlus();
          TestTypeChecker.this._validationTestHelper.assertError(it, _plus, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: BOOLEAN_TYPE is not a valid type for arithmetic operations");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkMinusLeftTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = true - 2;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _minus = LprSpecificationPackage.eINSTANCE.getMinus();
          TestTypeChecker.this._validationTestHelper.assertError(it, _minus, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: BOOLEAN_TYPE is not a valid type for arithmetic operations");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkMinusRightTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = 2 - true;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _minus = LprSpecificationPackage.eINSTANCE.getMinus();
          TestTypeChecker.this._validationTestHelper.assertError(it, _minus, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: BOOLEAN_TYPE is not a valid type for arithmetic operations");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkMulLeftTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = true * 2;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _mul = LprSpecificationPackage.eINSTANCE.getMul();
          TestTypeChecker.this._validationTestHelper.assertError(it, _mul, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: BOOLEAN_TYPE is not a valid type for arithmetic operations");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkMulRightTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = 2 * true;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _mul = LprSpecificationPackage.eINSTANCE.getMul();
          TestTypeChecker.this._validationTestHelper.assertError(it, _mul, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: BOOLEAN_TYPE is not a valid type for arithmetic operations");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkDivLeftTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = true / 2;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _div = LprSpecificationPackage.eINSTANCE.getDiv();
          TestTypeChecker.this._validationTestHelper.assertError(it, _div, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: BOOLEAN_TYPE is not a valid type for arithmetic operations");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void checkDivRightTypeError() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("specification Test;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant int c2 = 2 / true;");
      _builder.newLine();
      _builder.newLine();
      Model _parse = this._parseHelper.parse(_builder);
      final Procedure1<Model> _function = new Procedure1<Model>() {
        @Override
        public void apply(final Model it) {
          EClass _div = LprSpecificationPackage.eINSTANCE.getDiv();
          TestTypeChecker.this._validationTestHelper.assertError(it, _div, 
            LprSpecificationValidator.WRONG_TYPE, 
            "Type error: BOOLEAN_TYPE is not a valid type for arithmetic operations");
        }
      };
      ObjectExtensions.<Model>operator_doubleArrow(_parse, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
