package org.cmg.ml.sam.xtext.population.tests;

import com.google.inject.Inject;
import java.util.List;
import java.util.function.Consumer;
import org.cmg.ml.sam.core.Filter;
import org.cmg.ml.sam.core.logic.StateFormula;
import org.cmg.ml.sam.population.PopulationModule;
import org.cmg.ml.sam.population.PopulationSpecification;
import org.cmg.ml.sam.util.OnTheFlyJavaCompiler;
import org.cmg.ml.sam.xtext.population.PopulationInjectorProvider;
import org.cmg.ml.sam.xtext.population.generator.PopulationToJavaCompiler;
import org.cmg.ml.sam.xtext.population.population.Model;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(PopulationInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class TestParser {
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Inject
  private OnTheFlyJavaCompiler compiler;
  
  @Inject
  private PopulationToJavaCompiler populationToJavaCompiler;
  
  @Test
  public void testParserEpidemic() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("constant N = 100;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("constant alpha_e = 0.1;");
      _builder.newLine();
      _builder.append("constant alpha_i = 0.2;");
      _builder.newLine();
      _builder.append("constant alpha_r = 0.2;");
      _builder.newLine();
      _builder.append("constant alpha_a = 1-alpha_r*alpha_i;");
      _builder.newLine();
      _builder.append("constant alpha_s = 1-alpha_a;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("action inf_ext: alpha_e;");
      _builder.newLine();
      _builder.append("action inf_sus: 1/alpha_i;");
      _builder.newLine();
      _builder.append("action activate: alpha_a;");
      _builder.newLine();
      _builder.append("action patch: alpha_r;");
      _builder.newLine();
      _builder.append("action loss: alpha_s;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("state S {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("inf_ext.E");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("+ inf_sus.E");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("state E {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("activate.I");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("state I {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("patch.R");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("state R {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("loss.S");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("system mySystem = < S[100] >;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("formula test1: P{ <= 0.2 }[ true U<=100 I];");
      _builder.newLine();
      _builder.newLine();
      _builder.append("//This is a comment!");
      _builder.newLine();
      _builder.append("formula test2: P{ <= 0.2 }[ frc(I)<0.25 U<=100 E];");
      _builder.newLine();
      _builder.newLine();
      _builder.append("//This is a comment!");
      _builder.newLine();
      _builder.append("formula test3: P{ <= 0.2 }[ test1 U<=100 test2 ];");
      _builder.newLine();
      Model m = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(m);
      PopulationSpecification o = this.doCompile(m);
      InputOutput.<PopulationSpecification>print(o);
      InputOutput.<String>print("\n\nState formulae: ");
      String[] _stateFormulae = o.getStateFormulae();
      final Consumer<String> _function = new Consumer<String>() {
        @Override
        public void accept(final String x) {
          InputOutput.<String>print((x + " "));
        }
      };
      ((List<String>)Conversions.doWrapArray(_stateFormulae)).forEach(_function);
      InputOutput.<String>print("\n");
      InputOutput.<String>print("Path formulae: ");
      String[] _pathFormulae = o.getPathFormulae();
      final Consumer<String> _function_1 = new Consumer<String>() {
        @Override
        public void accept(final String x) {
          InputOutput.<String>print((x + " "));
        }
      };
      ((List<String>)Conversions.doWrapArray(_pathFormulae)).forEach(_function_1);
      InputOutput.<String>print("\n");
      InputOutput.<String>print("Configurations: ");
      String[] _configurationNames = o.getConfigurationNames();
      final Consumer<String> _function_2 = new Consumer<String>() {
        @Override
        public void accept(final String x) {
          InputOutput.<String>print((x + " "));
        }
      };
      ((List<String>)Conversions.doWrapArray(_configurationNames)).forEach(_function_2);
      InputOutput.<String>print("\n");
      InputOutput.<String>print("******** END *********\n\n");
      return;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public PopulationSpecification doCompile(final Model m) {
    try {
      PopulationSpecification _xblockexpression = null;
      {
        CharSequence code = this.populationToJavaCompiler.compile("testpackage", "TestClass", m);
        InputOutput.<CharSequence>println(code);
        ClassLoader _systemClassLoader = ClassLoader.getSystemClassLoader();
        this.compiler.setParentClassLoader(_systemClassLoader);
        this.compiler.addClassPathOfClass(PopulationModule.class);
        this.compiler.addClassPathOfClass(Filter.class);
        this.compiler.addClassPathOfClass(StateFormula.class);
        String _string = code.toString();
        Class<?> c = this.compiler.compileToClass("testpackage.TestClass", _string);
        Object _newInstance = c.newInstance();
        _xblockexpression = ((PopulationSpecification) _newInstance);
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
