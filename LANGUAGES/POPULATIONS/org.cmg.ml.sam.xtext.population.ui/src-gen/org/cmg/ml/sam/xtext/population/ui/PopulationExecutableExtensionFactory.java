/*
 * generated by Xtext
 */
package org.cmg.ml.sam.xtext.population.ui;

import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import org.cmg.ml.sam.xtext.population.ui.internal.PopulationActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class PopulationExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return PopulationActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return PopulationActivator.getInstance().getInjector(PopulationActivator.ORG_CMG_ML_SAM_XTEXT_POPULATION_POPULATION);
	}
	
}
