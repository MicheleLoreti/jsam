/**
 */
package org.cmg.ml.sam.xtext.population.population;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.Configuration#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getConfiguration()
 * @model
 * @generated
 */
public interface Configuration extends Element
{
  /**
   * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
   * The list contents are of type {@link org.cmg.ml.sam.xtext.population.population.PopulationElement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elements</em>' containment reference list.
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getConfiguration_Elements()
   * @model containment="true"
   * @generated
   */
  EList<PopulationElement> getElements();

} // Configuration
