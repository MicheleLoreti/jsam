/**
 */
package org.cmg.ml.sam.xtext.population.population;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends EObject
{
} // Expression
