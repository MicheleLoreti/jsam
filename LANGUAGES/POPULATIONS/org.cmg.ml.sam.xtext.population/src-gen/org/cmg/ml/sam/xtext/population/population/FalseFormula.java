/**
 */
package org.cmg.ml.sam.xtext.population.population;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>False Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getFalseFormula()
 * @model
 * @generated
 */
public interface FalseFormula extends Expression
{
} // FalseFormula
