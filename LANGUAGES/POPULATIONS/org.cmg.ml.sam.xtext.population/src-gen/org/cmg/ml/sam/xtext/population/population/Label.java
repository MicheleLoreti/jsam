/**
 */
package org.cmg.ml.sam.xtext.population.population;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Label</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.Label#getStates <em>States</em>}</li>
 * </ul>
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getLabel()
 * @model
 * @generated
 */
public interface Label extends Element, ReferenceableName
{
  /**
   * Returns the value of the '<em><b>States</b></em>' reference list.
   * The list contents are of type {@link org.cmg.ml.sam.xtext.population.population.StateConstant}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>States</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>States</em>' reference list.
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getLabel_States()
   * @model
   * @generated
   */
  EList<StateConstant> getStates();

} // Label
