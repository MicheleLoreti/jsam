/**
 */
package org.cmg.ml.sam.xtext.population.population;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Named Pctl Path Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.NamedPctlPathFormula#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getNamedPctlPathFormula()
 * @model
 * @generated
 */
public interface NamedPctlPathFormula extends PctlPathFormula
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' reference.
   * @see #setName(PathFormula)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getNamedPctlPathFormula_Name()
   * @model
   * @generated
   */
  PathFormula getName();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.NamedPctlPathFormula#getName <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' reference.
   * @see #getName()
   * @generated
   */
  void setName(PathFormula value);

} // NamedPctlPathFormula
