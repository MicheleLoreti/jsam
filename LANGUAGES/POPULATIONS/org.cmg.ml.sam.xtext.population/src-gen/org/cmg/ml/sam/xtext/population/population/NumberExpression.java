/**
 */
package org.cmg.ml.sam.xtext.population.population;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getNumberExpression()
 * @model
 * @generated
 */
public interface NumberExpression extends Expression
{
} // NumberExpression
