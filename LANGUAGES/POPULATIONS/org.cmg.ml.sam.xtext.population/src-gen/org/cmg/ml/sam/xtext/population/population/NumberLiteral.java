/**
 */
package org.cmg.ml.sam.xtext.population.population;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.NumberLiteral#getIntPart <em>Int Part</em>}</li>
 * </ul>
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getNumberLiteral()
 * @model
 * @generated
 */
public interface NumberLiteral extends NumberExpression
{
  /**
   * Returns the value of the '<em><b>Int Part</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Int Part</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Int Part</em>' attribute.
   * @see #setIntPart(int)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getNumberLiteral_IntPart()
   * @model
   * @generated
   */
  int getIntPart();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.NumberLiteral#getIntPart <em>Int Part</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Int Part</em>' attribute.
   * @see #getIntPart()
   * @generated
   */
  void setIntPart(int value);

} // NumberLiteral
