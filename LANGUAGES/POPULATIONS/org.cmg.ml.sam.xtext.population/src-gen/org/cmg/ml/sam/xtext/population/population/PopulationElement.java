/**
 */
package org.cmg.ml.sam.xtext.population.population;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#getState <em>State</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#isHasSize <em>Has Size</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getPopulationElement()
 * @model
 * @generated
 */
public interface PopulationElement extends EObject
{
  /**
   * Returns the value of the '<em><b>State</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>State</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>State</em>' reference.
   * @see #setState(StateConstant)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getPopulationElement_State()
   * @model
   * @generated
   */
  StateConstant getState();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#getState <em>State</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>State</em>' reference.
   * @see #getState()
   * @generated
   */
  void setState(StateConstant value);

  /**
   * Returns the value of the '<em><b>Has Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Has Size</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Has Size</em>' attribute.
   * @see #setHasSize(boolean)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getPopulationElement_HasSize()
   * @model
   * @generated
   */
  boolean isHasSize();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#isHasSize <em>Has Size</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Has Size</em>' attribute.
   * @see #isHasSize()
   * @generated
   */
  void setHasSize(boolean value);

  /**
   * Returns the value of the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Size</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Size</em>' containment reference.
   * @see #setSize(Expression)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getPopulationElement_Size()
   * @model containment="true"
   * @generated
   */
  Expression getSize();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#getSize <em>Size</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Size</em>' containment reference.
   * @see #getSize()
   * @generated
   */
  void setSize(Expression value);

} // PopulationElement
