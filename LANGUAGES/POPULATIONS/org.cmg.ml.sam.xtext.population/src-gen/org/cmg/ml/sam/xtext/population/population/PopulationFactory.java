/**
 */
package org.cmg.ml.sam.xtext.population.population;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage
 * @generated
 */
public interface PopulationFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  PopulationFactory eINSTANCE = org.cmg.ml.sam.xtext.population.population.impl.PopulationFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Model</em>'.
   * @generated
   */
  Model createModel();

  /**
   * Returns a new object of class '<em>Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Element</em>'.
   * @generated
   */
  Element createElement();

  /**
   * Returns a new object of class '<em>Path Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Path Formula</em>'.
   * @generated
   */
  PathFormula createPathFormula();

  /**
   * Returns a new object of class '<em>Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Formula</em>'.
   * @generated
   */
  Formula createFormula();

  /**
   * Returns a new object of class '<em>Label</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Label</em>'.
   * @generated
   */
  Label createLabel();

  /**
   * Returns a new object of class '<em>Constant</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constant</em>'.
   * @generated
   */
  Constant createConstant();

  /**
   * Returns a new object of class '<em>Action</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Action</em>'.
   * @generated
   */
  Action createAction();

  /**
   * Returns a new object of class '<em>State Constant</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>State Constant</em>'.
   * @generated
   */
  StateConstant createStateConstant();

  /**
   * Returns a new object of class '<em>Transition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Transition</em>'.
   * @generated
   */
  Transition createTransition();

  /**
   * Returns a new object of class '<em>Configuration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Configuration</em>'.
   * @generated
   */
  Configuration createConfiguration();

  /**
   * Returns a new object of class '<em>Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Element</em>'.
   * @generated
   */
  PopulationElement createPopulationElement();

  /**
   * Returns a new object of class '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression</em>'.
   * @generated
   */
  Expression createExpression();

  /**
   * Returns a new object of class '<em>Literal Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Literal Expression</em>'.
   * @generated
   */
  LiteralExpression createLiteralExpression();

  /**
   * Returns a new object of class '<em>Mod Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mod Expression</em>'.
   * @generated
   */
  ModExpression createModExpression();

  /**
   * Returns a new object of class '<em>Log Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Log Expression</em>'.
   * @generated
   */
  LogExpression createLogExpression();

  /**
   * Returns a new object of class '<em>Pow Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pow Expression</em>'.
   * @generated
   */
  PowExpression createPowExpression();

  /**
   * Returns a new object of class '<em>Floor Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Floor Expression</em>'.
   * @generated
   */
  FloorExpression createFloorExpression();

  /**
   * Returns a new object of class '<em>Ceil Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ceil Expression</em>'.
   * @generated
   */
  CeilExpression createCeilExpression();

  /**
   * Returns a new object of class '<em>Min Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Min Expression</em>'.
   * @generated
   */
  MinExpression createMinExpression();

  /**
   * Returns a new object of class '<em>Max Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Max Expression</em>'.
   * @generated
   */
  MaxExpression createMaxExpression();

  /**
   * Returns a new object of class '<em>Sin Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sin Expression</em>'.
   * @generated
   */
  SinExpression createSinExpression();

  /**
   * Returns a new object of class '<em>Cos Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cos Expression</em>'.
   * @generated
   */
  CosExpression createCosExpression();

  /**
   * Returns a new object of class '<em>Tan Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Tan Expression</em>'.
   * @generated
   */
  TanExpression createTanExpression();

  /**
   * Returns a new object of class '<em>ATan Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>ATan Expression</em>'.
   * @generated
   */
  ATanExpression createATanExpression();

  /**
   * Returns a new object of class '<em>ASin Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>ASin Expression</em>'.
   * @generated
   */
  ASinExpression createASinExpression();

  /**
   * Returns a new object of class '<em>ACos Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>ACos Expression</em>'.
   * @generated
   */
  ACosExpression createACosExpression();

  /**
   * Returns a new object of class '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression</em>'.
   * @generated
   */
  PopulationExpression createPopulationExpression();

  /**
   * Returns a new object of class '<em>Number Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Number Expression</em>'.
   * @generated
   */
  NumberExpression createNumberExpression();

  /**
   * Returns a new object of class '<em>Number Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Number Literal</em>'.
   * @generated
   */
  NumberLiteral createNumberLiteral();

  /**
   * Returns a new object of class '<em>Referenceable Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Referenceable Name</em>'.
   * @generated
   */
  ReferenceableName createReferenceableName();

  /**
   * Returns a new object of class '<em>Probability Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Probability Formula</em>'.
   * @generated
   */
  ProbabilityFormula createProbabilityFormula();

  /**
   * Returns a new object of class '<em>Pctl Path Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pctl Path Formula</em>'.
   * @generated
   */
  PctlPathFormula createPctlPathFormula();

  /**
   * Returns a new object of class '<em>Named Pctl Path Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Named Pctl Path Formula</em>'.
   * @generated
   */
  NamedPctlPathFormula createNamedPctlPathFormula();

  /**
   * Returns a new object of class '<em>Next Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Next Formula</em>'.
   * @generated
   */
  NextFormula createNextFormula();

  /**
   * Returns a new object of class '<em>Until Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Until Formula</em>'.
   * @generated
   */
  UntilFormula createUntilFormula();

  /**
   * Returns a new object of class '<em>False Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>False Formula</em>'.
   * @generated
   */
  FalseFormula createFalseFormula();

  /**
   * Returns a new object of class '<em>True Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>True Formula</em>'.
   * @generated
   */
  TrueFormula createTrueFormula();

  /**
   * Returns a new object of class '<em>Not Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Not Formula</em>'.
   * @generated
   */
  NotFormula createNotFormula();

  /**
   * Returns a new object of class '<em>Relation Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Relation Expression</em>'.
   * @generated
   */
  RelationExpression createRelationExpression();

  /**
   * Returns a new object of class '<em>Sum Diff Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sum Diff Expression</em>'.
   * @generated
   */
  SumDiffExpression createSumDiffExpression();

  /**
   * Returns a new object of class '<em>Mul Div Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mul Div Expression</em>'.
   * @generated
   */
  MulDivExpression createMulDivExpression();

  /**
   * Returns a new object of class '<em>Decimal Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Decimal Literal</em>'.
   * @generated
   */
  DecimalLiteral createDecimalLiteral();

  /**
   * Returns a new object of class '<em>Or Pctl Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Or Pctl Formula</em>'.
   * @generated
   */
  OrPctlFormula createOrPctlFormula();

  /**
   * Returns a new object of class '<em>And Pctl Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>And Pctl Formula</em>'.
   * @generated
   */
  AndPctlFormula createAndPctlFormula();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  PopulationPackage getPopulationPackage();

} //PopulationFactory
