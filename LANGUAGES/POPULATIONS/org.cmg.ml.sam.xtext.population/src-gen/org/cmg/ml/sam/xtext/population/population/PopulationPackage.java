/**
 */
package org.cmg.ml.sam.xtext.population.population;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.cmg.ml.sam.xtext.population.population.PopulationFactory
 * @model kind="package"
 * @generated
 */
public interface PopulationPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "population";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.cmg.org/ml/sam/xtext/population/Population";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "population";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  PopulationPackage eINSTANCE = org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl.init();

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ModelImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__ELEMENTS = 0;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ElementImpl <em>Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ElementImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getElement()
   * @generated
   */
  int ELEMENT = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT__NAME = 0;

  /**
   * The number of structural features of the '<em>Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PathFormulaImpl <em>Path Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.PathFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPathFormula()
   * @generated
   */
  int PATH_FORMULA = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_FORMULA__NAME = ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Formula</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_FORMULA__FORMULA = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Path Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_FORMULA_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.FormulaImpl <em>Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.FormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getFormula()
   * @generated
   */
  int FORMULA = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULA__NAME = ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Formula</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULA__FORMULA = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULA_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.LabelImpl <em>Label</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.LabelImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getLabel()
   * @generated
   */
  int LABEL = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL__NAME = ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>States</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL__STATES = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Label</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ConstantImpl <em>Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ConstantImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getConstant()
   * @generated
   */
  int CONSTANT = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__NAME = ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__EXP = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ActionImpl <em>Action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ActionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getAction()
   * @generated
   */
  int ACTION = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__NAME = ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Probability</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__PROBABILITY = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.StateConstantImpl <em>State Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.StateConstantImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getStateConstant()
   * @generated
   */
  int STATE_CONSTANT = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_CONSTANT__NAME = ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_CONSTANT__TRANSITIONS = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>State Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_CONSTANT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.TransitionImpl <em>Transition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.TransitionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getTransition()
   * @generated
   */
  int TRANSITION = 8;

  /**
   * The feature id for the '<em><b>Action</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITION__ACTION = 0;

  /**
   * The feature id for the '<em><b>Next State</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITION__NEXT_STATE = 1;

  /**
   * The number of structural features of the '<em>Transition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ConfigurationImpl <em>Configuration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ConfigurationImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getConfiguration()
   * @generated
   */
  int CONFIGURATION = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION__NAME = ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION__ELEMENTS = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Configuration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PopulationElementImpl <em>Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationElementImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPopulationElement()
   * @generated
   */
  int POPULATION_ELEMENT = 10;

  /**
   * The feature id for the '<em><b>State</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POPULATION_ELEMENT__STATE = 0;

  /**
   * The feature id for the '<em><b>Has Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POPULATION_ELEMENT__HAS_SIZE = 1;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POPULATION_ELEMENT__SIZE = 2;

  /**
   * The number of structural features of the '<em>Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POPULATION_ELEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 11;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.LiteralExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getLiteralExpression()
   * @generated
   */
  int LITERAL_EXPRESSION = 12;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LITERAL_EXPRESSION__REF = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Literal Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LITERAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ModExpressionImpl <em>Mod Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ModExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getModExpression()
   * @generated
   */
  int MOD_EXPRESSION = 13;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOD_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Mod</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOD_EXPRESSION__MOD = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Mod Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOD_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.LogExpressionImpl <em>Log Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.LogExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getLogExpression()
   * @generated
   */
  int LOG_EXPRESSION = 14;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOG_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Log Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOG_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PowExpressionImpl <em>Pow Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.PowExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPowExpression()
   * @generated
   */
  int POW_EXPRESSION = 15;

  /**
   * The feature id for the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POW_EXPRESSION__BASE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POW_EXPRESSION__EXP = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Pow Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POW_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.FloorExpressionImpl <em>Floor Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.FloorExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getFloorExpression()
   * @generated
   */
  int FLOOR_EXPRESSION = 16;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FLOOR_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Floor Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FLOOR_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.CeilExpressionImpl <em>Ceil Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.CeilExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getCeilExpression()
   * @generated
   */
  int CEIL_EXPRESSION = 17;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CEIL_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ceil Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CEIL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.MinExpressionImpl <em>Min Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.MinExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getMinExpression()
   * @generated
   */
  int MIN_EXPRESSION = 18;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Min Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.MaxExpressionImpl <em>Max Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.MaxExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getMaxExpression()
   * @generated
   */
  int MAX_EXPRESSION = 19;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Max Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.SinExpressionImpl <em>Sin Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.SinExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getSinExpression()
   * @generated
   */
  int SIN_EXPRESSION = 20;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIN_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Sin Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIN_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.CosExpressionImpl <em>Cos Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.CosExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getCosExpression()
   * @generated
   */
  int COS_EXPRESSION = 21;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COS_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Cos Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.TanExpressionImpl <em>Tan Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.TanExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getTanExpression()
   * @generated
   */
  int TAN_EXPRESSION = 22;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAN_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Tan Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAN_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ATanExpressionImpl <em>ATan Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ATanExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getATanExpression()
   * @generated
   */
  int ATAN_EXPRESSION = 23;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ATAN_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>ATan Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ATAN_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ASinExpressionImpl <em>ASin Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ASinExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getASinExpression()
   * @generated
   */
  int ASIN_EXPRESSION = 24;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASIN_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>ASin Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASIN_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ACosExpressionImpl <em>ACos Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ACosExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getACosExpression()
   * @generated
   */
  int ACOS_EXPRESSION = 25;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACOS_EXPRESSION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>ACos Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACOS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PopulationExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPopulationExpression()
   * @generated
   */
  int POPULATION_EXPRESSION = 26;

  /**
   * The feature id for the '<em><b>State</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POPULATION_EXPRESSION__STATE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POPULATION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NumberExpressionImpl <em>Number Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.NumberExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNumberExpression()
   * @generated
   */
  int NUMBER_EXPRESSION = 27;

  /**
   * The number of structural features of the '<em>Number Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMBER_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NumberLiteralImpl <em>Number Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.NumberLiteralImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNumberLiteral()
   * @generated
   */
  int NUMBER_LITERAL = 28;

  /**
   * The feature id for the '<em><b>Int Part</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMBER_LITERAL__INT_PART = NUMBER_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Number Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMBER_LITERAL_FEATURE_COUNT = NUMBER_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ReferenceableNameImpl <em>Referenceable Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ReferenceableNameImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getReferenceableName()
   * @generated
   */
  int REFERENCEABLE_NAME = 29;

  /**
   * The number of structural features of the '<em>Referenceable Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCEABLE_NAME_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ProbabilityFormulaImpl <em>Probability Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.ProbabilityFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getProbabilityFormula()
   * @generated
   */
  int PROBABILITY_FORMULA = 30;

  /**
   * The feature id for the '<em><b>Rel</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROBABILITY_FORMULA__REL = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>PBound</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROBABILITY_FORMULA__PBOUND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Path</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROBABILITY_FORMULA__PATH = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Probability Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROBABILITY_FORMULA_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PctlPathFormulaImpl <em>Pctl Path Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.PctlPathFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPctlPathFormula()
   * @generated
   */
  int PCTL_PATH_FORMULA = 31;

  /**
   * The number of structural features of the '<em>Pctl Path Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PCTL_PATH_FORMULA_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NamedPctlPathFormulaImpl <em>Named Pctl Path Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.NamedPctlPathFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNamedPctlPathFormula()
   * @generated
   */
  int NAMED_PCTL_PATH_FORMULA = 32;

  /**
   * The feature id for the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_PCTL_PATH_FORMULA__NAME = PCTL_PATH_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Named Pctl Path Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_PCTL_PATH_FORMULA_FEATURE_COUNT = PCTL_PATH_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NextFormulaImpl <em>Next Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.NextFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNextFormula()
   * @generated
   */
  int NEXT_FORMULA = 33;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEXT_FORMULA__ARG = PCTL_PATH_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Next Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEXT_FORMULA_FEATURE_COUNT = PCTL_PATH_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.UntilFormulaImpl <em>Until Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.UntilFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getUntilFormula()
   * @generated
   */
  int UNTIL_FORMULA = 34;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNTIL_FORMULA__LEFT = PCTL_PATH_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Is Bound</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNTIL_FORMULA__IS_BOUND = PCTL_PATH_FORMULA_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Bound</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNTIL_FORMULA__BOUND = PCTL_PATH_FORMULA_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNTIL_FORMULA__RIGHT = PCTL_PATH_FORMULA_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Until Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNTIL_FORMULA_FEATURE_COUNT = PCTL_PATH_FORMULA_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.FalseFormulaImpl <em>False Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.FalseFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getFalseFormula()
   * @generated
   */
  int FALSE_FORMULA = 35;

  /**
   * The number of structural features of the '<em>False Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FALSE_FORMULA_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.TrueFormulaImpl <em>True Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.TrueFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getTrueFormula()
   * @generated
   */
  int TRUE_FORMULA = 36;

  /**
   * The number of structural features of the '<em>True Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRUE_FORMULA_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NotFormulaImpl <em>Not Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.NotFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNotFormula()
   * @generated
   */
  int NOT_FORMULA = 37;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_FORMULA__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Not Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_FORMULA_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.RelationExpressionImpl <em>Relation Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.RelationExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getRelationExpression()
   * @generated
   */
  int RELATION_EXPRESSION = 38;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION_EXPRESSION__OP = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Relation Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.SumDiffExpressionImpl <em>Sum Diff Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.SumDiffExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getSumDiffExpression()
   * @generated
   */
  int SUM_DIFF_EXPRESSION = 39;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_DIFF_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_DIFF_EXPRESSION__OP = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_DIFF_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Sum Diff Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_DIFF_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.MulDivExpressionImpl <em>Mul Div Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.MulDivExpressionImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getMulDivExpression()
   * @generated
   */
  int MUL_DIV_EXPRESSION = 40;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_DIV_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_DIV_EXPRESSION__OP = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_DIV_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Mul Div Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_DIV_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.DecimalLiteralImpl <em>Decimal Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.DecimalLiteralImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getDecimalLiteral()
   * @generated
   */
  int DECIMAL_LITERAL = 41;

  /**
   * The feature id for the '<em><b>Integer Part</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_LITERAL__INTEGER_PART = NUMBER_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Decimal Part</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_LITERAL__DECIMAL_PART = NUMBER_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Decimal Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_LITERAL_FEATURE_COUNT = NUMBER_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.OrPctlFormulaImpl <em>Or Pctl Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.OrPctlFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getOrPctlFormula()
   * @generated
   */
  int OR_PCTL_FORMULA = 42;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PCTL_FORMULA__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PCTL_FORMULA__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Or Pctl Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PCTL_FORMULA_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.impl.AndPctlFormulaImpl <em>And Pctl Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.impl.AndPctlFormulaImpl
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getAndPctlFormula()
   * @generated
   */
  int AND_PCTL_FORMULA = 43;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_PCTL_FORMULA__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_PCTL_FORMULA__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>And Pctl Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_PCTL_FORMULA_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.ml.sam.xtext.population.population.RelationSymbol <em>Relation Symbol</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.ml.sam.xtext.population.population.RelationSymbol
   * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getRelationSymbol()
   * @generated
   */
  int RELATION_SYMBOL = 44;


  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.ml.sam.xtext.population.population.Model#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Model#getElements()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Elements();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Element <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Element</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Element
   * @generated
   */
  EClass getElement();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.Element#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Element#getName()
   * @see #getElement()
   * @generated
   */
  EAttribute getElement_Name();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.PathFormula <em>Path Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Path Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PathFormula
   * @generated
   */
  EClass getPathFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.PathFormula#getFormula <em>Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PathFormula#getFormula()
   * @see #getPathFormula()
   * @generated
   */
  EReference getPathFormula_Formula();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Formula <em>Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Formula
   * @generated
   */
  EClass getFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.Formula#getFormula <em>Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Formula#getFormula()
   * @see #getFormula()
   * @generated
   */
  EReference getFormula_Formula();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Label <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Label</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Label
   * @generated
   */
  EClass getLabel();

  /**
   * Returns the meta object for the reference list '{@link org.cmg.ml.sam.xtext.population.population.Label#getStates <em>States</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>States</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Label#getStates()
   * @see #getLabel()
   * @generated
   */
  EReference getLabel_States();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Constant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Constant
   * @generated
   */
  EClass getConstant();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.Constant#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Constant#getExp()
   * @see #getConstant()
   * @generated
   */
  EReference getConstant_Exp();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Action <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Action</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Action
   * @generated
   */
  EClass getAction();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.Action#getProbability <em>Probability</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Probability</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Action#getProbability()
   * @see #getAction()
   * @generated
   */
  EReference getAction_Probability();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.StateConstant <em>State Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>State Constant</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.StateConstant
   * @generated
   */
  EClass getStateConstant();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.ml.sam.xtext.population.population.StateConstant#getTransitions <em>Transitions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Transitions</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.StateConstant#getTransitions()
   * @see #getStateConstant()
   * @generated
   */
  EReference getStateConstant_Transitions();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Transition <em>Transition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Transition</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Transition
   * @generated
   */
  EClass getTransition();

  /**
   * Returns the meta object for the reference '{@link org.cmg.ml.sam.xtext.population.population.Transition#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Action</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Transition#getAction()
   * @see #getTransition()
   * @generated
   */
  EReference getTransition_Action();

  /**
   * Returns the meta object for the reference '{@link org.cmg.ml.sam.xtext.population.population.Transition#getNextState <em>Next State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Next State</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Transition#getNextState()
   * @see #getTransition()
   * @generated
   */
  EReference getTransition_NextState();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Configuration <em>Configuration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Configuration</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Configuration
   * @generated
   */
  EClass getConfiguration();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.ml.sam.xtext.population.population.Configuration#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Configuration#getElements()
   * @see #getConfiguration()
   * @generated
   */
  EReference getConfiguration_Elements();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.PopulationElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Element</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PopulationElement
   * @generated
   */
  EClass getPopulationElement();

  /**
   * Returns the meta object for the reference '{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#getState <em>State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>State</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PopulationElement#getState()
   * @see #getPopulationElement()
   * @generated
   */
  EReference getPopulationElement_State();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#isHasSize <em>Has Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Has Size</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PopulationElement#isHasSize()
   * @see #getPopulationElement()
   * @generated
   */
  EAttribute getPopulationElement_HasSize();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.PopulationElement#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Size</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PopulationElement#getSize()
   * @see #getPopulationElement()
   * @generated
   */
  EReference getPopulationElement_Size();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.LiteralExpression <em>Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Literal Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.LiteralExpression
   * @generated
   */
  EClass getLiteralExpression();

  /**
   * Returns the meta object for the reference '{@link org.cmg.ml.sam.xtext.population.population.LiteralExpression#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.LiteralExpression#getRef()
   * @see #getLiteralExpression()
   * @generated
   */
  EReference getLiteralExpression_Ref();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.ModExpression <em>Mod Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mod Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ModExpression
   * @generated
   */
  EClass getModExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.ModExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ModExpression#getArg()
   * @see #getModExpression()
   * @generated
   */
  EReference getModExpression_Arg();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.ModExpression#getMod <em>Mod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Mod</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ModExpression#getMod()
   * @see #getModExpression()
   * @generated
   */
  EReference getModExpression_Mod();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.LogExpression <em>Log Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Log Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.LogExpression
   * @generated
   */
  EClass getLogExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.LogExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.LogExpression#getArg()
   * @see #getLogExpression()
   * @generated
   */
  EReference getLogExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.PowExpression <em>Pow Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pow Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PowExpression
   * @generated
   */
  EClass getPowExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.PowExpression#getBase <em>Base</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Base</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PowExpression#getBase()
   * @see #getPowExpression()
   * @generated
   */
  EReference getPowExpression_Base();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.PowExpression#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PowExpression#getExp()
   * @see #getPowExpression()
   * @generated
   */
  EReference getPowExpression_Exp();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.FloorExpression <em>Floor Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Floor Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.FloorExpression
   * @generated
   */
  EClass getFloorExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.FloorExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.FloorExpression#getArg()
   * @see #getFloorExpression()
   * @generated
   */
  EReference getFloorExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.CeilExpression <em>Ceil Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ceil Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.CeilExpression
   * @generated
   */
  EClass getCeilExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.CeilExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.CeilExpression#getArg()
   * @see #getCeilExpression()
   * @generated
   */
  EReference getCeilExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.MinExpression <em>Min Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Min Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MinExpression
   * @generated
   */
  EClass getMinExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.MinExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MinExpression#getLeft()
   * @see #getMinExpression()
   * @generated
   */
  EReference getMinExpression_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.MinExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MinExpression#getRight()
   * @see #getMinExpression()
   * @generated
   */
  EReference getMinExpression_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.MaxExpression <em>Max Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Max Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MaxExpression
   * @generated
   */
  EClass getMaxExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.MaxExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MaxExpression#getLeft()
   * @see #getMaxExpression()
   * @generated
   */
  EReference getMaxExpression_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.MaxExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MaxExpression#getRight()
   * @see #getMaxExpression()
   * @generated
   */
  EReference getMaxExpression_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.SinExpression <em>Sin Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sin Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.SinExpression
   * @generated
   */
  EClass getSinExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.SinExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.SinExpression#getArg()
   * @see #getSinExpression()
   * @generated
   */
  EReference getSinExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.CosExpression <em>Cos Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cos Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.CosExpression
   * @generated
   */
  EClass getCosExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.CosExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.CosExpression#getArg()
   * @see #getCosExpression()
   * @generated
   */
  EReference getCosExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.TanExpression <em>Tan Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Tan Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.TanExpression
   * @generated
   */
  EClass getTanExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.TanExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.TanExpression#getArg()
   * @see #getTanExpression()
   * @generated
   */
  EReference getTanExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.ATanExpression <em>ATan Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>ATan Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ATanExpression
   * @generated
   */
  EClass getATanExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.ATanExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ATanExpression#getArg()
   * @see #getATanExpression()
   * @generated
   */
  EReference getATanExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.ASinExpression <em>ASin Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>ASin Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ASinExpression
   * @generated
   */
  EClass getASinExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.ASinExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ASinExpression#getArg()
   * @see #getASinExpression()
   * @generated
   */
  EReference getASinExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.ACosExpression <em>ACos Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>ACos Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ACosExpression
   * @generated
   */
  EClass getACosExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.ACosExpression#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ACosExpression#getArg()
   * @see #getACosExpression()
   * @generated
   */
  EReference getACosExpression_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.PopulationExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PopulationExpression
   * @generated
   */
  EClass getPopulationExpression();

  /**
   * Returns the meta object for the reference '{@link org.cmg.ml.sam.xtext.population.population.PopulationExpression#getState <em>State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>State</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PopulationExpression#getState()
   * @see #getPopulationExpression()
   * @generated
   */
  EReference getPopulationExpression_State();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.NumberExpression <em>Number Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Number Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NumberExpression
   * @generated
   */
  EClass getNumberExpression();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.NumberLiteral <em>Number Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Number Literal</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NumberLiteral
   * @generated
   */
  EClass getNumberLiteral();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.NumberLiteral#getIntPart <em>Int Part</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Int Part</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NumberLiteral#getIntPart()
   * @see #getNumberLiteral()
   * @generated
   */
  EAttribute getNumberLiteral_IntPart();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.ReferenceableName <em>Referenceable Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Referenceable Name</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ReferenceableName
   * @generated
   */
  EClass getReferenceableName();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula <em>Probability Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Probability Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ProbabilityFormula
   * @generated
   */
  EClass getProbabilityFormula();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getRel <em>Rel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Rel</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getRel()
   * @see #getProbabilityFormula()
   * @generated
   */
  EAttribute getProbabilityFormula_Rel();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getPBound <em>PBound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>PBound</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getPBound()
   * @see #getProbabilityFormula()
   * @generated
   */
  EReference getProbabilityFormula_PBound();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getPath <em>Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Path</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getPath()
   * @see #getProbabilityFormula()
   * @generated
   */
  EReference getProbabilityFormula_Path();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.PctlPathFormula <em>Pctl Path Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pctl Path Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.PctlPathFormula
   * @generated
   */
  EClass getPctlPathFormula();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.NamedPctlPathFormula <em>Named Pctl Path Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Named Pctl Path Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NamedPctlPathFormula
   * @generated
   */
  EClass getNamedPctlPathFormula();

  /**
   * Returns the meta object for the reference '{@link org.cmg.ml.sam.xtext.population.population.NamedPctlPathFormula#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Name</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NamedPctlPathFormula#getName()
   * @see #getNamedPctlPathFormula()
   * @generated
   */
  EReference getNamedPctlPathFormula_Name();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.NextFormula <em>Next Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Next Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NextFormula
   * @generated
   */
  EClass getNextFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.NextFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NextFormula#getArg()
   * @see #getNextFormula()
   * @generated
   */
  EReference getNextFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula <em>Until Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Until Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.UntilFormula
   * @generated
   */
  EClass getUntilFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.UntilFormula#getLeft()
   * @see #getUntilFormula()
   * @generated
   */
  EReference getUntilFormula_Left();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#isIsBound <em>Is Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Is Bound</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.UntilFormula#isIsBound()
   * @see #getUntilFormula()
   * @generated
   */
  EAttribute getUntilFormula_IsBound();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getBound <em>Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bound</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.UntilFormula#getBound()
   * @see #getUntilFormula()
   * @generated
   */
  EAttribute getUntilFormula_Bound();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.UntilFormula#getRight()
   * @see #getUntilFormula()
   * @generated
   */
  EReference getUntilFormula_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.FalseFormula <em>False Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>False Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.FalseFormula
   * @generated
   */
  EClass getFalseFormula();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.TrueFormula <em>True Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>True Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.TrueFormula
   * @generated
   */
  EClass getTrueFormula();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.NotFormula <em>Not Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Not Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NotFormula
   * @generated
   */
  EClass getNotFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.NotFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.NotFormula#getArg()
   * @see #getNotFormula()
   * @generated
   */
  EReference getNotFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.RelationExpression <em>Relation Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Relation Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.RelationExpression
   * @generated
   */
  EClass getRelationExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.RelationExpression#getLeft()
   * @see #getRelationExpression()
   * @generated
   */
  EReference getRelationExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.RelationExpression#getOp()
   * @see #getRelationExpression()
   * @generated
   */
  EAttribute getRelationExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.RelationExpression#getRight()
   * @see #getRelationExpression()
   * @generated
   */
  EReference getRelationExpression_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.SumDiffExpression <em>Sum Diff Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sum Diff Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.SumDiffExpression
   * @generated
   */
  EClass getSumDiffExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.SumDiffExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.SumDiffExpression#getLeft()
   * @see #getSumDiffExpression()
   * @generated
   */
  EReference getSumDiffExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.SumDiffExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.SumDiffExpression#getOp()
   * @see #getSumDiffExpression()
   * @generated
   */
  EAttribute getSumDiffExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.SumDiffExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.SumDiffExpression#getRight()
   * @see #getSumDiffExpression()
   * @generated
   */
  EReference getSumDiffExpression_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.MulDivExpression <em>Mul Div Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mul Div Expression</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MulDivExpression
   * @generated
   */
  EClass getMulDivExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.MulDivExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MulDivExpression#getLeft()
   * @see #getMulDivExpression()
   * @generated
   */
  EReference getMulDivExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.MulDivExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MulDivExpression#getOp()
   * @see #getMulDivExpression()
   * @generated
   */
  EAttribute getMulDivExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.MulDivExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.MulDivExpression#getRight()
   * @see #getMulDivExpression()
   * @generated
   */
  EReference getMulDivExpression_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.DecimalLiteral <em>Decimal Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Decimal Literal</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.DecimalLiteral
   * @generated
   */
  EClass getDecimalLiteral();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.DecimalLiteral#getIntegerPart <em>Integer Part</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Integer Part</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.DecimalLiteral#getIntegerPart()
   * @see #getDecimalLiteral()
   * @generated
   */
  EReference getDecimalLiteral_IntegerPart();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.ml.sam.xtext.population.population.DecimalLiteral#getDecimalPart <em>Decimal Part</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Decimal Part</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.DecimalLiteral#getDecimalPart()
   * @see #getDecimalLiteral()
   * @generated
   */
  EAttribute getDecimalLiteral_DecimalPart();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.OrPctlFormula <em>Or Pctl Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Or Pctl Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.OrPctlFormula
   * @generated
   */
  EClass getOrPctlFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.OrPctlFormula#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.OrPctlFormula#getLeft()
   * @see #getOrPctlFormula()
   * @generated
   */
  EReference getOrPctlFormula_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.OrPctlFormula#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.OrPctlFormula#getRight()
   * @see #getOrPctlFormula()
   * @generated
   */
  EReference getOrPctlFormula_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.ml.sam.xtext.population.population.AndPctlFormula <em>And Pctl Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>And Pctl Formula</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.AndPctlFormula
   * @generated
   */
  EClass getAndPctlFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.AndPctlFormula#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.AndPctlFormula#getLeft()
   * @see #getAndPctlFormula()
   * @generated
   */
  EReference getAndPctlFormula_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.ml.sam.xtext.population.population.AndPctlFormula#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.AndPctlFormula#getRight()
   * @see #getAndPctlFormula()
   * @generated
   */
  EReference getAndPctlFormula_Right();

  /**
   * Returns the meta object for enum '{@link org.cmg.ml.sam.xtext.population.population.RelationSymbol <em>Relation Symbol</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Relation Symbol</em>'.
   * @see org.cmg.ml.sam.xtext.population.population.RelationSymbol
   * @generated
   */
  EEnum getRelationSymbol();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  PopulationFactory getPopulationFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ModelImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__ELEMENTS = eINSTANCE.getModel_Elements();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ElementImpl <em>Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ElementImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getElement()
     * @generated
     */
    EClass ELEMENT = eINSTANCE.getElement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ELEMENT__NAME = eINSTANCE.getElement_Name();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PathFormulaImpl <em>Path Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.PathFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPathFormula()
     * @generated
     */
    EClass PATH_FORMULA = eINSTANCE.getPathFormula();

    /**
     * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PATH_FORMULA__FORMULA = eINSTANCE.getPathFormula_Formula();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.FormulaImpl <em>Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.FormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getFormula()
     * @generated
     */
    EClass FORMULA = eINSTANCE.getFormula();

    /**
     * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FORMULA__FORMULA = eINSTANCE.getFormula_Formula();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.LabelImpl <em>Label</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.LabelImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getLabel()
     * @generated
     */
    EClass LABEL = eINSTANCE.getLabel();

    /**
     * The meta object literal for the '<em><b>States</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LABEL__STATES = eINSTANCE.getLabel_States();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ConstantImpl <em>Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ConstantImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getConstant()
     * @generated
     */
    EClass CONSTANT = eINSTANCE.getConstant();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONSTANT__EXP = eINSTANCE.getConstant_Exp();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ActionImpl <em>Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ActionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getAction()
     * @generated
     */
    EClass ACTION = eINSTANCE.getAction();

    /**
     * The meta object literal for the '<em><b>Probability</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTION__PROBABILITY = eINSTANCE.getAction_Probability();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.StateConstantImpl <em>State Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.StateConstantImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getStateConstant()
     * @generated
     */
    EClass STATE_CONSTANT = eINSTANCE.getStateConstant();

    /**
     * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATE_CONSTANT__TRANSITIONS = eINSTANCE.getStateConstant_Transitions();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.TransitionImpl <em>Transition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.TransitionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getTransition()
     * @generated
     */
    EClass TRANSITION = eINSTANCE.getTransition();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TRANSITION__ACTION = eINSTANCE.getTransition_Action();

    /**
     * The meta object literal for the '<em><b>Next State</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TRANSITION__NEXT_STATE = eINSTANCE.getTransition_NextState();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ConfigurationImpl <em>Configuration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ConfigurationImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getConfiguration()
     * @generated
     */
    EClass CONFIGURATION = eINSTANCE.getConfiguration();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONFIGURATION__ELEMENTS = eINSTANCE.getConfiguration_Elements();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PopulationElementImpl <em>Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationElementImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPopulationElement()
     * @generated
     */
    EClass POPULATION_ELEMENT = eINSTANCE.getPopulationElement();

    /**
     * The meta object literal for the '<em><b>State</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POPULATION_ELEMENT__STATE = eINSTANCE.getPopulationElement_State();

    /**
     * The meta object literal for the '<em><b>Has Size</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute POPULATION_ELEMENT__HAS_SIZE = eINSTANCE.getPopulationElement_HasSize();

    /**
     * The meta object literal for the '<em><b>Size</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POPULATION_ELEMENT__SIZE = eINSTANCE.getPopulationElement_Size();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.LiteralExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getLiteralExpression()
     * @generated
     */
    EClass LITERAL_EXPRESSION = eINSTANCE.getLiteralExpression();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LITERAL_EXPRESSION__REF = eINSTANCE.getLiteralExpression_Ref();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ModExpressionImpl <em>Mod Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ModExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getModExpression()
     * @generated
     */
    EClass MOD_EXPRESSION = eINSTANCE.getModExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MOD_EXPRESSION__ARG = eINSTANCE.getModExpression_Arg();

    /**
     * The meta object literal for the '<em><b>Mod</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MOD_EXPRESSION__MOD = eINSTANCE.getModExpression_Mod();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.LogExpressionImpl <em>Log Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.LogExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getLogExpression()
     * @generated
     */
    EClass LOG_EXPRESSION = eINSTANCE.getLogExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LOG_EXPRESSION__ARG = eINSTANCE.getLogExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PowExpressionImpl <em>Pow Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.PowExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPowExpression()
     * @generated
     */
    EClass POW_EXPRESSION = eINSTANCE.getPowExpression();

    /**
     * The meta object literal for the '<em><b>Base</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POW_EXPRESSION__BASE = eINSTANCE.getPowExpression_Base();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POW_EXPRESSION__EXP = eINSTANCE.getPowExpression_Exp();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.FloorExpressionImpl <em>Floor Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.FloorExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getFloorExpression()
     * @generated
     */
    EClass FLOOR_EXPRESSION = eINSTANCE.getFloorExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FLOOR_EXPRESSION__ARG = eINSTANCE.getFloorExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.CeilExpressionImpl <em>Ceil Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.CeilExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getCeilExpression()
     * @generated
     */
    EClass CEIL_EXPRESSION = eINSTANCE.getCeilExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CEIL_EXPRESSION__ARG = eINSTANCE.getCeilExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.MinExpressionImpl <em>Min Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.MinExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getMinExpression()
     * @generated
     */
    EClass MIN_EXPRESSION = eINSTANCE.getMinExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MIN_EXPRESSION__LEFT = eINSTANCE.getMinExpression_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MIN_EXPRESSION__RIGHT = eINSTANCE.getMinExpression_Right();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.MaxExpressionImpl <em>Max Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.MaxExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getMaxExpression()
     * @generated
     */
    EClass MAX_EXPRESSION = eINSTANCE.getMaxExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAX_EXPRESSION__LEFT = eINSTANCE.getMaxExpression_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAX_EXPRESSION__RIGHT = eINSTANCE.getMaxExpression_Right();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.SinExpressionImpl <em>Sin Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.SinExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getSinExpression()
     * @generated
     */
    EClass SIN_EXPRESSION = eINSTANCE.getSinExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIN_EXPRESSION__ARG = eINSTANCE.getSinExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.CosExpressionImpl <em>Cos Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.CosExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getCosExpression()
     * @generated
     */
    EClass COS_EXPRESSION = eINSTANCE.getCosExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COS_EXPRESSION__ARG = eINSTANCE.getCosExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.TanExpressionImpl <em>Tan Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.TanExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getTanExpression()
     * @generated
     */
    EClass TAN_EXPRESSION = eINSTANCE.getTanExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TAN_EXPRESSION__ARG = eINSTANCE.getTanExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ATanExpressionImpl <em>ATan Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ATanExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getATanExpression()
     * @generated
     */
    EClass ATAN_EXPRESSION = eINSTANCE.getATanExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ATAN_EXPRESSION__ARG = eINSTANCE.getATanExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ASinExpressionImpl <em>ASin Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ASinExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getASinExpression()
     * @generated
     */
    EClass ASIN_EXPRESSION = eINSTANCE.getASinExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASIN_EXPRESSION__ARG = eINSTANCE.getASinExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ACosExpressionImpl <em>ACos Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ACosExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getACosExpression()
     * @generated
     */
    EClass ACOS_EXPRESSION = eINSTANCE.getACosExpression();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACOS_EXPRESSION__ARG = eINSTANCE.getACosExpression_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PopulationExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPopulationExpression()
     * @generated
     */
    EClass POPULATION_EXPRESSION = eINSTANCE.getPopulationExpression();

    /**
     * The meta object literal for the '<em><b>State</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POPULATION_EXPRESSION__STATE = eINSTANCE.getPopulationExpression_State();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NumberExpressionImpl <em>Number Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.NumberExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNumberExpression()
     * @generated
     */
    EClass NUMBER_EXPRESSION = eINSTANCE.getNumberExpression();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NumberLiteralImpl <em>Number Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.NumberLiteralImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNumberLiteral()
     * @generated
     */
    EClass NUMBER_LITERAL = eINSTANCE.getNumberLiteral();

    /**
     * The meta object literal for the '<em><b>Int Part</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NUMBER_LITERAL__INT_PART = eINSTANCE.getNumberLiteral_IntPart();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ReferenceableNameImpl <em>Referenceable Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ReferenceableNameImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getReferenceableName()
     * @generated
     */
    EClass REFERENCEABLE_NAME = eINSTANCE.getReferenceableName();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.ProbabilityFormulaImpl <em>Probability Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.ProbabilityFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getProbabilityFormula()
     * @generated
     */
    EClass PROBABILITY_FORMULA = eINSTANCE.getProbabilityFormula();

    /**
     * The meta object literal for the '<em><b>Rel</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROBABILITY_FORMULA__REL = eINSTANCE.getProbabilityFormula_Rel();

    /**
     * The meta object literal for the '<em><b>PBound</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROBABILITY_FORMULA__PBOUND = eINSTANCE.getProbabilityFormula_PBound();

    /**
     * The meta object literal for the '<em><b>Path</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROBABILITY_FORMULA__PATH = eINSTANCE.getProbabilityFormula_Path();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.PctlPathFormulaImpl <em>Pctl Path Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.PctlPathFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getPctlPathFormula()
     * @generated
     */
    EClass PCTL_PATH_FORMULA = eINSTANCE.getPctlPathFormula();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NamedPctlPathFormulaImpl <em>Named Pctl Path Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.NamedPctlPathFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNamedPctlPathFormula()
     * @generated
     */
    EClass NAMED_PCTL_PATH_FORMULA = eINSTANCE.getNamedPctlPathFormula();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NAMED_PCTL_PATH_FORMULA__NAME = eINSTANCE.getNamedPctlPathFormula_Name();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NextFormulaImpl <em>Next Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.NextFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNextFormula()
     * @generated
     */
    EClass NEXT_FORMULA = eINSTANCE.getNextFormula();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NEXT_FORMULA__ARG = eINSTANCE.getNextFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.UntilFormulaImpl <em>Until Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.UntilFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getUntilFormula()
     * @generated
     */
    EClass UNTIL_FORMULA = eINSTANCE.getUntilFormula();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNTIL_FORMULA__LEFT = eINSTANCE.getUntilFormula_Left();

    /**
     * The meta object literal for the '<em><b>Is Bound</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNTIL_FORMULA__IS_BOUND = eINSTANCE.getUntilFormula_IsBound();

    /**
     * The meta object literal for the '<em><b>Bound</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNTIL_FORMULA__BOUND = eINSTANCE.getUntilFormula_Bound();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNTIL_FORMULA__RIGHT = eINSTANCE.getUntilFormula_Right();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.FalseFormulaImpl <em>False Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.FalseFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getFalseFormula()
     * @generated
     */
    EClass FALSE_FORMULA = eINSTANCE.getFalseFormula();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.TrueFormulaImpl <em>True Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.TrueFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getTrueFormula()
     * @generated
     */
    EClass TRUE_FORMULA = eINSTANCE.getTrueFormula();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.NotFormulaImpl <em>Not Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.NotFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getNotFormula()
     * @generated
     */
    EClass NOT_FORMULA = eINSTANCE.getNotFormula();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOT_FORMULA__ARG = eINSTANCE.getNotFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.RelationExpressionImpl <em>Relation Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.RelationExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getRelationExpression()
     * @generated
     */
    EClass RELATION_EXPRESSION = eINSTANCE.getRelationExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION_EXPRESSION__LEFT = eINSTANCE.getRelationExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATION_EXPRESSION__OP = eINSTANCE.getRelationExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION_EXPRESSION__RIGHT = eINSTANCE.getRelationExpression_Right();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.SumDiffExpressionImpl <em>Sum Diff Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.SumDiffExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getSumDiffExpression()
     * @generated
     */
    EClass SUM_DIFF_EXPRESSION = eINSTANCE.getSumDiffExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUM_DIFF_EXPRESSION__LEFT = eINSTANCE.getSumDiffExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SUM_DIFF_EXPRESSION__OP = eINSTANCE.getSumDiffExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUM_DIFF_EXPRESSION__RIGHT = eINSTANCE.getSumDiffExpression_Right();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.MulDivExpressionImpl <em>Mul Div Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.MulDivExpressionImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getMulDivExpression()
     * @generated
     */
    EClass MUL_DIV_EXPRESSION = eINSTANCE.getMulDivExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MUL_DIV_EXPRESSION__LEFT = eINSTANCE.getMulDivExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MUL_DIV_EXPRESSION__OP = eINSTANCE.getMulDivExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MUL_DIV_EXPRESSION__RIGHT = eINSTANCE.getMulDivExpression_Right();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.DecimalLiteralImpl <em>Decimal Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.DecimalLiteralImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getDecimalLiteral()
     * @generated
     */
    EClass DECIMAL_LITERAL = eINSTANCE.getDecimalLiteral();

    /**
     * The meta object literal for the '<em><b>Integer Part</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_LITERAL__INTEGER_PART = eINSTANCE.getDecimalLiteral_IntegerPart();

    /**
     * The meta object literal for the '<em><b>Decimal Part</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DECIMAL_LITERAL__DECIMAL_PART = eINSTANCE.getDecimalLiteral_DecimalPart();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.OrPctlFormulaImpl <em>Or Pctl Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.OrPctlFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getOrPctlFormula()
     * @generated
     */
    EClass OR_PCTL_FORMULA = eINSTANCE.getOrPctlFormula();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR_PCTL_FORMULA__LEFT = eINSTANCE.getOrPctlFormula_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR_PCTL_FORMULA__RIGHT = eINSTANCE.getOrPctlFormula_Right();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.impl.AndPctlFormulaImpl <em>And Pctl Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.impl.AndPctlFormulaImpl
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getAndPctlFormula()
     * @generated
     */
    EClass AND_PCTL_FORMULA = eINSTANCE.getAndPctlFormula();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND_PCTL_FORMULA__LEFT = eINSTANCE.getAndPctlFormula_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND_PCTL_FORMULA__RIGHT = eINSTANCE.getAndPctlFormula_Right();

    /**
     * The meta object literal for the '{@link org.cmg.ml.sam.xtext.population.population.RelationSymbol <em>Relation Symbol</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.ml.sam.xtext.population.population.RelationSymbol
     * @see org.cmg.ml.sam.xtext.population.population.impl.PopulationPackageImpl#getRelationSymbol()
     * @generated
     */
    EEnum RELATION_SYMBOL = eINSTANCE.getRelationSymbol();

  }

} //PopulationPackage
