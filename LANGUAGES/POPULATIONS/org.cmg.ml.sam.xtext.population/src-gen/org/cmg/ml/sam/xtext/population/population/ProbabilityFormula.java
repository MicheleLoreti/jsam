/**
 */
package org.cmg.ml.sam.xtext.population.population;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Probability Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getRel <em>Rel</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getPBound <em>PBound</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getPath <em>Path</em>}</li>
 * </ul>
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getProbabilityFormula()
 * @model
 * @generated
 */
public interface ProbabilityFormula extends Expression
{
  /**
   * Returns the value of the '<em><b>Rel</b></em>' attribute.
   * The literals are from the enumeration {@link org.cmg.ml.sam.xtext.population.population.RelationSymbol}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rel</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rel</em>' attribute.
   * @see org.cmg.ml.sam.xtext.population.population.RelationSymbol
   * @see #setRel(RelationSymbol)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getProbabilityFormula_Rel()
   * @model
   * @generated
   */
  RelationSymbol getRel();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getRel <em>Rel</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rel</em>' attribute.
   * @see org.cmg.ml.sam.xtext.population.population.RelationSymbol
   * @see #getRel()
   * @generated
   */
  void setRel(RelationSymbol value);

  /**
   * Returns the value of the '<em><b>PBound</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PBound</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PBound</em>' containment reference.
   * @see #setPBound(NumberExpression)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getProbabilityFormula_PBound()
   * @model containment="true"
   * @generated
   */
  NumberExpression getPBound();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getPBound <em>PBound</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PBound</em>' containment reference.
   * @see #getPBound()
   * @generated
   */
  void setPBound(NumberExpression value);

  /**
   * Returns the value of the '<em><b>Path</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Path</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Path</em>' containment reference.
   * @see #setPath(PctlPathFormula)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getProbabilityFormula_Path()
   * @model containment="true"
   * @generated
   */
  PctlPathFormula getPath();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.ProbabilityFormula#getPath <em>Path</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Path</em>' containment reference.
   * @see #getPath()
   * @generated
   */
  void setPath(PctlPathFormula value);

} // ProbabilityFormula
