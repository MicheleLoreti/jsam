/**
 */
package org.cmg.ml.sam.xtext.population.population;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referenceable Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getReferenceableName()
 * @model
 * @generated
 */
public interface ReferenceableName extends EObject
{
} // ReferenceableName
