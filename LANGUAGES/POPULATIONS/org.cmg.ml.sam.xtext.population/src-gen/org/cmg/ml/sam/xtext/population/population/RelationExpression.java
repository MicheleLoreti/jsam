/**
 */
package org.cmg.ml.sam.xtext.population.population;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relation Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getLeft <em>Left</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getOp <em>Op</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getRelationExpression()
 * @model
 * @generated
 */
public interface RelationExpression extends Expression
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(Expression)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getRelationExpression_Left()
   * @model containment="true"
   * @generated
   */
  Expression getLeft();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(Expression value);

  /**
   * Returns the value of the '<em><b>Op</b></em>' attribute.
   * The literals are from the enumeration {@link org.cmg.ml.sam.xtext.population.population.RelationSymbol}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' attribute.
   * @see org.cmg.ml.sam.xtext.population.population.RelationSymbol
   * @see #setOp(RelationSymbol)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getRelationExpression_Op()
   * @model
   * @generated
   */
  RelationSymbol getOp();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getOp <em>Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op</em>' attribute.
   * @see org.cmg.ml.sam.xtext.population.population.RelationSymbol
   * @see #getOp()
   * @generated
   */
  void setOp(RelationSymbol value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(Expression)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getRelationExpression_Right()
   * @model containment="true"
   * @generated
   */
  Expression getRight();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.RelationExpression#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(Expression value);

} // RelationExpression
