/**
 */
package org.cmg.ml.sam.xtext.population.population;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>True Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getTrueFormula()
 * @model
 * @generated
 */
public interface TrueFormula extends Expression
{
} // TrueFormula
