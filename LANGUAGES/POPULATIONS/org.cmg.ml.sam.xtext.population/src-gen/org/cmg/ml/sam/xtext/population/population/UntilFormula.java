/**
 */
package org.cmg.ml.sam.xtext.population.population;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Until Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getLeft <em>Left</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#isIsBound <em>Is Bound</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getBound <em>Bound</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getUntilFormula()
 * @model
 * @generated
 */
public interface UntilFormula extends PctlPathFormula
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(Expression)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getUntilFormula_Left()
   * @model containment="true"
   * @generated
   */
  Expression getLeft();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(Expression value);

  /**
   * Returns the value of the '<em><b>Is Bound</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Is Bound</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Is Bound</em>' attribute.
   * @see #setIsBound(boolean)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getUntilFormula_IsBound()
   * @model
   * @generated
   */
  boolean isIsBound();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#isIsBound <em>Is Bound</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Is Bound</em>' attribute.
   * @see #isIsBound()
   * @generated
   */
  void setIsBound(boolean value);

  /**
   * Returns the value of the '<em><b>Bound</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bound</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bound</em>' attribute.
   * @see #setBound(int)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getUntilFormula_Bound()
   * @model
   * @generated
   */
  int getBound();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getBound <em>Bound</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bound</em>' attribute.
   * @see #getBound()
   * @generated
   */
  void setBound(int value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(Expression)
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#getUntilFormula_Right()
   * @model containment="true"
   * @generated
   */
  Expression getRight();

  /**
   * Sets the value of the '{@link org.cmg.ml.sam.xtext.population.population.UntilFormula#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(Expression value);

} // UntilFormula
