/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.FalseFormula;
import org.cmg.ml.sam.xtext.population.population.PopulationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>False Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FalseFormulaImpl extends ExpressionImpl implements FalseFormula
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FalseFormulaImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PopulationPackage.Literals.FALSE_FORMULA;
  }

} //FalseFormulaImpl
