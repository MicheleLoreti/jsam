/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.NumberExpression;
import org.cmg.ml.sam.xtext.population.population.PopulationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Number Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NumberExpressionImpl extends ExpressionImpl implements NumberExpression
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NumberExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PopulationPackage.Literals.NUMBER_EXPRESSION;
  }

} //NumberExpressionImpl
