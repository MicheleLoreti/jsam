/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.PctlPathFormula;
import org.cmg.ml.sam.xtext.population.population.PopulationPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pctl Path Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PctlPathFormulaImpl extends MinimalEObjectImpl.Container implements PctlPathFormula
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PctlPathFormulaImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PopulationPackage.Literals.PCTL_PATH_FORMULA;
  }

} //PctlPathFormulaImpl
