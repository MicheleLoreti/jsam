/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.Expression;
import org.cmg.ml.sam.xtext.population.population.PopulationElement;
import org.cmg.ml.sam.xtext.population.population.PopulationPackage;
import org.cmg.ml.sam.xtext.population.population.StateConstant;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.impl.PopulationElementImpl#getState <em>State</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.impl.PopulationElementImpl#isHasSize <em>Has Size</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.impl.PopulationElementImpl#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PopulationElementImpl extends MinimalEObjectImpl.Container implements PopulationElement
{
  /**
   * The cached value of the '{@link #getState() <em>State</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getState()
   * @generated
   * @ordered
   */
  protected StateConstant state;

  /**
   * The default value of the '{@link #isHasSize() <em>Has Size</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isHasSize()
   * @generated
   * @ordered
   */
  protected static final boolean HAS_SIZE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isHasSize() <em>Has Size</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isHasSize()
   * @generated
   * @ordered
   */
  protected boolean hasSize = HAS_SIZE_EDEFAULT;

  /**
   * The cached value of the '{@link #getSize() <em>Size</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSize()
   * @generated
   * @ordered
   */
  protected Expression size;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PopulationElementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PopulationPackage.Literals.POPULATION_ELEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StateConstant getState()
  {
    if (state != null && state.eIsProxy())
    {
      InternalEObject oldState = (InternalEObject)state;
      state = (StateConstant)eResolveProxy(oldState);
      if (state != oldState)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, PopulationPackage.POPULATION_ELEMENT__STATE, oldState, state));
      }
    }
    return state;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StateConstant basicGetState()
  {
    return state;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setState(StateConstant newState)
  {
    StateConstant oldState = state;
    state = newState;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PopulationPackage.POPULATION_ELEMENT__STATE, oldState, state));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isHasSize()
  {
    return hasSize;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHasSize(boolean newHasSize)
  {
    boolean oldHasSize = hasSize;
    hasSize = newHasSize;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PopulationPackage.POPULATION_ELEMENT__HAS_SIZE, oldHasSize, hasSize));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getSize()
  {
    return size;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSize(Expression newSize, NotificationChain msgs)
  {
    Expression oldSize = size;
    size = newSize;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PopulationPackage.POPULATION_ELEMENT__SIZE, oldSize, newSize);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSize(Expression newSize)
  {
    if (newSize != size)
    {
      NotificationChain msgs = null;
      if (size != null)
        msgs = ((InternalEObject)size).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PopulationPackage.POPULATION_ELEMENT__SIZE, null, msgs);
      if (newSize != null)
        msgs = ((InternalEObject)newSize).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PopulationPackage.POPULATION_ELEMENT__SIZE, null, msgs);
      msgs = basicSetSize(newSize, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PopulationPackage.POPULATION_ELEMENT__SIZE, newSize, newSize));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PopulationPackage.POPULATION_ELEMENT__SIZE:
        return basicSetSize(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PopulationPackage.POPULATION_ELEMENT__STATE:
        if (resolve) return getState();
        return basicGetState();
      case PopulationPackage.POPULATION_ELEMENT__HAS_SIZE:
        return isHasSize();
      case PopulationPackage.POPULATION_ELEMENT__SIZE:
        return getSize();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PopulationPackage.POPULATION_ELEMENT__STATE:
        setState((StateConstant)newValue);
        return;
      case PopulationPackage.POPULATION_ELEMENT__HAS_SIZE:
        setHasSize((Boolean)newValue);
        return;
      case PopulationPackage.POPULATION_ELEMENT__SIZE:
        setSize((Expression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PopulationPackage.POPULATION_ELEMENT__STATE:
        setState((StateConstant)null);
        return;
      case PopulationPackage.POPULATION_ELEMENT__HAS_SIZE:
        setHasSize(HAS_SIZE_EDEFAULT);
        return;
      case PopulationPackage.POPULATION_ELEMENT__SIZE:
        setSize((Expression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PopulationPackage.POPULATION_ELEMENT__STATE:
        return state != null;
      case PopulationPackage.POPULATION_ELEMENT__HAS_SIZE:
        return hasSize != HAS_SIZE_EDEFAULT;
      case PopulationPackage.POPULATION_ELEMENT__SIZE:
        return size != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (hasSize: ");
    result.append(hasSize);
    result.append(')');
    return result.toString();
  }

} //PopulationElementImpl
