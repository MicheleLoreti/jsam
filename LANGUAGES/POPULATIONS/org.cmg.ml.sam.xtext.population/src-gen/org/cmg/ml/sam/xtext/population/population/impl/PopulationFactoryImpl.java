/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PopulationFactoryImpl extends EFactoryImpl implements PopulationFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static PopulationFactory init()
  {
    try
    {
      PopulationFactory thePopulationFactory = (PopulationFactory)EPackage.Registry.INSTANCE.getEFactory(PopulationPackage.eNS_URI);
      if (thePopulationFactory != null)
      {
        return thePopulationFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new PopulationFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PopulationFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case PopulationPackage.MODEL: return createModel();
      case PopulationPackage.ELEMENT: return createElement();
      case PopulationPackage.PATH_FORMULA: return createPathFormula();
      case PopulationPackage.FORMULA: return createFormula();
      case PopulationPackage.LABEL: return createLabel();
      case PopulationPackage.CONSTANT: return createConstant();
      case PopulationPackage.ACTION: return createAction();
      case PopulationPackage.STATE_CONSTANT: return createStateConstant();
      case PopulationPackage.TRANSITION: return createTransition();
      case PopulationPackage.CONFIGURATION: return createConfiguration();
      case PopulationPackage.POPULATION_ELEMENT: return createPopulationElement();
      case PopulationPackage.EXPRESSION: return createExpression();
      case PopulationPackage.LITERAL_EXPRESSION: return createLiteralExpression();
      case PopulationPackage.MOD_EXPRESSION: return createModExpression();
      case PopulationPackage.LOG_EXPRESSION: return createLogExpression();
      case PopulationPackage.POW_EXPRESSION: return createPowExpression();
      case PopulationPackage.FLOOR_EXPRESSION: return createFloorExpression();
      case PopulationPackage.CEIL_EXPRESSION: return createCeilExpression();
      case PopulationPackage.MIN_EXPRESSION: return createMinExpression();
      case PopulationPackage.MAX_EXPRESSION: return createMaxExpression();
      case PopulationPackage.SIN_EXPRESSION: return createSinExpression();
      case PopulationPackage.COS_EXPRESSION: return createCosExpression();
      case PopulationPackage.TAN_EXPRESSION: return createTanExpression();
      case PopulationPackage.ATAN_EXPRESSION: return createATanExpression();
      case PopulationPackage.ASIN_EXPRESSION: return createASinExpression();
      case PopulationPackage.ACOS_EXPRESSION: return createACosExpression();
      case PopulationPackage.POPULATION_EXPRESSION: return createPopulationExpression();
      case PopulationPackage.NUMBER_EXPRESSION: return createNumberExpression();
      case PopulationPackage.NUMBER_LITERAL: return createNumberLiteral();
      case PopulationPackage.REFERENCEABLE_NAME: return createReferenceableName();
      case PopulationPackage.PROBABILITY_FORMULA: return createProbabilityFormula();
      case PopulationPackage.PCTL_PATH_FORMULA: return createPctlPathFormula();
      case PopulationPackage.NAMED_PCTL_PATH_FORMULA: return createNamedPctlPathFormula();
      case PopulationPackage.NEXT_FORMULA: return createNextFormula();
      case PopulationPackage.UNTIL_FORMULA: return createUntilFormula();
      case PopulationPackage.FALSE_FORMULA: return createFalseFormula();
      case PopulationPackage.TRUE_FORMULA: return createTrueFormula();
      case PopulationPackage.NOT_FORMULA: return createNotFormula();
      case PopulationPackage.RELATION_EXPRESSION: return createRelationExpression();
      case PopulationPackage.SUM_DIFF_EXPRESSION: return createSumDiffExpression();
      case PopulationPackage.MUL_DIV_EXPRESSION: return createMulDivExpression();
      case PopulationPackage.DECIMAL_LITERAL: return createDecimalLiteral();
      case PopulationPackage.OR_PCTL_FORMULA: return createOrPctlFormula();
      case PopulationPackage.AND_PCTL_FORMULA: return createAndPctlFormula();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case PopulationPackage.RELATION_SYMBOL:
        return createRelationSymbolFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case PopulationPackage.RELATION_SYMBOL:
        return convertRelationSymbolToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Element createElement()
  {
    ElementImpl element = new ElementImpl();
    return element;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PathFormula createPathFormula()
  {
    PathFormulaImpl pathFormula = new PathFormulaImpl();
    return pathFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Formula createFormula()
  {
    FormulaImpl formula = new FormulaImpl();
    return formula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Label createLabel()
  {
    LabelImpl label = new LabelImpl();
    return label;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Constant createConstant()
  {
    ConstantImpl constant = new ConstantImpl();
    return constant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action createAction()
  {
    ActionImpl action = new ActionImpl();
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StateConstant createStateConstant()
  {
    StateConstantImpl stateConstant = new StateConstantImpl();
    return stateConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Transition createTransition()
  {
    TransitionImpl transition = new TransitionImpl();
    return transition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Configuration createConfiguration()
  {
    ConfigurationImpl configuration = new ConfigurationImpl();
    return configuration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PopulationElement createPopulationElement()
  {
    PopulationElementImpl populationElement = new PopulationElementImpl();
    return populationElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LiteralExpression createLiteralExpression()
  {
    LiteralExpressionImpl literalExpression = new LiteralExpressionImpl();
    return literalExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModExpression createModExpression()
  {
    ModExpressionImpl modExpression = new ModExpressionImpl();
    return modExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogExpression createLogExpression()
  {
    LogExpressionImpl logExpression = new LogExpressionImpl();
    return logExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PowExpression createPowExpression()
  {
    PowExpressionImpl powExpression = new PowExpressionImpl();
    return powExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FloorExpression createFloorExpression()
  {
    FloorExpressionImpl floorExpression = new FloorExpressionImpl();
    return floorExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CeilExpression createCeilExpression()
  {
    CeilExpressionImpl ceilExpression = new CeilExpressionImpl();
    return ceilExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MinExpression createMinExpression()
  {
    MinExpressionImpl minExpression = new MinExpressionImpl();
    return minExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MaxExpression createMaxExpression()
  {
    MaxExpressionImpl maxExpression = new MaxExpressionImpl();
    return maxExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SinExpression createSinExpression()
  {
    SinExpressionImpl sinExpression = new SinExpressionImpl();
    return sinExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CosExpression createCosExpression()
  {
    CosExpressionImpl cosExpression = new CosExpressionImpl();
    return cosExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TanExpression createTanExpression()
  {
    TanExpressionImpl tanExpression = new TanExpressionImpl();
    return tanExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ATanExpression createATanExpression()
  {
    ATanExpressionImpl aTanExpression = new ATanExpressionImpl();
    return aTanExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ASinExpression createASinExpression()
  {
    ASinExpressionImpl aSinExpression = new ASinExpressionImpl();
    return aSinExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ACosExpression createACosExpression()
  {
    ACosExpressionImpl aCosExpression = new ACosExpressionImpl();
    return aCosExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PopulationExpression createPopulationExpression()
  {
    PopulationExpressionImpl populationExpression = new PopulationExpressionImpl();
    return populationExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NumberExpression createNumberExpression()
  {
    NumberExpressionImpl numberExpression = new NumberExpressionImpl();
    return numberExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NumberLiteral createNumberLiteral()
  {
    NumberLiteralImpl numberLiteral = new NumberLiteralImpl();
    return numberLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferenceableName createReferenceableName()
  {
    ReferenceableNameImpl referenceableName = new ReferenceableNameImpl();
    return referenceableName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProbabilityFormula createProbabilityFormula()
  {
    ProbabilityFormulaImpl probabilityFormula = new ProbabilityFormulaImpl();
    return probabilityFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PctlPathFormula createPctlPathFormula()
  {
    PctlPathFormulaImpl pctlPathFormula = new PctlPathFormulaImpl();
    return pctlPathFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NamedPctlPathFormula createNamedPctlPathFormula()
  {
    NamedPctlPathFormulaImpl namedPctlPathFormula = new NamedPctlPathFormulaImpl();
    return namedPctlPathFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NextFormula createNextFormula()
  {
    NextFormulaImpl nextFormula = new NextFormulaImpl();
    return nextFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UntilFormula createUntilFormula()
  {
    UntilFormulaImpl untilFormula = new UntilFormulaImpl();
    return untilFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FalseFormula createFalseFormula()
  {
    FalseFormulaImpl falseFormula = new FalseFormulaImpl();
    return falseFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TrueFormula createTrueFormula()
  {
    TrueFormulaImpl trueFormula = new TrueFormulaImpl();
    return trueFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotFormula createNotFormula()
  {
    NotFormulaImpl notFormula = new NotFormulaImpl();
    return notFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RelationExpression createRelationExpression()
  {
    RelationExpressionImpl relationExpression = new RelationExpressionImpl();
    return relationExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SumDiffExpression createSumDiffExpression()
  {
    SumDiffExpressionImpl sumDiffExpression = new SumDiffExpressionImpl();
    return sumDiffExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MulDivExpression createMulDivExpression()
  {
    MulDivExpressionImpl mulDivExpression = new MulDivExpressionImpl();
    return mulDivExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DecimalLiteral createDecimalLiteral()
  {
    DecimalLiteralImpl decimalLiteral = new DecimalLiteralImpl();
    return decimalLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrPctlFormula createOrPctlFormula()
  {
    OrPctlFormulaImpl orPctlFormula = new OrPctlFormulaImpl();
    return orPctlFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AndPctlFormula createAndPctlFormula()
  {
    AndPctlFormulaImpl andPctlFormula = new AndPctlFormulaImpl();
    return andPctlFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RelationSymbol createRelationSymbolFromString(EDataType eDataType, String initialValue)
  {
    RelationSymbol result = RelationSymbol.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertRelationSymbolToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PopulationPackage getPopulationPackage()
  {
    return (PopulationPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static PopulationPackage getPackage()
  {
    return PopulationPackage.eINSTANCE;
  }

} //PopulationFactoryImpl
