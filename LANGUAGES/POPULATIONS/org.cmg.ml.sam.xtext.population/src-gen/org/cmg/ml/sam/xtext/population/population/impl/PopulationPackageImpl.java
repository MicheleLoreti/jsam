/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.ACosExpression;
import org.cmg.ml.sam.xtext.population.population.ASinExpression;
import org.cmg.ml.sam.xtext.population.population.ATanExpression;
import org.cmg.ml.sam.xtext.population.population.Action;
import org.cmg.ml.sam.xtext.population.population.AndPctlFormula;
import org.cmg.ml.sam.xtext.population.population.CeilExpression;
import org.cmg.ml.sam.xtext.population.population.Configuration;
import org.cmg.ml.sam.xtext.population.population.Constant;
import org.cmg.ml.sam.xtext.population.population.CosExpression;
import org.cmg.ml.sam.xtext.population.population.DecimalLiteral;
import org.cmg.ml.sam.xtext.population.population.Element;
import org.cmg.ml.sam.xtext.population.population.Expression;
import org.cmg.ml.sam.xtext.population.population.FalseFormula;
import org.cmg.ml.sam.xtext.population.population.FloorExpression;
import org.cmg.ml.sam.xtext.population.population.Formula;
import org.cmg.ml.sam.xtext.population.population.Label;
import org.cmg.ml.sam.xtext.population.population.LiteralExpression;
import org.cmg.ml.sam.xtext.population.population.LogExpression;
import org.cmg.ml.sam.xtext.population.population.MaxExpression;
import org.cmg.ml.sam.xtext.population.population.MinExpression;
import org.cmg.ml.sam.xtext.population.population.ModExpression;
import org.cmg.ml.sam.xtext.population.population.Model;
import org.cmg.ml.sam.xtext.population.population.MulDivExpression;
import org.cmg.ml.sam.xtext.population.population.NamedPctlPathFormula;
import org.cmg.ml.sam.xtext.population.population.NextFormula;
import org.cmg.ml.sam.xtext.population.population.NotFormula;
import org.cmg.ml.sam.xtext.population.population.NumberExpression;
import org.cmg.ml.sam.xtext.population.population.NumberLiteral;
import org.cmg.ml.sam.xtext.population.population.OrPctlFormula;
import org.cmg.ml.sam.xtext.population.population.PathFormula;
import org.cmg.ml.sam.xtext.population.population.PctlPathFormula;
import org.cmg.ml.sam.xtext.population.population.PopulationElement;
import org.cmg.ml.sam.xtext.population.population.PopulationExpression;
import org.cmg.ml.sam.xtext.population.population.PopulationFactory;
import org.cmg.ml.sam.xtext.population.population.PopulationPackage;
import org.cmg.ml.sam.xtext.population.population.PowExpression;
import org.cmg.ml.sam.xtext.population.population.ProbabilityFormula;
import org.cmg.ml.sam.xtext.population.population.ReferenceableName;
import org.cmg.ml.sam.xtext.population.population.RelationExpression;
import org.cmg.ml.sam.xtext.population.population.RelationSymbol;
import org.cmg.ml.sam.xtext.population.population.SinExpression;
import org.cmg.ml.sam.xtext.population.population.StateConstant;
import org.cmg.ml.sam.xtext.population.population.SumDiffExpression;
import org.cmg.ml.sam.xtext.population.population.TanExpression;
import org.cmg.ml.sam.xtext.population.population.Transition;
import org.cmg.ml.sam.xtext.population.population.TrueFormula;
import org.cmg.ml.sam.xtext.population.population.UntilFormula;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PopulationPackageImpl extends EPackageImpl implements PopulationPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pathFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass formulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass labelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constantEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass actionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stateConstantEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass transitionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass configurationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass populationElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass literalExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass logExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass powExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass floorExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ceilExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass minExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass maxExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sinExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass cosExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tanExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass aTanExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass aSinExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass aCosExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass populationExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numberExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numberLiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass referenceableNameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass probabilityFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pctlPathFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass namedPctlPathFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nextFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass untilFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass falseFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass trueFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass notFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass relationExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sumDiffExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mulDivExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass decimalLiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass orPctlFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass andPctlFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum relationSymbolEEnum = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private PopulationPackageImpl()
  {
    super(eNS_URI, PopulationFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link PopulationPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static PopulationPackage init()
  {
    if (isInited) return (PopulationPackage)EPackage.Registry.INSTANCE.getEPackage(PopulationPackage.eNS_URI);

    // Obtain or create and register package
    PopulationPackageImpl thePopulationPackage = (PopulationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PopulationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PopulationPackageImpl());

    isInited = true;

    // Create package meta-data objects
    thePopulationPackage.createPackageContents();

    // Initialize created meta-data
    thePopulationPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    thePopulationPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(PopulationPackage.eNS_URI, thePopulationPackage);
    return thePopulationPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModel()
  {
    return modelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_Elements()
  {
    return (EReference)modelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getElement()
  {
    return elementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getElement_Name()
  {
    return (EAttribute)elementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPathFormula()
  {
    return pathFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPathFormula_Formula()
  {
    return (EReference)pathFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFormula()
  {
    return formulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFormula_Formula()
  {
    return (EReference)formulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLabel()
  {
    return labelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLabel_States()
  {
    return (EReference)labelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstant()
  {
    return constantEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstant_Exp()
  {
    return (EReference)constantEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAction()
  {
    return actionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAction_Probability()
  {
    return (EReference)actionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStateConstant()
  {
    return stateConstantEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStateConstant_Transitions()
  {
    return (EReference)stateConstantEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTransition()
  {
    return transitionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTransition_Action()
  {
    return (EReference)transitionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTransition_NextState()
  {
    return (EReference)transitionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConfiguration()
  {
    return configurationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfiguration_Elements()
  {
    return (EReference)configurationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPopulationElement()
  {
    return populationElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPopulationElement_State()
  {
    return (EReference)populationElementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPopulationElement_HasSize()
  {
    return (EAttribute)populationElementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPopulationElement_Size()
  {
    return (EReference)populationElementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpression()
  {
    return expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLiteralExpression()
  {
    return literalExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLiteralExpression_Ref()
  {
    return (EReference)literalExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModExpression()
  {
    return modExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModExpression_Arg()
  {
    return (EReference)modExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModExpression_Mod()
  {
    return (EReference)modExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLogExpression()
  {
    return logExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLogExpression_Arg()
  {
    return (EReference)logExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPowExpression()
  {
    return powExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPowExpression_Base()
  {
    return (EReference)powExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPowExpression_Exp()
  {
    return (EReference)powExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFloorExpression()
  {
    return floorExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFloorExpression_Arg()
  {
    return (EReference)floorExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCeilExpression()
  {
    return ceilExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCeilExpression_Arg()
  {
    return (EReference)ceilExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMinExpression()
  {
    return minExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMinExpression_Left()
  {
    return (EReference)minExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMinExpression_Right()
  {
    return (EReference)minExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMaxExpression()
  {
    return maxExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMaxExpression_Left()
  {
    return (EReference)maxExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMaxExpression_Right()
  {
    return (EReference)maxExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSinExpression()
  {
    return sinExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSinExpression_Arg()
  {
    return (EReference)sinExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCosExpression()
  {
    return cosExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCosExpression_Arg()
  {
    return (EReference)cosExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTanExpression()
  {
    return tanExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTanExpression_Arg()
  {
    return (EReference)tanExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getATanExpression()
  {
    return aTanExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getATanExpression_Arg()
  {
    return (EReference)aTanExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getASinExpression()
  {
    return aSinExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getASinExpression_Arg()
  {
    return (EReference)aSinExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getACosExpression()
  {
    return aCosExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getACosExpression_Arg()
  {
    return (EReference)aCosExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPopulationExpression()
  {
    return populationExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPopulationExpression_State()
  {
    return (EReference)populationExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNumberExpression()
  {
    return numberExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNumberLiteral()
  {
    return numberLiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNumberLiteral_IntPart()
  {
    return (EAttribute)numberLiteralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReferenceableName()
  {
    return referenceableNameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProbabilityFormula()
  {
    return probabilityFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getProbabilityFormula_Rel()
  {
    return (EAttribute)probabilityFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProbabilityFormula_PBound()
  {
    return (EReference)probabilityFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProbabilityFormula_Path()
  {
    return (EReference)probabilityFormulaEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPctlPathFormula()
  {
    return pctlPathFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNamedPctlPathFormula()
  {
    return namedPctlPathFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNamedPctlPathFormula_Name()
  {
    return (EReference)namedPctlPathFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNextFormula()
  {
    return nextFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNextFormula_Arg()
  {
    return (EReference)nextFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUntilFormula()
  {
    return untilFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUntilFormula_Left()
  {
    return (EReference)untilFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getUntilFormula_IsBound()
  {
    return (EAttribute)untilFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getUntilFormula_Bound()
  {
    return (EAttribute)untilFormulaEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUntilFormula_Right()
  {
    return (EReference)untilFormulaEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFalseFormula()
  {
    return falseFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTrueFormula()
  {
    return trueFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNotFormula()
  {
    return notFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNotFormula_Arg()
  {
    return (EReference)notFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRelationExpression()
  {
    return relationExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRelationExpression_Left()
  {
    return (EReference)relationExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRelationExpression_Op()
  {
    return (EAttribute)relationExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRelationExpression_Right()
  {
    return (EReference)relationExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSumDiffExpression()
  {
    return sumDiffExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSumDiffExpression_Left()
  {
    return (EReference)sumDiffExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSumDiffExpression_Op()
  {
    return (EAttribute)sumDiffExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSumDiffExpression_Right()
  {
    return (EReference)sumDiffExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMulDivExpression()
  {
    return mulDivExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMulDivExpression_Left()
  {
    return (EReference)mulDivExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMulDivExpression_Op()
  {
    return (EAttribute)mulDivExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMulDivExpression_Right()
  {
    return (EReference)mulDivExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDecimalLiteral()
  {
    return decimalLiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDecimalLiteral_IntegerPart()
  {
    return (EReference)decimalLiteralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDecimalLiteral_DecimalPart()
  {
    return (EAttribute)decimalLiteralEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOrPctlFormula()
  {
    return orPctlFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOrPctlFormula_Left()
  {
    return (EReference)orPctlFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOrPctlFormula_Right()
  {
    return (EReference)orPctlFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAndPctlFormula()
  {
    return andPctlFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAndPctlFormula_Left()
  {
    return (EReference)andPctlFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAndPctlFormula_Right()
  {
    return (EReference)andPctlFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getRelationSymbol()
  {
    return relationSymbolEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PopulationFactory getPopulationFactory()
  {
    return (PopulationFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    modelEClass = createEClass(MODEL);
    createEReference(modelEClass, MODEL__ELEMENTS);

    elementEClass = createEClass(ELEMENT);
    createEAttribute(elementEClass, ELEMENT__NAME);

    pathFormulaEClass = createEClass(PATH_FORMULA);
    createEReference(pathFormulaEClass, PATH_FORMULA__FORMULA);

    formulaEClass = createEClass(FORMULA);
    createEReference(formulaEClass, FORMULA__FORMULA);

    labelEClass = createEClass(LABEL);
    createEReference(labelEClass, LABEL__STATES);

    constantEClass = createEClass(CONSTANT);
    createEReference(constantEClass, CONSTANT__EXP);

    actionEClass = createEClass(ACTION);
    createEReference(actionEClass, ACTION__PROBABILITY);

    stateConstantEClass = createEClass(STATE_CONSTANT);
    createEReference(stateConstantEClass, STATE_CONSTANT__TRANSITIONS);

    transitionEClass = createEClass(TRANSITION);
    createEReference(transitionEClass, TRANSITION__ACTION);
    createEReference(transitionEClass, TRANSITION__NEXT_STATE);

    configurationEClass = createEClass(CONFIGURATION);
    createEReference(configurationEClass, CONFIGURATION__ELEMENTS);

    populationElementEClass = createEClass(POPULATION_ELEMENT);
    createEReference(populationElementEClass, POPULATION_ELEMENT__STATE);
    createEAttribute(populationElementEClass, POPULATION_ELEMENT__HAS_SIZE);
    createEReference(populationElementEClass, POPULATION_ELEMENT__SIZE);

    expressionEClass = createEClass(EXPRESSION);

    literalExpressionEClass = createEClass(LITERAL_EXPRESSION);
    createEReference(literalExpressionEClass, LITERAL_EXPRESSION__REF);

    modExpressionEClass = createEClass(MOD_EXPRESSION);
    createEReference(modExpressionEClass, MOD_EXPRESSION__ARG);
    createEReference(modExpressionEClass, MOD_EXPRESSION__MOD);

    logExpressionEClass = createEClass(LOG_EXPRESSION);
    createEReference(logExpressionEClass, LOG_EXPRESSION__ARG);

    powExpressionEClass = createEClass(POW_EXPRESSION);
    createEReference(powExpressionEClass, POW_EXPRESSION__BASE);
    createEReference(powExpressionEClass, POW_EXPRESSION__EXP);

    floorExpressionEClass = createEClass(FLOOR_EXPRESSION);
    createEReference(floorExpressionEClass, FLOOR_EXPRESSION__ARG);

    ceilExpressionEClass = createEClass(CEIL_EXPRESSION);
    createEReference(ceilExpressionEClass, CEIL_EXPRESSION__ARG);

    minExpressionEClass = createEClass(MIN_EXPRESSION);
    createEReference(minExpressionEClass, MIN_EXPRESSION__LEFT);
    createEReference(minExpressionEClass, MIN_EXPRESSION__RIGHT);

    maxExpressionEClass = createEClass(MAX_EXPRESSION);
    createEReference(maxExpressionEClass, MAX_EXPRESSION__LEFT);
    createEReference(maxExpressionEClass, MAX_EXPRESSION__RIGHT);

    sinExpressionEClass = createEClass(SIN_EXPRESSION);
    createEReference(sinExpressionEClass, SIN_EXPRESSION__ARG);

    cosExpressionEClass = createEClass(COS_EXPRESSION);
    createEReference(cosExpressionEClass, COS_EXPRESSION__ARG);

    tanExpressionEClass = createEClass(TAN_EXPRESSION);
    createEReference(tanExpressionEClass, TAN_EXPRESSION__ARG);

    aTanExpressionEClass = createEClass(ATAN_EXPRESSION);
    createEReference(aTanExpressionEClass, ATAN_EXPRESSION__ARG);

    aSinExpressionEClass = createEClass(ASIN_EXPRESSION);
    createEReference(aSinExpressionEClass, ASIN_EXPRESSION__ARG);

    aCosExpressionEClass = createEClass(ACOS_EXPRESSION);
    createEReference(aCosExpressionEClass, ACOS_EXPRESSION__ARG);

    populationExpressionEClass = createEClass(POPULATION_EXPRESSION);
    createEReference(populationExpressionEClass, POPULATION_EXPRESSION__STATE);

    numberExpressionEClass = createEClass(NUMBER_EXPRESSION);

    numberLiteralEClass = createEClass(NUMBER_LITERAL);
    createEAttribute(numberLiteralEClass, NUMBER_LITERAL__INT_PART);

    referenceableNameEClass = createEClass(REFERENCEABLE_NAME);

    probabilityFormulaEClass = createEClass(PROBABILITY_FORMULA);
    createEAttribute(probabilityFormulaEClass, PROBABILITY_FORMULA__REL);
    createEReference(probabilityFormulaEClass, PROBABILITY_FORMULA__PBOUND);
    createEReference(probabilityFormulaEClass, PROBABILITY_FORMULA__PATH);

    pctlPathFormulaEClass = createEClass(PCTL_PATH_FORMULA);

    namedPctlPathFormulaEClass = createEClass(NAMED_PCTL_PATH_FORMULA);
    createEReference(namedPctlPathFormulaEClass, NAMED_PCTL_PATH_FORMULA__NAME);

    nextFormulaEClass = createEClass(NEXT_FORMULA);
    createEReference(nextFormulaEClass, NEXT_FORMULA__ARG);

    untilFormulaEClass = createEClass(UNTIL_FORMULA);
    createEReference(untilFormulaEClass, UNTIL_FORMULA__LEFT);
    createEAttribute(untilFormulaEClass, UNTIL_FORMULA__IS_BOUND);
    createEAttribute(untilFormulaEClass, UNTIL_FORMULA__BOUND);
    createEReference(untilFormulaEClass, UNTIL_FORMULA__RIGHT);

    falseFormulaEClass = createEClass(FALSE_FORMULA);

    trueFormulaEClass = createEClass(TRUE_FORMULA);

    notFormulaEClass = createEClass(NOT_FORMULA);
    createEReference(notFormulaEClass, NOT_FORMULA__ARG);

    relationExpressionEClass = createEClass(RELATION_EXPRESSION);
    createEReference(relationExpressionEClass, RELATION_EXPRESSION__LEFT);
    createEAttribute(relationExpressionEClass, RELATION_EXPRESSION__OP);
    createEReference(relationExpressionEClass, RELATION_EXPRESSION__RIGHT);

    sumDiffExpressionEClass = createEClass(SUM_DIFF_EXPRESSION);
    createEReference(sumDiffExpressionEClass, SUM_DIFF_EXPRESSION__LEFT);
    createEAttribute(sumDiffExpressionEClass, SUM_DIFF_EXPRESSION__OP);
    createEReference(sumDiffExpressionEClass, SUM_DIFF_EXPRESSION__RIGHT);

    mulDivExpressionEClass = createEClass(MUL_DIV_EXPRESSION);
    createEReference(mulDivExpressionEClass, MUL_DIV_EXPRESSION__LEFT);
    createEAttribute(mulDivExpressionEClass, MUL_DIV_EXPRESSION__OP);
    createEReference(mulDivExpressionEClass, MUL_DIV_EXPRESSION__RIGHT);

    decimalLiteralEClass = createEClass(DECIMAL_LITERAL);
    createEReference(decimalLiteralEClass, DECIMAL_LITERAL__INTEGER_PART);
    createEAttribute(decimalLiteralEClass, DECIMAL_LITERAL__DECIMAL_PART);

    orPctlFormulaEClass = createEClass(OR_PCTL_FORMULA);
    createEReference(orPctlFormulaEClass, OR_PCTL_FORMULA__LEFT);
    createEReference(orPctlFormulaEClass, OR_PCTL_FORMULA__RIGHT);

    andPctlFormulaEClass = createEClass(AND_PCTL_FORMULA);
    createEReference(andPctlFormulaEClass, AND_PCTL_FORMULA__LEFT);
    createEReference(andPctlFormulaEClass, AND_PCTL_FORMULA__RIGHT);

    // Create enums
    relationSymbolEEnum = createEEnum(RELATION_SYMBOL);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    pathFormulaEClass.getESuperTypes().add(this.getElement());
    formulaEClass.getESuperTypes().add(this.getElement());
    formulaEClass.getESuperTypes().add(this.getReferenceableName());
    labelEClass.getESuperTypes().add(this.getElement());
    labelEClass.getESuperTypes().add(this.getReferenceableName());
    constantEClass.getESuperTypes().add(this.getElement());
    constantEClass.getESuperTypes().add(this.getReferenceableName());
    actionEClass.getESuperTypes().add(this.getElement());
    stateConstantEClass.getESuperTypes().add(this.getElement());
    stateConstantEClass.getESuperTypes().add(this.getReferenceableName());
    configurationEClass.getESuperTypes().add(this.getElement());
    literalExpressionEClass.getESuperTypes().add(this.getExpression());
    modExpressionEClass.getESuperTypes().add(this.getExpression());
    logExpressionEClass.getESuperTypes().add(this.getExpression());
    powExpressionEClass.getESuperTypes().add(this.getExpression());
    floorExpressionEClass.getESuperTypes().add(this.getExpression());
    ceilExpressionEClass.getESuperTypes().add(this.getExpression());
    minExpressionEClass.getESuperTypes().add(this.getExpression());
    maxExpressionEClass.getESuperTypes().add(this.getExpression());
    sinExpressionEClass.getESuperTypes().add(this.getExpression());
    cosExpressionEClass.getESuperTypes().add(this.getExpression());
    tanExpressionEClass.getESuperTypes().add(this.getExpression());
    aTanExpressionEClass.getESuperTypes().add(this.getExpression());
    aSinExpressionEClass.getESuperTypes().add(this.getExpression());
    aCosExpressionEClass.getESuperTypes().add(this.getExpression());
    populationExpressionEClass.getESuperTypes().add(this.getExpression());
    numberExpressionEClass.getESuperTypes().add(this.getExpression());
    numberLiteralEClass.getESuperTypes().add(this.getNumberExpression());
    probabilityFormulaEClass.getESuperTypes().add(this.getExpression());
    namedPctlPathFormulaEClass.getESuperTypes().add(this.getPctlPathFormula());
    nextFormulaEClass.getESuperTypes().add(this.getPctlPathFormula());
    untilFormulaEClass.getESuperTypes().add(this.getPctlPathFormula());
    falseFormulaEClass.getESuperTypes().add(this.getExpression());
    trueFormulaEClass.getESuperTypes().add(this.getExpression());
    notFormulaEClass.getESuperTypes().add(this.getExpression());
    relationExpressionEClass.getESuperTypes().add(this.getExpression());
    sumDiffExpressionEClass.getESuperTypes().add(this.getExpression());
    mulDivExpressionEClass.getESuperTypes().add(this.getExpression());
    decimalLiteralEClass.getESuperTypes().add(this.getNumberExpression());
    orPctlFormulaEClass.getESuperTypes().add(this.getExpression());
    andPctlFormulaEClass.getESuperTypes().add(this.getExpression());

    // Initialize classes and features; add operations and parameters
    initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getModel_Elements(), this.getElement(), null, "elements", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(elementEClass, Element.class, "Element", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, Element.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(pathFormulaEClass, PathFormula.class, "PathFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPathFormula_Formula(), this.getPctlPathFormula(), null, "formula", null, 0, 1, PathFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(formulaEClass, Formula.class, "Formula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getFormula_Formula(), this.getExpression(), null, "formula", null, 0, 1, Formula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(labelEClass, Label.class, "Label", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLabel_States(), this.getStateConstant(), null, "states", null, 0, -1, Label.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(constantEClass, Constant.class, "Constant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getConstant_Exp(), this.getExpression(), null, "exp", null, 0, 1, Constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(actionEClass, Action.class, "Action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAction_Probability(), this.getExpression(), null, "probability", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(stateConstantEClass, StateConstant.class, "StateConstant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStateConstant_Transitions(), this.getTransition(), null, "transitions", null, 0, -1, StateConstant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(transitionEClass, Transition.class, "Transition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getTransition_Action(), this.getAction(), null, "action", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTransition_NextState(), this.getStateConstant(), null, "nextState", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(configurationEClass, Configuration.class, "Configuration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getConfiguration_Elements(), this.getPopulationElement(), null, "elements", null, 0, -1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(populationElementEClass, PopulationElement.class, "PopulationElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPopulationElement_State(), this.getStateConstant(), null, "state", null, 0, 1, PopulationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPopulationElement_HasSize(), ecorePackage.getEBoolean(), "hasSize", null, 0, 1, PopulationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getPopulationElement_Size(), this.getExpression(), null, "size", null, 0, 1, PopulationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionEClass, Expression.class, "Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(literalExpressionEClass, LiteralExpression.class, "LiteralExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLiteralExpression_Ref(), this.getReferenceableName(), null, "ref", null, 0, 1, LiteralExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(modExpressionEClass, ModExpression.class, "ModExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getModExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, ModExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModExpression_Mod(), this.getExpression(), null, "mod", null, 0, 1, ModExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(logExpressionEClass, LogExpression.class, "LogExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLogExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, LogExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(powExpressionEClass, PowExpression.class, "PowExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPowExpression_Base(), this.getExpression(), null, "base", null, 0, 1, PowExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getPowExpression_Exp(), this.getExpression(), null, "exp", null, 0, 1, PowExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(floorExpressionEClass, FloorExpression.class, "FloorExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getFloorExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, FloorExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ceilExpressionEClass, CeilExpression.class, "CeilExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCeilExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, CeilExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(minExpressionEClass, MinExpression.class, "MinExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMinExpression_Left(), this.getExpression(), null, "left", null, 0, 1, MinExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMinExpression_Right(), this.getExpression(), null, "right", null, 0, 1, MinExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(maxExpressionEClass, MaxExpression.class, "MaxExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMaxExpression_Left(), this.getExpression(), null, "left", null, 0, 1, MaxExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMaxExpression_Right(), this.getExpression(), null, "right", null, 0, 1, MaxExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(sinExpressionEClass, SinExpression.class, "SinExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSinExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, SinExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(cosExpressionEClass, CosExpression.class, "CosExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCosExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, CosExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(tanExpressionEClass, TanExpression.class, "TanExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getTanExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, TanExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(aTanExpressionEClass, ATanExpression.class, "ATanExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getATanExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, ATanExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(aSinExpressionEClass, ASinExpression.class, "ASinExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getASinExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, ASinExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(aCosExpressionEClass, ACosExpression.class, "ACosExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getACosExpression_Arg(), this.getExpression(), null, "arg", null, 0, 1, ACosExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(populationExpressionEClass, PopulationExpression.class, "PopulationExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPopulationExpression_State(), this.getStateConstant(), null, "state", null, 0, 1, PopulationExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(numberExpressionEClass, NumberExpression.class, "NumberExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(numberLiteralEClass, NumberLiteral.class, "NumberLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getNumberLiteral_IntPart(), ecorePackage.getEInt(), "intPart", null, 0, 1, NumberLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(referenceableNameEClass, ReferenceableName.class, "ReferenceableName", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(probabilityFormulaEClass, ProbabilityFormula.class, "ProbabilityFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getProbabilityFormula_Rel(), this.getRelationSymbol(), "rel", null, 0, 1, ProbabilityFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProbabilityFormula_PBound(), this.getNumberExpression(), null, "pBound", null, 0, 1, ProbabilityFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProbabilityFormula_Path(), this.getPctlPathFormula(), null, "path", null, 0, 1, ProbabilityFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(pctlPathFormulaEClass, PctlPathFormula.class, "PctlPathFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(namedPctlPathFormulaEClass, NamedPctlPathFormula.class, "NamedPctlPathFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNamedPctlPathFormula_Name(), this.getPathFormula(), null, "name", null, 0, 1, NamedPctlPathFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(nextFormulaEClass, NextFormula.class, "NextFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNextFormula_Arg(), this.getExpression(), null, "arg", null, 0, 1, NextFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(untilFormulaEClass, UntilFormula.class, "UntilFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getUntilFormula_Left(), this.getExpression(), null, "left", null, 0, 1, UntilFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getUntilFormula_IsBound(), ecorePackage.getEBoolean(), "isBound", null, 0, 1, UntilFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getUntilFormula_Bound(), ecorePackage.getEInt(), "bound", null, 0, 1, UntilFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getUntilFormula_Right(), this.getExpression(), null, "right", null, 0, 1, UntilFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(falseFormulaEClass, FalseFormula.class, "FalseFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(trueFormulaEClass, TrueFormula.class, "TrueFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(notFormulaEClass, NotFormula.class, "NotFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNotFormula_Arg(), this.getExpression(), null, "arg", null, 0, 1, NotFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(relationExpressionEClass, RelationExpression.class, "RelationExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRelationExpression_Left(), this.getExpression(), null, "left", null, 0, 1, RelationExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRelationExpression_Op(), this.getRelationSymbol(), "op", null, 0, 1, RelationExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRelationExpression_Right(), this.getExpression(), null, "right", null, 0, 1, RelationExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(sumDiffExpressionEClass, SumDiffExpression.class, "SumDiffExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSumDiffExpression_Left(), this.getExpression(), null, "left", null, 0, 1, SumDiffExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSumDiffExpression_Op(), ecorePackage.getEString(), "op", null, 0, 1, SumDiffExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSumDiffExpression_Right(), this.getExpression(), null, "right", null, 0, 1, SumDiffExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(mulDivExpressionEClass, MulDivExpression.class, "MulDivExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMulDivExpression_Left(), this.getExpression(), null, "left", null, 0, 1, MulDivExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMulDivExpression_Op(), ecorePackage.getEString(), "op", null, 0, 1, MulDivExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMulDivExpression_Right(), this.getExpression(), null, "right", null, 0, 1, MulDivExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(decimalLiteralEClass, DecimalLiteral.class, "DecimalLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDecimalLiteral_IntegerPart(), this.getNumberLiteral(), null, "integerPart", null, 0, 1, DecimalLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDecimalLiteral_DecimalPart(), ecorePackage.getEString(), "decimalPart", null, 0, 1, DecimalLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(orPctlFormulaEClass, OrPctlFormula.class, "OrPctlFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOrPctlFormula_Left(), this.getExpression(), null, "left", null, 0, 1, OrPctlFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOrPctlFormula_Right(), this.getExpression(), null, "right", null, 0, 1, OrPctlFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(andPctlFormulaEClass, AndPctlFormula.class, "AndPctlFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAndPctlFormula_Left(), this.getExpression(), null, "left", null, 0, 1, AndPctlFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAndPctlFormula_Right(), this.getExpression(), null, "right", null, 0, 1, AndPctlFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Initialize enums and add enum literals
    initEEnum(relationSymbolEEnum, RelationSymbol.class, "RelationSymbol");
    addEEnumLiteral(relationSymbolEEnum, RelationSymbol.LES);
    addEEnumLiteral(relationSymbolEEnum, RelationSymbol.LEQ);
    addEEnumLiteral(relationSymbolEEnum, RelationSymbol.GEQ);
    addEEnumLiteral(relationSymbolEEnum, RelationSymbol.GTR);

    // Create resource
    createResource(eNS_URI);
  }

} //PopulationPackageImpl
