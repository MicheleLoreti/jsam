/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.NumberExpression;
import org.cmg.ml.sam.xtext.population.population.PctlPathFormula;
import org.cmg.ml.sam.xtext.population.population.PopulationPackage;
import org.cmg.ml.sam.xtext.population.population.ProbabilityFormula;
import org.cmg.ml.sam.xtext.population.population.RelationSymbol;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Probability Formula</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.impl.ProbabilityFormulaImpl#getRel <em>Rel</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.impl.ProbabilityFormulaImpl#getPBound <em>PBound</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.impl.ProbabilityFormulaImpl#getPath <em>Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProbabilityFormulaImpl extends ExpressionImpl implements ProbabilityFormula
{
  /**
   * The default value of the '{@link #getRel() <em>Rel</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRel()
   * @generated
   * @ordered
   */
  protected static final RelationSymbol REL_EDEFAULT = RelationSymbol.LES;

  /**
   * The cached value of the '{@link #getRel() <em>Rel</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRel()
   * @generated
   * @ordered
   */
  protected RelationSymbol rel = REL_EDEFAULT;

  /**
   * The cached value of the '{@link #getPBound() <em>PBound</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPBound()
   * @generated
   * @ordered
   */
  protected NumberExpression pBound;

  /**
   * The cached value of the '{@link #getPath() <em>Path</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPath()
   * @generated
   * @ordered
   */
  protected PctlPathFormula path;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProbabilityFormulaImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PopulationPackage.Literals.PROBABILITY_FORMULA;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RelationSymbol getRel()
  {
    return rel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRel(RelationSymbol newRel)
  {
    RelationSymbol oldRel = rel;
    rel = newRel == null ? REL_EDEFAULT : newRel;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PopulationPackage.PROBABILITY_FORMULA__REL, oldRel, rel));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NumberExpression getPBound()
  {
    return pBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPBound(NumberExpression newPBound, NotificationChain msgs)
  {
    NumberExpression oldPBound = pBound;
    pBound = newPBound;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PopulationPackage.PROBABILITY_FORMULA__PBOUND, oldPBound, newPBound);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPBound(NumberExpression newPBound)
  {
    if (newPBound != pBound)
    {
      NotificationChain msgs = null;
      if (pBound != null)
        msgs = ((InternalEObject)pBound).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PopulationPackage.PROBABILITY_FORMULA__PBOUND, null, msgs);
      if (newPBound != null)
        msgs = ((InternalEObject)newPBound).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PopulationPackage.PROBABILITY_FORMULA__PBOUND, null, msgs);
      msgs = basicSetPBound(newPBound, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PopulationPackage.PROBABILITY_FORMULA__PBOUND, newPBound, newPBound));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PctlPathFormula getPath()
  {
    return path;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPath(PctlPathFormula newPath, NotificationChain msgs)
  {
    PctlPathFormula oldPath = path;
    path = newPath;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PopulationPackage.PROBABILITY_FORMULA__PATH, oldPath, newPath);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPath(PctlPathFormula newPath)
  {
    if (newPath != path)
    {
      NotificationChain msgs = null;
      if (path != null)
        msgs = ((InternalEObject)path).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PopulationPackage.PROBABILITY_FORMULA__PATH, null, msgs);
      if (newPath != null)
        msgs = ((InternalEObject)newPath).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PopulationPackage.PROBABILITY_FORMULA__PATH, null, msgs);
      msgs = basicSetPath(newPath, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PopulationPackage.PROBABILITY_FORMULA__PATH, newPath, newPath));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PopulationPackage.PROBABILITY_FORMULA__PBOUND:
        return basicSetPBound(null, msgs);
      case PopulationPackage.PROBABILITY_FORMULA__PATH:
        return basicSetPath(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PopulationPackage.PROBABILITY_FORMULA__REL:
        return getRel();
      case PopulationPackage.PROBABILITY_FORMULA__PBOUND:
        return getPBound();
      case PopulationPackage.PROBABILITY_FORMULA__PATH:
        return getPath();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PopulationPackage.PROBABILITY_FORMULA__REL:
        setRel((RelationSymbol)newValue);
        return;
      case PopulationPackage.PROBABILITY_FORMULA__PBOUND:
        setPBound((NumberExpression)newValue);
        return;
      case PopulationPackage.PROBABILITY_FORMULA__PATH:
        setPath((PctlPathFormula)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PopulationPackage.PROBABILITY_FORMULA__REL:
        setRel(REL_EDEFAULT);
        return;
      case PopulationPackage.PROBABILITY_FORMULA__PBOUND:
        setPBound((NumberExpression)null);
        return;
      case PopulationPackage.PROBABILITY_FORMULA__PATH:
        setPath((PctlPathFormula)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PopulationPackage.PROBABILITY_FORMULA__REL:
        return rel != REL_EDEFAULT;
      case PopulationPackage.PROBABILITY_FORMULA__PBOUND:
        return pBound != null;
      case PopulationPackage.PROBABILITY_FORMULA__PATH:
        return path != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (rel: ");
    result.append(rel);
    result.append(')');
    return result.toString();
  }

} //ProbabilityFormulaImpl
