/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.Action;
import org.cmg.ml.sam.xtext.population.population.PopulationPackage;
import org.cmg.ml.sam.xtext.population.population.StateConstant;
import org.cmg.ml.sam.xtext.population.population.Transition;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.impl.TransitionImpl#getAction <em>Action</em>}</li>
 *   <li>{@link org.cmg.ml.sam.xtext.population.population.impl.TransitionImpl#getNextState <em>Next State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionImpl extends MinimalEObjectImpl.Container implements Transition
{
  /**
   * The cached value of the '{@link #getAction() <em>Action</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAction()
   * @generated
   * @ordered
   */
  protected Action action;

  /**
   * The cached value of the '{@link #getNextState() <em>Next State</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNextState()
   * @generated
   * @ordered
   */
  protected StateConstant nextState;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TransitionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PopulationPackage.Literals.TRANSITION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action getAction()
  {
    if (action != null && action.eIsProxy())
    {
      InternalEObject oldAction = (InternalEObject)action;
      action = (Action)eResolveProxy(oldAction);
      if (action != oldAction)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, PopulationPackage.TRANSITION__ACTION, oldAction, action));
      }
    }
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action basicGetAction()
  {
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAction(Action newAction)
  {
    Action oldAction = action;
    action = newAction;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PopulationPackage.TRANSITION__ACTION, oldAction, action));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StateConstant getNextState()
  {
    if (nextState != null && nextState.eIsProxy())
    {
      InternalEObject oldNextState = (InternalEObject)nextState;
      nextState = (StateConstant)eResolveProxy(oldNextState);
      if (nextState != oldNextState)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, PopulationPackage.TRANSITION__NEXT_STATE, oldNextState, nextState));
      }
    }
    return nextState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StateConstant basicGetNextState()
  {
    return nextState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNextState(StateConstant newNextState)
  {
    StateConstant oldNextState = nextState;
    nextState = newNextState;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PopulationPackage.TRANSITION__NEXT_STATE, oldNextState, nextState));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PopulationPackage.TRANSITION__ACTION:
        if (resolve) return getAction();
        return basicGetAction();
      case PopulationPackage.TRANSITION__NEXT_STATE:
        if (resolve) return getNextState();
        return basicGetNextState();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PopulationPackage.TRANSITION__ACTION:
        setAction((Action)newValue);
        return;
      case PopulationPackage.TRANSITION__NEXT_STATE:
        setNextState((StateConstant)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PopulationPackage.TRANSITION__ACTION:
        setAction((Action)null);
        return;
      case PopulationPackage.TRANSITION__NEXT_STATE:
        setNextState((StateConstant)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PopulationPackage.TRANSITION__ACTION:
        return action != null;
      case PopulationPackage.TRANSITION__NEXT_STATE:
        return nextState != null;
    }
    return super.eIsSet(featureID);
  }

} //TransitionImpl
