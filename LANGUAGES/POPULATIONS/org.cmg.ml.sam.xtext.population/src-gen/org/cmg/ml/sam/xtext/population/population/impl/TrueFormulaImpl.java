/**
 */
package org.cmg.ml.sam.xtext.population.population.impl;

import org.cmg.ml.sam.xtext.population.population.PopulationPackage;
import org.cmg.ml.sam.xtext.population.population.TrueFormula;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>True Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TrueFormulaImpl extends ExpressionImpl implements TrueFormula
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TrueFormulaImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PopulationPackage.Literals.TRUE_FORMULA;
  }

} //TrueFormulaImpl
