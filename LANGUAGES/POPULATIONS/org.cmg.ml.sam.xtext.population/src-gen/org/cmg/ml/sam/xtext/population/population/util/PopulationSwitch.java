/**
 */
package org.cmg.ml.sam.xtext.population.population.util;

import org.cmg.ml.sam.xtext.population.population.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.cmg.ml.sam.xtext.population.population.PopulationPackage
 * @generated
 */
public class PopulationSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static PopulationPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PopulationSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = PopulationPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case PopulationPackage.MODEL:
      {
        Model model = (Model)theEObject;
        T result = caseModel(model);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.ELEMENT:
      {
        Element element = (Element)theEObject;
        T result = caseElement(element);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.PATH_FORMULA:
      {
        PathFormula pathFormula = (PathFormula)theEObject;
        T result = casePathFormula(pathFormula);
        if (result == null) result = caseElement(pathFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.FORMULA:
      {
        Formula formula = (Formula)theEObject;
        T result = caseFormula(formula);
        if (result == null) result = caseElement(formula);
        if (result == null) result = caseReferenceableName(formula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.LABEL:
      {
        Label label = (Label)theEObject;
        T result = caseLabel(label);
        if (result == null) result = caseElement(label);
        if (result == null) result = caseReferenceableName(label);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.CONSTANT:
      {
        Constant constant = (Constant)theEObject;
        T result = caseConstant(constant);
        if (result == null) result = caseElement(constant);
        if (result == null) result = caseReferenceableName(constant);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.ACTION:
      {
        Action action = (Action)theEObject;
        T result = caseAction(action);
        if (result == null) result = caseElement(action);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.STATE_CONSTANT:
      {
        StateConstant stateConstant = (StateConstant)theEObject;
        T result = caseStateConstant(stateConstant);
        if (result == null) result = caseElement(stateConstant);
        if (result == null) result = caseReferenceableName(stateConstant);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.TRANSITION:
      {
        Transition transition = (Transition)theEObject;
        T result = caseTransition(transition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.CONFIGURATION:
      {
        Configuration configuration = (Configuration)theEObject;
        T result = caseConfiguration(configuration);
        if (result == null) result = caseElement(configuration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.POPULATION_ELEMENT:
      {
        PopulationElement populationElement = (PopulationElement)theEObject;
        T result = casePopulationElement(populationElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.EXPRESSION:
      {
        Expression expression = (Expression)theEObject;
        T result = caseExpression(expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.LITERAL_EXPRESSION:
      {
        LiteralExpression literalExpression = (LiteralExpression)theEObject;
        T result = caseLiteralExpression(literalExpression);
        if (result == null) result = caseExpression(literalExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.MOD_EXPRESSION:
      {
        ModExpression modExpression = (ModExpression)theEObject;
        T result = caseModExpression(modExpression);
        if (result == null) result = caseExpression(modExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.LOG_EXPRESSION:
      {
        LogExpression logExpression = (LogExpression)theEObject;
        T result = caseLogExpression(logExpression);
        if (result == null) result = caseExpression(logExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.POW_EXPRESSION:
      {
        PowExpression powExpression = (PowExpression)theEObject;
        T result = casePowExpression(powExpression);
        if (result == null) result = caseExpression(powExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.FLOOR_EXPRESSION:
      {
        FloorExpression floorExpression = (FloorExpression)theEObject;
        T result = caseFloorExpression(floorExpression);
        if (result == null) result = caseExpression(floorExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.CEIL_EXPRESSION:
      {
        CeilExpression ceilExpression = (CeilExpression)theEObject;
        T result = caseCeilExpression(ceilExpression);
        if (result == null) result = caseExpression(ceilExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.MIN_EXPRESSION:
      {
        MinExpression minExpression = (MinExpression)theEObject;
        T result = caseMinExpression(minExpression);
        if (result == null) result = caseExpression(minExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.MAX_EXPRESSION:
      {
        MaxExpression maxExpression = (MaxExpression)theEObject;
        T result = caseMaxExpression(maxExpression);
        if (result == null) result = caseExpression(maxExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.SIN_EXPRESSION:
      {
        SinExpression sinExpression = (SinExpression)theEObject;
        T result = caseSinExpression(sinExpression);
        if (result == null) result = caseExpression(sinExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.COS_EXPRESSION:
      {
        CosExpression cosExpression = (CosExpression)theEObject;
        T result = caseCosExpression(cosExpression);
        if (result == null) result = caseExpression(cosExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.TAN_EXPRESSION:
      {
        TanExpression tanExpression = (TanExpression)theEObject;
        T result = caseTanExpression(tanExpression);
        if (result == null) result = caseExpression(tanExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.ATAN_EXPRESSION:
      {
        ATanExpression aTanExpression = (ATanExpression)theEObject;
        T result = caseATanExpression(aTanExpression);
        if (result == null) result = caseExpression(aTanExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.ASIN_EXPRESSION:
      {
        ASinExpression aSinExpression = (ASinExpression)theEObject;
        T result = caseASinExpression(aSinExpression);
        if (result == null) result = caseExpression(aSinExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.ACOS_EXPRESSION:
      {
        ACosExpression aCosExpression = (ACosExpression)theEObject;
        T result = caseACosExpression(aCosExpression);
        if (result == null) result = caseExpression(aCosExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.POPULATION_EXPRESSION:
      {
        PopulationExpression populationExpression = (PopulationExpression)theEObject;
        T result = casePopulationExpression(populationExpression);
        if (result == null) result = caseExpression(populationExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.NUMBER_EXPRESSION:
      {
        NumberExpression numberExpression = (NumberExpression)theEObject;
        T result = caseNumberExpression(numberExpression);
        if (result == null) result = caseExpression(numberExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.NUMBER_LITERAL:
      {
        NumberLiteral numberLiteral = (NumberLiteral)theEObject;
        T result = caseNumberLiteral(numberLiteral);
        if (result == null) result = caseNumberExpression(numberLiteral);
        if (result == null) result = caseExpression(numberLiteral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.REFERENCEABLE_NAME:
      {
        ReferenceableName referenceableName = (ReferenceableName)theEObject;
        T result = caseReferenceableName(referenceableName);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.PROBABILITY_FORMULA:
      {
        ProbabilityFormula probabilityFormula = (ProbabilityFormula)theEObject;
        T result = caseProbabilityFormula(probabilityFormula);
        if (result == null) result = caseExpression(probabilityFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.PCTL_PATH_FORMULA:
      {
        PctlPathFormula pctlPathFormula = (PctlPathFormula)theEObject;
        T result = casePctlPathFormula(pctlPathFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.NAMED_PCTL_PATH_FORMULA:
      {
        NamedPctlPathFormula namedPctlPathFormula = (NamedPctlPathFormula)theEObject;
        T result = caseNamedPctlPathFormula(namedPctlPathFormula);
        if (result == null) result = casePctlPathFormula(namedPctlPathFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.NEXT_FORMULA:
      {
        NextFormula nextFormula = (NextFormula)theEObject;
        T result = caseNextFormula(nextFormula);
        if (result == null) result = casePctlPathFormula(nextFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.UNTIL_FORMULA:
      {
        UntilFormula untilFormula = (UntilFormula)theEObject;
        T result = caseUntilFormula(untilFormula);
        if (result == null) result = casePctlPathFormula(untilFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.FALSE_FORMULA:
      {
        FalseFormula falseFormula = (FalseFormula)theEObject;
        T result = caseFalseFormula(falseFormula);
        if (result == null) result = caseExpression(falseFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.TRUE_FORMULA:
      {
        TrueFormula trueFormula = (TrueFormula)theEObject;
        T result = caseTrueFormula(trueFormula);
        if (result == null) result = caseExpression(trueFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.NOT_FORMULA:
      {
        NotFormula notFormula = (NotFormula)theEObject;
        T result = caseNotFormula(notFormula);
        if (result == null) result = caseExpression(notFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.RELATION_EXPRESSION:
      {
        RelationExpression relationExpression = (RelationExpression)theEObject;
        T result = caseRelationExpression(relationExpression);
        if (result == null) result = caseExpression(relationExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.SUM_DIFF_EXPRESSION:
      {
        SumDiffExpression sumDiffExpression = (SumDiffExpression)theEObject;
        T result = caseSumDiffExpression(sumDiffExpression);
        if (result == null) result = caseExpression(sumDiffExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.MUL_DIV_EXPRESSION:
      {
        MulDivExpression mulDivExpression = (MulDivExpression)theEObject;
        T result = caseMulDivExpression(mulDivExpression);
        if (result == null) result = caseExpression(mulDivExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.DECIMAL_LITERAL:
      {
        DecimalLiteral decimalLiteral = (DecimalLiteral)theEObject;
        T result = caseDecimalLiteral(decimalLiteral);
        if (result == null) result = caseNumberExpression(decimalLiteral);
        if (result == null) result = caseExpression(decimalLiteral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.OR_PCTL_FORMULA:
      {
        OrPctlFormula orPctlFormula = (OrPctlFormula)theEObject;
        T result = caseOrPctlFormula(orPctlFormula);
        if (result == null) result = caseExpression(orPctlFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case PopulationPackage.AND_PCTL_FORMULA:
      {
        AndPctlFormula andPctlFormula = (AndPctlFormula)theEObject;
        T result = caseAndPctlFormula(andPctlFormula);
        if (result == null) result = caseExpression(andPctlFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Model</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModel(Model object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElement(Element object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Path Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Path Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePathFormula(PathFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFormula(Formula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Label</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Label</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLabel(Label object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constant</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constant</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstant(Constant object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Action</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAction(Action object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>State Constant</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>State Constant</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStateConstant(StateConstant object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Transition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Transition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTransition(Transition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Configuration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Configuration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConfiguration(Configuration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePopulationElement(PopulationElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpression(Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Literal Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Literal Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLiteralExpression(LiteralExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mod Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mod Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModExpression(ModExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Log Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Log Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLogExpression(LogExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Pow Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Pow Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePowExpression(PowExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Floor Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Floor Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFloorExpression(FloorExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ceil Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ceil Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCeilExpression(CeilExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Min Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Min Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMinExpression(MinExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Max Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Max Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMaxExpression(MaxExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sin Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sin Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSinExpression(SinExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cos Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cos Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCosExpression(CosExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Tan Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Tan Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTanExpression(TanExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ATan Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ATan Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseATanExpression(ATanExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ASin Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ASin Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseASinExpression(ASinExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ACos Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ACos Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseACosExpression(ACosExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePopulationExpression(PopulationExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Number Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Number Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNumberExpression(NumberExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Number Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Number Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNumberLiteral(NumberLiteral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Referenceable Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Referenceable Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReferenceableName(ReferenceableName object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Probability Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Probability Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProbabilityFormula(ProbabilityFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Pctl Path Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Pctl Path Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePctlPathFormula(PctlPathFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Named Pctl Path Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Named Pctl Path Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNamedPctlPathFormula(NamedPctlPathFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Next Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Next Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNextFormula(NextFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Until Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Until Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUntilFormula(UntilFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>False Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>False Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFalseFormula(FalseFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>True Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>True Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTrueFormula(TrueFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Not Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Not Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNotFormula(NotFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Relation Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Relation Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRelationExpression(RelationExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sum Diff Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sum Diff Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSumDiffExpression(SumDiffExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mul Div Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mul Div Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMulDivExpression(MulDivExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Decimal Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Decimal Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDecimalLiteral(DecimalLiteral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Or Pctl Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Or Pctl Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOrPctlFormula(OrPctlFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>And Pctl Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>And Pctl Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAndPctlFormula(AndPctlFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //PopulationSwitch
