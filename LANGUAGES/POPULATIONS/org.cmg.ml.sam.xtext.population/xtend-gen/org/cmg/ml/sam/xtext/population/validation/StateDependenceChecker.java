package org.cmg.ml.sam.xtext.population.validation;

import java.util.Arrays;
import org.cmg.ml.sam.xtext.population.population.ACosExpression;
import org.cmg.ml.sam.xtext.population.population.ASinExpression;
import org.cmg.ml.sam.xtext.population.population.ATanExpression;
import org.cmg.ml.sam.xtext.population.population.AndPctlFormula;
import org.cmg.ml.sam.xtext.population.population.CeilExpression;
import org.cmg.ml.sam.xtext.population.population.CosExpression;
import org.cmg.ml.sam.xtext.population.population.Expression;
import org.cmg.ml.sam.xtext.population.population.FalseFormula;
import org.cmg.ml.sam.xtext.population.population.FloorExpression;
import org.cmg.ml.sam.xtext.population.population.LiteralExpression;
import org.cmg.ml.sam.xtext.population.population.LogExpression;
import org.cmg.ml.sam.xtext.population.population.MaxExpression;
import org.cmg.ml.sam.xtext.population.population.MinExpression;
import org.cmg.ml.sam.xtext.population.population.ModExpression;
import org.cmg.ml.sam.xtext.population.population.MulDivExpression;
import org.cmg.ml.sam.xtext.population.population.NotFormula;
import org.cmg.ml.sam.xtext.population.population.OrPctlFormula;
import org.cmg.ml.sam.xtext.population.population.PopulationExpression;
import org.cmg.ml.sam.xtext.population.population.PowExpression;
import org.cmg.ml.sam.xtext.population.population.ProbabilityFormula;
import org.cmg.ml.sam.xtext.population.population.RelationExpression;
import org.cmg.ml.sam.xtext.population.population.SinExpression;
import org.cmg.ml.sam.xtext.population.population.SumDiffExpression;
import org.cmg.ml.sam.xtext.population.population.TanExpression;
import org.cmg.ml.sam.xtext.population.population.TrueFormula;

@SuppressWarnings("all")
public class StateDependenceChecker {
  protected boolean _isStateDependent(final OrPctlFormula e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _left = e.getLeft();
    boolean _isStateDependent = this.isStateDependent(_left);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _right = e.getRight();
      boolean _isStateDependent_1 = this.isStateDependent(_right);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final AndPctlFormula e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _left = e.getLeft();
    boolean _isStateDependent = this.isStateDependent(_left);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _right = e.getRight();
      boolean _isStateDependent_1 = this.isStateDependent(_right);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final RelationExpression e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _left = e.getLeft();
    boolean _isStateDependent = this.isStateDependent(_left);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _right = e.getRight();
      boolean _isStateDependent_1 = this.isStateDependent(_right);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final SumDiffExpression e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _left = e.getLeft();
    boolean _isStateDependent = this.isStateDependent(_left);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _right = e.getRight();
      boolean _isStateDependent_1 = this.isStateDependent(_right);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final MulDivExpression e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _left = e.getLeft();
    boolean _isStateDependent = this.isStateDependent(_left);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _right = e.getRight();
      boolean _isStateDependent_1 = this.isStateDependent(_right);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final Number e) {
    return false;
  }
  
  protected boolean _isStateDependent(final NotFormula e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final TrueFormula e) {
    return false;
  }
  
  protected boolean _isStateDependent(final FalseFormula e) {
    return false;
  }
  
  protected boolean _isStateDependent(final ProbabilityFormula e) {
    return false;
  }
  
  protected boolean _isStateDependent(final LiteralExpression e) {
    return false;
  }
  
  protected boolean _isStateDependent(final PopulationExpression e) {
    return true;
  }
  
  protected boolean _isStateDependent(final Expression e) {
    return false;
  }
  
  protected boolean _isStateDependent(final LogExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final ModExpression e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _arg = e.getArg();
    boolean _isStateDependent = this.isStateDependent(_arg);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _mod = e.getMod();
      boolean _isStateDependent_1 = this.isStateDependent(_mod);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final PowExpression e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _base = e.getBase();
    boolean _isStateDependent = this.isStateDependent(_base);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _exp = e.getExp();
      boolean _isStateDependent_1 = this.isStateDependent(_exp);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final FloorExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final CeilExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final MinExpression e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _left = e.getLeft();
    boolean _isStateDependent = this.isStateDependent(_left);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _right = e.getRight();
      boolean _isStateDependent_1 = this.isStateDependent(_right);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final MaxExpression e) {
    boolean _xifexpression = false;
    boolean _and = false;
    Expression _left = e.getLeft();
    boolean _isStateDependent = this.isStateDependent(_left);
    if (!_isStateDependent) {
      _and = false;
    } else {
      Expression _right = e.getRight();
      boolean _isStateDependent_1 = this.isStateDependent(_right);
      _and = _isStateDependent_1;
    }
    if (_and) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  protected boolean _isStateDependent(final SinExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final CosExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final TanExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final ATanExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final ASinExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  protected boolean _isStateDependent(final ACosExpression e) {
    Expression _arg = e.getArg();
    return this.isStateDependent(_arg);
  }
  
  public boolean isStateDependent(final Object e) {
    if (e instanceof ACosExpression) {
      return _isStateDependent((ACosExpression)e);
    } else if (e instanceof ASinExpression) {
      return _isStateDependent((ASinExpression)e);
    } else if (e instanceof ATanExpression) {
      return _isStateDependent((ATanExpression)e);
    } else if (e instanceof AndPctlFormula) {
      return _isStateDependent((AndPctlFormula)e);
    } else if (e instanceof CeilExpression) {
      return _isStateDependent((CeilExpression)e);
    } else if (e instanceof CosExpression) {
      return _isStateDependent((CosExpression)e);
    } else if (e instanceof FalseFormula) {
      return _isStateDependent((FalseFormula)e);
    } else if (e instanceof FloorExpression) {
      return _isStateDependent((FloorExpression)e);
    } else if (e instanceof LiteralExpression) {
      return _isStateDependent((LiteralExpression)e);
    } else if (e instanceof LogExpression) {
      return _isStateDependent((LogExpression)e);
    } else if (e instanceof MaxExpression) {
      return _isStateDependent((MaxExpression)e);
    } else if (e instanceof MinExpression) {
      return _isStateDependent((MinExpression)e);
    } else if (e instanceof ModExpression) {
      return _isStateDependent((ModExpression)e);
    } else if (e instanceof MulDivExpression) {
      return _isStateDependent((MulDivExpression)e);
    } else if (e instanceof NotFormula) {
      return _isStateDependent((NotFormula)e);
    } else if (e instanceof OrPctlFormula) {
      return _isStateDependent((OrPctlFormula)e);
    } else if (e instanceof PopulationExpression) {
      return _isStateDependent((PopulationExpression)e);
    } else if (e instanceof PowExpression) {
      return _isStateDependent((PowExpression)e);
    } else if (e instanceof ProbabilityFormula) {
      return _isStateDependent((ProbabilityFormula)e);
    } else if (e instanceof RelationExpression) {
      return _isStateDependent((RelationExpression)e);
    } else if (e instanceof SinExpression) {
      return _isStateDependent((SinExpression)e);
    } else if (e instanceof SumDiffExpression) {
      return _isStateDependent((SumDiffExpression)e);
    } else if (e instanceof TanExpression) {
      return _isStateDependent((TanExpression)e);
    } else if (e instanceof TrueFormula) {
      return _isStateDependent((TrueFormula)e);
    } else if (e instanceof Expression) {
      return _isStateDependent((Expression)e);
    } else if (e instanceof Number) {
      return _isStateDependent((Number)e);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(e).toString());
    }
  }
}
