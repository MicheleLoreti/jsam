package org.cmg.ml.sam.xtext.population.validation;

import java.util.Arrays;
import org.cmg.ml.sam.xtext.population.population.ACosExpression;
import org.cmg.ml.sam.xtext.population.population.ASinExpression;
import org.cmg.ml.sam.xtext.population.population.ATanExpression;
import org.cmg.ml.sam.xtext.population.population.AndPctlFormula;
import org.cmg.ml.sam.xtext.population.population.CeilExpression;
import org.cmg.ml.sam.xtext.population.population.Constant;
import org.cmg.ml.sam.xtext.population.population.CosExpression;
import org.cmg.ml.sam.xtext.population.population.DecimalLiteral;
import org.cmg.ml.sam.xtext.population.population.Expression;
import org.cmg.ml.sam.xtext.population.population.FalseFormula;
import org.cmg.ml.sam.xtext.population.population.FloorExpression;
import org.cmg.ml.sam.xtext.population.population.Formula;
import org.cmg.ml.sam.xtext.population.population.LiteralExpression;
import org.cmg.ml.sam.xtext.population.population.LogExpression;
import org.cmg.ml.sam.xtext.population.population.MaxExpression;
import org.cmg.ml.sam.xtext.population.population.MinExpression;
import org.cmg.ml.sam.xtext.population.population.ModExpression;
import org.cmg.ml.sam.xtext.population.population.MulDivExpression;
import org.cmg.ml.sam.xtext.population.population.NotFormula;
import org.cmg.ml.sam.xtext.population.population.NumberExpression;
import org.cmg.ml.sam.xtext.population.population.NumberLiteral;
import org.cmg.ml.sam.xtext.population.population.OrPctlFormula;
import org.cmg.ml.sam.xtext.population.population.PathFormula;
import org.cmg.ml.sam.xtext.population.population.PopulationExpression;
import org.cmg.ml.sam.xtext.population.population.PowExpression;
import org.cmg.ml.sam.xtext.population.population.ProbabilityFormula;
import org.cmg.ml.sam.xtext.population.population.ReferenceableName;
import org.cmg.ml.sam.xtext.population.population.RelationExpression;
import org.cmg.ml.sam.xtext.population.population.SinExpression;
import org.cmg.ml.sam.xtext.population.population.StateConstant;
import org.cmg.ml.sam.xtext.population.population.SumDiffExpression;
import org.cmg.ml.sam.xtext.population.population.TanExpression;
import org.cmg.ml.sam.xtext.population.population.TrueFormula;
import org.cmg.ml.sam.xtext.population.validation.PopulationDataType;

@SuppressWarnings("all")
public class TypeInference {
  protected PopulationDataType _getExpressionType(final OrPctlFormula e) {
    return PopulationDataType.STATE_FORMULA;
  }
  
  protected PopulationDataType _getExpressionType(final AndPctlFormula e) {
    return PopulationDataType.STATE_FORMULA;
  }
  
  protected PopulationDataType _getExpressionType(final RelationExpression e) {
    return PopulationDataType.STATE_FORMULA;
  }
  
  protected PopulationDataType _getExpressionType(final SumDiffExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final MulDivExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final DecimalLiteral e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final NumberLiteral e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final NotFormula e) {
    return PopulationDataType.STATE_FORMULA;
  }
  
  protected PopulationDataType _getExpressionType(final NumberExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final TrueFormula e) {
    return PopulationDataType.STATE_FORMULA;
  }
  
  protected PopulationDataType _getExpressionType(final FalseFormula e) {
    return PopulationDataType.STATE_FORMULA;
  }
  
  protected PopulationDataType _getExpressionType(final ProbabilityFormula e) {
    return PopulationDataType.STATE_FORMULA;
  }
  
  protected PopulationDataType _getExpressionType(final LiteralExpression e) {
    PopulationDataType _switchResult = null;
    ReferenceableName _ref = e.getRef();
    boolean _matched = false;
    if (!_matched) {
      if (_ref instanceof Constant) {
        _matched=true;
        _switchResult = PopulationDataType.NUMBER;
      }
    }
    if (!_matched) {
      if (_ref instanceof Formula) {
        _matched=true;
        _switchResult = PopulationDataType.STATE_FORMULA;
      }
    }
    if (!_matched) {
      if (_ref instanceof PathFormula) {
        _matched=true;
        _switchResult = PopulationDataType.PATH_FORMULA;
      }
    }
    if (!_matched) {
      if (_ref instanceof StateConstant) {
        _matched=true;
        _switchResult = PopulationDataType.STATE_FORMULA;
      }
    }
    return _switchResult;
  }
  
  protected PopulationDataType _getExpressionType(final PopulationExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final LogExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final ModExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final PowExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final FloorExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final CeilExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final MinExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final MaxExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final SinExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final CosExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final TanExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final ATanExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final ASinExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  protected PopulationDataType _getExpressionType(final ACosExpression e) {
    return PopulationDataType.NUMBER;
  }
  
  public PopulationDataType getExpressionType(final Expression e) {
    if (e instanceof DecimalLiteral) {
      return _getExpressionType((DecimalLiteral)e);
    } else if (e instanceof NumberLiteral) {
      return _getExpressionType((NumberLiteral)e);
    } else if (e instanceof ACosExpression) {
      return _getExpressionType((ACosExpression)e);
    } else if (e instanceof ASinExpression) {
      return _getExpressionType((ASinExpression)e);
    } else if (e instanceof ATanExpression) {
      return _getExpressionType((ATanExpression)e);
    } else if (e instanceof AndPctlFormula) {
      return _getExpressionType((AndPctlFormula)e);
    } else if (e instanceof CeilExpression) {
      return _getExpressionType((CeilExpression)e);
    } else if (e instanceof CosExpression) {
      return _getExpressionType((CosExpression)e);
    } else if (e instanceof FalseFormula) {
      return _getExpressionType((FalseFormula)e);
    } else if (e instanceof FloorExpression) {
      return _getExpressionType((FloorExpression)e);
    } else if (e instanceof LiteralExpression) {
      return _getExpressionType((LiteralExpression)e);
    } else if (e instanceof LogExpression) {
      return _getExpressionType((LogExpression)e);
    } else if (e instanceof MaxExpression) {
      return _getExpressionType((MaxExpression)e);
    } else if (e instanceof MinExpression) {
      return _getExpressionType((MinExpression)e);
    } else if (e instanceof ModExpression) {
      return _getExpressionType((ModExpression)e);
    } else if (e instanceof MulDivExpression) {
      return _getExpressionType((MulDivExpression)e);
    } else if (e instanceof NotFormula) {
      return _getExpressionType((NotFormula)e);
    } else if (e instanceof NumberExpression) {
      return _getExpressionType((NumberExpression)e);
    } else if (e instanceof OrPctlFormula) {
      return _getExpressionType((OrPctlFormula)e);
    } else if (e instanceof PopulationExpression) {
      return _getExpressionType((PopulationExpression)e);
    } else if (e instanceof PowExpression) {
      return _getExpressionType((PowExpression)e);
    } else if (e instanceof ProbabilityFormula) {
      return _getExpressionType((ProbabilityFormula)e);
    } else if (e instanceof RelationExpression) {
      return _getExpressionType((RelationExpression)e);
    } else if (e instanceof SinExpression) {
      return _getExpressionType((SinExpression)e);
    } else if (e instanceof SumDiffExpression) {
      return _getExpressionType((SumDiffExpression)e);
    } else if (e instanceof TanExpression) {
      return _getExpressionType((TanExpression)e);
    } else if (e instanceof TrueFormula) {
      return _getExpressionType((TrueFormula)e);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(e).toString());
    }
  }
}
