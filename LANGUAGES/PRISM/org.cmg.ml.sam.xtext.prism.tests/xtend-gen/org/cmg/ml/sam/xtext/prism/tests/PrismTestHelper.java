package org.cmg.ml.sam.xtext.prism.tests;

import org.cmg.ml.sam.core.logic.StateFormula;
import org.cmg.ml.sam.core.logic.Until;
import org.cmg.ml.sam.core.mc.pomc.OnTheFlyProbabilisticModelChecker;
import org.cmg.ml.sam.prism.PrismState;
import org.cmg.ml.sam.xtext.prism.PrismInjectorProvider;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Assert;

@InjectWith(PrismInjectorProvider.class)
@SuppressWarnings("all")
public class PrismTestHelper {
  public String assertBoundUntilProbability(final PrismState s, final StateFormula<PrismState> left, final int bound, final StateFormula<PrismState> right, final double expected, final double delta) {
    String _xblockexpression = null;
    {
      Until<PrismState> u = new Until<PrismState>(left, Integer.valueOf(bound), right);
      InputOutput.<String>println(((("Checking " + s) + " sat ") + u));
      OnTheFlyProbabilisticModelChecker<PrismState> opmc = new OnTheFlyProbabilisticModelChecker<PrismState>();
      long ct = System.currentTimeMillis();
      double res = opmc.getProbability(s, u);
      long _currentTimeMillis = System.currentTimeMillis();
      long _minus = (_currentTimeMillis - ct);
      ct = _minus;
      Assert.assertEquals(expected, res, delta);
      _xblockexpression = InputOutput.<String>println(("Time: " + Long.valueOf(ct)));
    }
    return _xblockexpression;
  }
  
  public void assertUntilProbability(final PrismState s, final StateFormula<PrismState> left, final StateFormula<PrismState> right, final double expected, final double delta) {
    Until<PrismState> u = new Until<PrismState>(left, right);
    InputOutput.<String>println(((("Checking " + s) + " sat ") + u));
    OnTheFlyProbabilisticModelChecker<PrismState> opmc = new OnTheFlyProbabilisticModelChecker<PrismState>();
    long ct = System.currentTimeMillis();
    double res = opmc.getProbability(s, u);
    long _currentTimeMillis = System.currentTimeMillis();
    long _minus = (_currentTimeMillis - ct);
    ct = _minus;
    InputOutput.<String>println(("Time: " + Long.valueOf(ct)));
    Assert.assertEquals(expected, res, delta);
  }
}
