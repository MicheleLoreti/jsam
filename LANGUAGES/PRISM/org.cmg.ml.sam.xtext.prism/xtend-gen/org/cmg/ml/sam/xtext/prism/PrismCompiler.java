package org.cmg.ml.sam.xtext.prism;

import java.util.List;
import org.cmg.ml.sam.core.Filter;
import org.cmg.ml.sam.prism.PrismModel;
import org.cmg.ml.sam.util.OnTheFlyJavaCompiler;
import org.cmg.ml.sam.xtext.prism.generator.ClassGenerator;
import org.cmg.ml.sam.xtext.prism.generator.ModuleStructureGenerator;
import org.cmg.ml.sam.xtext.prism.prism.Constant;
import org.cmg.ml.sam.xtext.prism.prism.Element;
import org.cmg.ml.sam.xtext.prism.prism.Formula;
import org.cmg.ml.sam.xtext.prism.prism.Model;
import org.cmg.ml.sam.xtext.prism.prism.Module;
import org.cmg.ml.sam.xtext.prism.prism.PathFormulaDeclaration;
import org.cmg.ml.sam.xtext.prism.prism.PrismSystem;
import org.cmg.ml.sam.xtext.prism.prism.StateFormulaDeclaration;
import org.cmg.ml.sam.xtext.prism.prism.Variable;
import org.cmg.ml.sam.xtext.prism.util.ModuleStructure;
import org.cmg.ml.sam.xtext.prism.util.SymbolTable;
import org.cmg.ml.sam.xtext.prism.util.SymbolTableCollector;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class PrismCompiler {
  private OnTheFlyJavaCompiler compiler;
  
  private SymbolTableCollector stc = new SymbolTableCollector();
  
  private ModuleStructureGenerator generator = new ModuleStructureGenerator();
  
  public PrismCompiler(final OnTheFlyJavaCompiler compiler) {
    this.compiler = compiler;
  }
  
  public PrismModel compile(final Model m) {
    try {
      PrismModel _xblockexpression = null;
      {
        SymbolTable st = this.stc.collecSymbols(m);
        EList<Element> _elements = m.getElements();
        final Function1<Element, ModuleStructure> _function = new Function1<Element, ModuleStructure>() {
          @Override
          public ModuleStructure apply(final Element e) {
            ModuleStructure _switchResult = null;
            boolean _matched = false;
            if (!_matched) {
              if (e instanceof Module) {
                _matched=true;
                _switchResult = PrismCompiler.this.generator.generateStructure(((Module)e));
              }
            }
            if (!_matched) {
              _switchResult = null;
            }
            return _switchResult;
          }
        };
        List<ModuleStructure> _map = ListExtensions.<Element, ModuleStructure>map(_elements, _function);
        Iterable<ModuleStructure> structures = IterableExtensions.<ModuleStructure>filterNull(_map);
        ClassGenerator classGenerator = new ClassGenerator();
        classGenerator.setSymbolTable(st);
        EList<Element> _elements_1 = m.getElements();
        final Function1<Element, Boolean> _function_1 = new Function1<Element, Boolean>() {
          @Override
          public Boolean apply(final Element e) {
            return Boolean.valueOf((e instanceof Constant));
          }
        };
        Iterable<Element> _filter = IterableExtensions.<Element>filter(_elements_1, _function_1);
        EList<Element> _elements_2 = m.getElements();
        final Function1<Element, Boolean> _function_2 = new Function1<Element, Boolean>() {
          @Override
          public Boolean apply(final Element e) {
            return Boolean.valueOf((e instanceof Formula));
          }
        };
        Iterable<Element> _filter_1 = IterableExtensions.<Element>filter(_elements_2, _function_2);
        EList<Element> _elements_3 = m.getElements();
        final Function1<Element, Boolean> _function_3 = new Function1<Element, Boolean>() {
          @Override
          public Boolean apply(final Element e) {
            return Boolean.valueOf((e instanceof Variable));
          }
        };
        Iterable<Element> _filter_2 = IterableExtensions.<Element>filter(_elements_3, _function_3);
        EList<Element> _elements_4 = m.getElements();
        final Function1<Element, Boolean> _function_4 = new Function1<Element, Boolean>() {
          @Override
          public Boolean apply(final Element e) {
            return Boolean.valueOf((e instanceof PathFormulaDeclaration));
          }
        };
        Iterable<Element> _filter_3 = IterableExtensions.<Element>filter(_elements_4, _function_4);
        EList<Element> _elements_5 = m.getElements();
        final Function1<Element, Boolean> _function_5 = new Function1<Element, Boolean>() {
          @Override
          public Boolean apply(final Element e) {
            return Boolean.valueOf((e instanceof StateFormulaDeclaration));
          }
        };
        Iterable<Element> _filter_4 = IterableExtensions.<Element>filter(_elements_5, _function_5);
        EList<Element> _elements_6 = m.getElements();
        final Function1<Element, Boolean> _function_6 = new Function1<Element, Boolean>() {
          @Override
          public Boolean apply(final Element e) {
            return Boolean.valueOf((e instanceof PrismSystem));
          }
        };
        Element _findFirst = IterableExtensions.<Element>findFirst(_elements_6, _function_6);
        CharSequence code = classGenerator.generateClass("test.pkg", "test", _filter, _filter_1, _filter_2, _filter_3, _filter_4, _findFirst, structures);
        InputOutput.<CharSequence>println(code);
        ClassLoader _systemClassLoader = ClassLoader.getSystemClassLoader();
        this.compiler.setParentClassLoader(_systemClassLoader);
        this.compiler.addClassPathOfClass(PrismModel.class);
        this.compiler.addClassPathOfClass(Filter.class);
        String _string = code.toString();
        Class<?> c = this.compiler.compileToClass("test.pkg.test", _string);
        Object _newInstance = c.newInstance();
        _xblockexpression = ((PrismModel) _newInstance);
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
