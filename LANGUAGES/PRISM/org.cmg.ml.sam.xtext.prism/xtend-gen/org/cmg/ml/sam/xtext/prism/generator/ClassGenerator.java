package org.cmg.ml.sam.xtext.prism.generator;

import com.google.common.base.Objects;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.cmg.ml.sam.xtext.prism.prism.ActionRenaming;
import org.cmg.ml.sam.xtext.prism.prism.AlphabetisedParallelComposition;
import org.cmg.ml.sam.xtext.prism.prism.AndExpression;
import org.cmg.ml.sam.xtext.prism.prism.AsynchronousParallelComposition;
import org.cmg.ml.sam.xtext.prism.prism.AtomicStateFormula;
import org.cmg.ml.sam.xtext.prism.prism.BooleanType;
import org.cmg.ml.sam.xtext.prism.prism.Bound;
import org.cmg.ml.sam.xtext.prism.prism.Constant;
import org.cmg.ml.sam.xtext.prism.prism.ConstantType;
import org.cmg.ml.sam.xtext.prism.prism.DecimalLiteral;
import org.cmg.ml.sam.xtext.prism.prism.Element;
import org.cmg.ml.sam.xtext.prism.prism.Expression;
import org.cmg.ml.sam.xtext.prism.prism.False;
import org.cmg.ml.sam.xtext.prism.prism.Formula;
import org.cmg.ml.sam.xtext.prism.prism.Hiding;
import org.cmg.ml.sam.xtext.prism.prism.IfAndOnlyIf;
import org.cmg.ml.sam.xtext.prism.prism.IfThenElse;
import org.cmg.ml.sam.xtext.prism.prism.Implies;
import org.cmg.ml.sam.xtext.prism.prism.IntegerLiteral;
import org.cmg.ml.sam.xtext.prism.prism.IntervalType;
import org.cmg.ml.sam.xtext.prism.prism.Module;
import org.cmg.ml.sam.xtext.prism.prism.ModuleReference;
import org.cmg.ml.sam.xtext.prism.prism.MulExpression;
import org.cmg.ml.sam.xtext.prism.prism.Negation;
import org.cmg.ml.sam.xtext.prism.prism.NegationFormula;
import org.cmg.ml.sam.xtext.prism.prism.NextFormula;
import org.cmg.ml.sam.xtext.prism.prism.OrExpression;
import org.cmg.ml.sam.xtext.prism.prism.PathFormula;
import org.cmg.ml.sam.xtext.prism.prism.PathFormulaDeclaration;
import org.cmg.ml.sam.xtext.prism.prism.PrismSystem;
import org.cmg.ml.sam.xtext.prism.prism.ProbabilityFormula;
import org.cmg.ml.sam.xtext.prism.prism.Reference;
import org.cmg.ml.sam.xtext.prism.prism.RelExpression;
import org.cmg.ml.sam.xtext.prism.prism.Relations;
import org.cmg.ml.sam.xtext.prism.prism.Renaming;
import org.cmg.ml.sam.xtext.prism.prism.RestrictedParallelComposition;
import org.cmg.ml.sam.xtext.prism.prism.StateAnd;
import org.cmg.ml.sam.xtext.prism.prism.StateFormula;
import org.cmg.ml.sam.xtext.prism.prism.StateFormulaDeclaration;
import org.cmg.ml.sam.xtext.prism.prism.StateOr;
import org.cmg.ml.sam.xtext.prism.prism.SumExpression;
import org.cmg.ml.sam.xtext.prism.prism.True;
import org.cmg.ml.sam.xtext.prism.prism.Type;
import org.cmg.ml.sam.xtext.prism.prism.UntilFormula;
import org.cmg.ml.sam.xtext.prism.prism.UpdateElement;
import org.cmg.ml.sam.xtext.prism.prism.Variable;
import org.cmg.ml.sam.xtext.prism.util.ModuleStructure;
import org.cmg.ml.sam.xtext.prism.util.RenamedElement;
import org.cmg.ml.sam.xtext.prism.util.RuleStructure;
import org.cmg.ml.sam.xtext.prism.util.SymbolTable;
import org.cmg.ml.sam.xtext.prism.util.UpdateStructure;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.util.Wrapper;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pair;

@SuppressWarnings("all")
public class ClassGenerator {
  private SymbolTable symbolTable;
  
  private int variableIndex = (-1);
  
  public SymbolTable setSymbolTable(final SymbolTable symbolTable) {
    return this.symbolTable = symbolTable;
  }
  
  public CharSequence generateClass(final String packageName, final String moduleName, final Iterable<Element> constants, final Iterable<Element> formula, final Iterable<Element> globalVariables, final Iterable<Element> pathFormulae, final Iterable<Element> stateFormulae, final Element system, final Iterable<ModuleStructure> mlist) {
    CharSequence _xblockexpression = null;
    {
      int _size = IterableExtensions.size(globalVariables);
      this.variableIndex = _size;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("/*");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("* Java class automatically generetated by jSAM.");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("* ");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("* Source module: ");
      _builder.append(moduleName, " ");
      _builder.newLineIfNotEmpty();
      _builder.append(" ");
      _builder.append("*/");
      _builder.newLine();
      _builder.append("package ");
      _builder.append(packageName, "");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("import java.math.BigInteger;");
      _builder.newLine();
      _builder.append("import org.apache.commons.math.random.RandomGenerator;");
      _builder.newLine();
      _builder.append("import org.cmg.ml.sam.prism.*;");
      _builder.newLine();
      _builder.append("import org.cmg.ml.sam.core.*;");
      _builder.newLine();
      _builder.append("import org.cmg.ml.sam.core.logic.*;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("public class ");
      _builder.append(moduleName, "");
      _builder.append(" extends PrismModel {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// START CONSTANTS");
      _builder.newLine();
      {
        for(final Element c : constants) {
          _builder.append("\t");
          CharSequence _generateDeclarationCode = this.generateDeclarationCode(c);
          _builder.append(_generateDeclarationCode, "\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.append("// END CONSTANTS");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// START ACTIONS");
      _builder.newLine();
      {
        HashSet<String> _actions = this.symbolTable.getActions();
        Iterable<Pair<Integer, String>> _generateIndexes = this.<String>generateIndexes(_actions);
        for(final Pair<Integer, String> a : _generateIndexes) {
          _builder.append("\t");
          _builder.append("public static int _LABEL_");
          String _value = a.getValue();
          _builder.append(_value, "\t");
          _builder.append(" = ");
          Integer _key = a.getKey();
          int _plus = ((_key).intValue() + 1);
          _builder.append(_plus, "\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.append("// END ACTIONS");
      _builder.newLine();
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// START GLOBAL VARIABLES");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//");
      _builder.newLine();
      {
        Iterable<Pair<Integer, Element>> _generateIndexes_1 = this.<Element>generateIndexes(globalVariables);
        for(final Pair<Integer, Element> v : _generateIndexes_1) {
          _builder.append("\t");
          CharSequence _generateDeclarationCode_1 = this.generateDeclarationCode(v);
          _builder.append(_generateDeclarationCode_1, "\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.append("//");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// END GLOBAL VARIABLES");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//");
      _builder.newLine();
      _builder.newLine();
      {
        for(final ModuleStructure m : mlist) {
          _builder.append("\t");
          _builder.newLine();
          _builder.append("\t");
          _builder.newLine();
          _builder.newLine();
          _builder.append("\t");
          _builder.append("//");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("// START LOCAL VARIABLES MODULE ");
          String _name = m.getName();
          _builder.append(_name, "\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("//");
          _builder.newLine();
          {
            Iterable<String> _localVariables = m.getLocalVariables();
            Iterable<Pair<Integer, String>> _generateLocalVariableIndexes = this.generateLocalVariableIndexes(_localVariables);
            for(final Pair<Integer, String> v_1 : _generateLocalVariableIndexes) {
              _builder.append("\t");
              _builder.append("public static int _LOCAL_");
              String _value_1 = v_1.getValue();
              _builder.append(_value_1, "\t");
              _builder.append(" = ");
              Integer _key_1 = v_1.getKey();
              _builder.append(_key_1, "\t");
              _builder.append(";");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.append("//");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("// END LOCAL VARIABLES MODULE ");
          String _name_1 = m.getName();
          _builder.append(_name_1, "\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("//");
          _builder.newLine();
          _builder.newLine();
          _builder.append("\t");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// STATE ENUMERATOR");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("public static StateEnumerator generateStateEnumerator() {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int[] lowerBounds = new int[");
      int _numberOfVariables = this.symbolTable.getNumberOfVariables();
      _builder.append(_numberOfVariables, "\t\t");
      _builder.append("];");
      _builder.newLineIfNotEmpty();
      {
        Set<String> _globalVariables = this.symbolTable.getGlobalVariables();
        for(final String v_2 : _globalVariables) {
          _builder.append("\t\t");
          _builder.append("lowerBounds[");
          CharSequence _globalVariableCode = this.symbolTable.getGlobalVariableCode(v_2);
          _builder.append(_globalVariableCode, "\t\t");
          _builder.append("] = ");
          Type _typeOf = this.symbolTable.getTypeOf(v_2);
          CharSequence _lowerBound = this.lowerBound(_typeOf);
          _builder.append(_lowerBound, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        Set<String> _localVariables_1 = this.symbolTable.getLocalVariables();
        for(final String v_3 : _localVariables_1) {
          _builder.append("\t\t");
          _builder.append("lowerBounds[");
          CharSequence _localVariableCode = this.symbolTable.getLocalVariableCode(v_3);
          _builder.append(_localVariableCode, "\t\t");
          _builder.append("] = ");
          Type _typeOf_1 = this.symbolTable.getTypeOf(v_3);
          CharSequence _lowerBound_1 = this.lowerBound(_typeOf_1);
          _builder.append(_lowerBound_1, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int[] upperBounds = new int[");
      int _numberOfVariables_1 = this.symbolTable.getNumberOfVariables();
      _builder.append(_numberOfVariables_1, "\t\t");
      _builder.append("];");
      _builder.newLineIfNotEmpty();
      {
        Set<String> _globalVariables_1 = this.symbolTable.getGlobalVariables();
        for(final String v_4 : _globalVariables_1) {
          _builder.append("\t\t");
          _builder.append("upperBounds[");
          CharSequence _globalVariableCode_1 = this.symbolTable.getGlobalVariableCode(v_4);
          _builder.append(_globalVariableCode_1, "\t\t");
          _builder.append("] = ");
          Type _typeOf_2 = this.symbolTable.getTypeOf(v_4);
          CharSequence _upperBound = this.upperBound(_typeOf_2);
          _builder.append(_upperBound, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        Set<String> _localVariables_2 = this.symbolTable.getLocalVariables();
        for(final String v_5 : _localVariables_2) {
          _builder.append("\t\t");
          _builder.append("upperBounds[");
          CharSequence _localVariableCode_1 = this.symbolTable.getLocalVariableCode(v_5);
          _builder.append(_localVariableCode_1, "\t\t");
          _builder.append("] = ");
          Type _typeOf_3 = this.symbolTable.getTypeOf(v_5);
          CharSequence _upperBound_1 = this.upperBound(_typeOf_3);
          _builder.append(_upperBound_1, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("return StateEnumerator.createEnumerator( lowerBounds , upperBounds );");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("public static StateEnumerator enumerator = generateStateEnumerator();");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// END STATE ENUMERATOR");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// START FORMULAE");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//");
      _builder.newLine();
      {
        for(final Element f : formula) {
          _builder.append("\t");
          CharSequence _generateDeclarationCode_2 = this.generateDeclarationCode(f);
          _builder.append(_generateDeclarationCode_2, "\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.append("// ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// END FORMULAE");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//");
      _builder.newLine();
      _builder.newLine();
      {
        for(final ModuleStructure m_1 : mlist) {
          _builder.newLine();
          _builder.append("\t");
          _builder.append("//");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("// START MODULE ");
          String _name_2 = m_1.getName();
          _builder.append(_name_2, "\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("//");
          _builder.newLine();
          {
            Iterable<Pair<Integer, RuleStructure>> _indexedRules = m_1.getIndexedRules();
            for(final Pair<Integer, RuleStructure> r : _indexedRules) {
              _builder.newLine();
              _builder.append("\t");
              _builder.append("//");
              _builder.newLine();
              _builder.append("\t");
              _builder.append("// START RULE ");
              Integer _key_2 = r.getKey();
              _builder.append(_key_2, "\t");
              _builder.append(" ");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("//");
              _builder.newLine();
              _builder.append("\t");
              _builder.append("public static Filter<BigInteger> GUARD_RULE_");
              Integer _key_3 = r.getKey();
              _builder.append(_key_3, "\t");
              _builder.append("_");
              String _name_3 = m_1.getName();
              _builder.append(_name_3, "\t");
              _builder.append(" = ");
              RuleStructure _value_2 = r.getValue();
              Iterable<RenamedElement<Expression>> _guard = _value_2.getGuard();
              CharSequence _generateGuardFilter = this.generateGuardFilter(_guard);
              _builder.append(_generateGuardFilter, "\t");
              _builder.append(";");
              _builder.newLineIfNotEmpty();
              _builder.newLine();
              {
                RuleStructure _value_3 = r.getValue();
                Iterable<Pair<Integer, UpdateStructure>> _indexedCommands = _value_3.getIndexedCommands();
                for(final Pair<Integer, UpdateStructure> c_1 : _indexedCommands) {
                  _builder.append("\t");
                  _builder.append("public static Command COMMAND_");
                  Integer _key_4 = c_1.getKey();
                  _builder.append(_key_4, "\t");
                  _builder.append("_RULE_");
                  Integer _key_5 = r.getKey();
                  _builder.append(_key_5, "\t");
                  _builder.append("_");
                  String _name_4 = m_1.getName();
                  _builder.append(_name_4, "\t");
                  _builder.append(" = ");
                  UpdateStructure _value_4 = c_1.getValue();
                  CharSequence _generateCommand = this.generateCommand(_value_4);
                  _builder.append(_generateCommand, "\t");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.newLine();
              _builder.append("\t");
              _builder.append("public static Rule RULE_");
              Integer _key_6 = r.getKey();
              _builder.append(_key_6, "\t");
              _builder.append("_");
              String _name_5 = m_1.getName();
              _builder.append(_name_5, "\t");
              _builder.append(" = new Rule( ");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("\t");
              {
                RuleStructure _value_5 = r.getValue();
                String _action = _value_5.getAction();
                boolean _notEquals = (!Objects.equal(_action, null));
                if (_notEquals) {
                  _builder.append("_LABEL_");
                  RuleStructure _value_6 = r.getValue();
                  String _action_1 = _value_6.getAction();
                  _builder.append(_action_1, "\t\t");
                  _builder.append(" ,");
                }
              }
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("\t");
              _builder.append("GUARD_RULE_");
              Integer _key_7 = r.getKey();
              _builder.append(_key_7, "\t\t");
              _builder.append("_");
              String _name_6 = m_1.getName();
              _builder.append(_name_6, "\t\t");
              _builder.append(" , ");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("\t");
              _builder.append("new Command[] { ");
              {
                RuleStructure _value_7 = r.getValue();
                Iterable<Pair<Integer, UpdateStructure>> _indexedCommands_1 = _value_7.getIndexedCommands();
                boolean _hasElements = false;
                for(final Pair<Integer, UpdateStructure> c_2 : _indexedCommands_1) {
                  if (!_hasElements) {
                    _hasElements = true;
                  } else {
                    _builder.appendImmediate(",", "\t\t");
                  }
                  _builder.append(" COMMAND_");
                  Integer _key_8 = c_2.getKey();
                  _builder.append(_key_8, "\t\t");
                  _builder.append("_RULE_");
                  Integer _key_9 = r.getKey();
                  _builder.append(_key_9, "\t\t");
                  _builder.append("_");
                  String _name_7 = m_1.getName();
                  _builder.append(_name_7, "\t\t");
                  _builder.append(" ");
                }
              }
              _builder.append("} ");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append(");");
              _builder.newLine();
              _builder.append("\t");
              _builder.append("//");
              _builder.newLine();
              _builder.append("\t");
              _builder.append("// END RULE ");
              Integer _key_10 = r.getKey();
              _builder.append(_key_10, "\t");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("//");
              _builder.newLine();
            }
          }
          _builder.append("\t");
          _builder.append("\t");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("public static PrismSystem MODULE_");
          String _name_8 = m_1.getName();
          _builder.append(_name_8, "\t");
          _builder.append(" = new  BasicPrismSystem(");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          HashSet<String> _actions_1 = this.symbolTable.getActions();
          int _size_1 = _actions_1.size();
          int _plus_1 = (_size_1 + 1);
          _builder.append(_plus_1, "\t\t");
          _builder.append(" ");
          _builder.newLineIfNotEmpty();
          {
            Iterable<Pair<Integer, RuleStructure>> _indexedRules_1 = m_1.getIndexedRules();
            for(final Pair<Integer, RuleStructure> r_1 : _indexedRules_1) {
              _builder.append("\t");
              _builder.append("\t");
              _builder.append(", RULE_");
              Integer _key_11 = r_1.getKey();
              _builder.append(_key_11, "\t\t");
              _builder.append("_");
              String _name_9 = m_1.getName();
              _builder.append(_name_9, "\t\t");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.append(");\t");
          _builder.newLine();
          _builder.newLine();
          _builder.append("\t");
          _builder.append("//");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("// END MODULE ");
          String _name_10 = m_1.getName();
          _builder.append(_name_10, "\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("//");
          _builder.newLine();
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("@Override");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("protected PrismSystem createSystem() {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("return ");
      {
        boolean _notEquals_1 = (!Objects.equal(system, null));
        if (_notEquals_1) {
          CharSequence _generateDeclarationCode_3 = this.generateDeclarationCode(system);
          _builder.append(_generateDeclarationCode_3, "\t\t");
        } else {
          CharSequence _defaultInstantiation = this.defaultInstantiation(mlist);
          _builder.append(_defaultInstantiation, "\t\t");
        }
      }
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("public ");
      _builder.append(moduleName, "\t");
      _builder.append("() {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("super( ModelType.DTMC , ");
      HashSet<String> _actions_2 = this.symbolTable.getActions();
      int _size_2 = _actions_2.size();
      int _plus_2 = (_size_2 + 1);
      _builder.append(_plus_2, "\t\t");
      _builder.append(" );\t");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("int[] lowerBounds = new int[");
      int _numberOfVariables_2 = this.symbolTable.getNumberOfVariables();
      _builder.append(_numberOfVariables_2, "\t\t");
      _builder.append("];");
      _builder.newLineIfNotEmpty();
      {
        Set<String> _globalVariables_2 = this.symbolTable.getGlobalVariables();
        for(final String v_6 : _globalVariables_2) {
          _builder.append("\t\t");
          _builder.append("lowerBounds[");
          CharSequence _globalVariableCode_2 = this.symbolTable.getGlobalVariableCode(v_6);
          _builder.append(_globalVariableCode_2, "\t\t");
          _builder.append("] = ");
          Type _typeOf_4 = this.symbolTable.getTypeOf(v_6);
          CharSequence _lowerBound_2 = this.lowerBound(_typeOf_4);
          _builder.append(_lowerBound_2, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        Set<String> _localVariables_3 = this.symbolTable.getLocalVariables();
        for(final String v_7 : _localVariables_3) {
          _builder.append("\t\t");
          _builder.append("lowerBounds[");
          CharSequence _localVariableCode_2 = this.symbolTable.getLocalVariableCode(v_7);
          _builder.append(_localVariableCode_2, "\t\t");
          _builder.append("] = ");
          Type _typeOf_5 = this.symbolTable.getTypeOf(v_7);
          CharSequence _lowerBound_3 = this.lowerBound(_typeOf_5);
          _builder.append(_lowerBound_3, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int[] upperBounds = new int[");
      int _numberOfVariables_3 = this.symbolTable.getNumberOfVariables();
      _builder.append(_numberOfVariables_3, "\t\t");
      _builder.append("];");
      _builder.newLineIfNotEmpty();
      {
        Set<String> _globalVariables_3 = this.symbolTable.getGlobalVariables();
        for(final String v_8 : _globalVariables_3) {
          _builder.append("\t\t");
          _builder.append("upperBounds[");
          CharSequence _globalVariableCode_3 = this.symbolTable.getGlobalVariableCode(v_8);
          _builder.append(_globalVariableCode_3, "\t\t");
          _builder.append("] = ");
          Type _typeOf_6 = this.symbolTable.getTypeOf(v_8);
          CharSequence _upperBound_2 = this.upperBound(_typeOf_6);
          _builder.append(_upperBound_2, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        Set<String> _localVariables_4 = this.symbolTable.getLocalVariables();
        for(final String v_9 : _localVariables_4) {
          _builder.append("\t\t");
          _builder.append("upperBounds[");
          CharSequence _localVariableCode_3 = this.symbolTable.getLocalVariableCode(v_9);
          _builder.append(_localVariableCode_3, "\t\t");
          _builder.append("] = ");
          Type _typeOf_7 = this.symbolTable.getTypeOf(v_9);
          CharSequence _upperBound_3 = this.upperBound(_typeOf_7);
          _builder.append(_upperBound_3, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("setStateEnumerator( StateEnumerator.createEnumerator( lowerBounds , upperBounds ) );");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int[] startData = new int[");
      int _numberOfVariables_4 = this.symbolTable.getNumberOfVariables();
      _builder.append(_numberOfVariables_4, "\t\t");
      _builder.append("];");
      _builder.newLineIfNotEmpty();
      {
        Set<String> _globalVariables_4 = this.symbolTable.getGlobalVariables();
        for(final String v_10 : _globalVariables_4) {
          _builder.append("\t\t");
          _builder.append("startData[");
          CharSequence _globalVariableCode_4 = this.symbolTable.getGlobalVariableCode(v_10);
          _builder.append(_globalVariableCode_4, "\t\t");
          _builder.append("] = ");
          RenamedElement<Expression> _initExpression = this.symbolTable.getInitExpression(v_10);
          CharSequence _initExpressionCode = this.getInitExpressionCode(_initExpression);
          _builder.append(_initExpressionCode, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        Set<String> _localVariables_5 = this.symbolTable.getLocalVariables();
        for(final String v_11 : _localVariables_5) {
          _builder.append("\t\t");
          _builder.append("startData[");
          CharSequence _localVariableCode_4 = this.symbolTable.getLocalVariableCode(v_11);
          _builder.append(_localVariableCode_4, "\t\t");
          _builder.append("] = ");
          RenamedElement<Expression> _initExpression_1 = this.symbolTable.getInitExpression(v_11);
          CharSequence _initExpressionCode_1 = this.getInitExpressionCode(_initExpression_1);
          _builder.append(_initExpressionCode_1, "\t\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t\t");
      _builder.append("start = new PrismState(getState(startData) ,this);\t\t\t\t");
      _builder.newLine();
      {
        for(final Element f_1 : pathFormulae) {
          _builder.append("\t\t");
          CharSequence _generateFormulaInizialization = this.generateFormulaInizialization(f_1);
          _builder.append(_generateFormulaInizialization, "\t\t");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        for(final Element f_2 : stateFormulae) {
          _builder.append("\t\t");
          CharSequence _generateFormulaInizialization_1 = this.generateFormulaInizialization(f_2);
          _builder.append(_generateFormulaInizialization_1, "\t\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("}\t\t");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence lowerBound(final Type t) {
    CharSequence _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (t instanceof BooleanType) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("0");
        _switchResult = _builder;
      }
    }
    if (!_matched) {
      if (t instanceof IntervalType) {
        _matched=true;
        Expression _min = ((IntervalType)t).getMin();
        _switchResult = this.generateExpressionCode(_min, null, false);
      }
    }
    return _switchResult;
  }
  
  public CharSequence upperBound(final Type t) {
    CharSequence _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (t instanceof BooleanType) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("1");
        _switchResult = _builder;
      }
    }
    if (!_matched) {
      if (t instanceof IntervalType) {
        _matched=true;
        Expression _max = ((IntervalType)t).getMax();
        _switchResult = this.generateExpressionCode(_max, null, false);
      }
    }
    return _switchResult;
  }
  
  protected CharSequence _generateFormulaInizialization(final Element e) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _generateFormulaInizialization(final PathFormulaDeclaration e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("setPathFormula(\"");
    String _name = e.getName();
    _builder.append(_name, "");
    _builder.append("\" , ");
    PathFormula _formula = e.getFormula();
    CharSequence _generatePathFormulaCode = this.generatePathFormulaCode(_formula);
    _builder.append(_generatePathFormulaCode, "");
    _builder.append(");");
    return _builder;
  }
  
  protected CharSequence _generateFormulaInizialization(final StateFormulaDeclaration e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("setStateFormula(\"");
    String _name = e.getName();
    _builder.append(_name, "");
    _builder.append("\" , ");
    StateFormula _formula = e.getFormula();
    CharSequence _generateStateFormulaCode = this.generateStateFormulaCode(_formula);
    _builder.append(_generateStateFormulaCode, "");
    _builder.append(");");
    return _builder;
  }
  
  protected CharSequence _generatePathFormulaCode(final NextFormula f) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new Next<PrismState>( ");
    StateFormula _arg = f.getArg();
    CharSequence _generateStateFormulaCode = this.generateStateFormulaCode(_arg);
    _builder.append(_generateStateFormulaCode, "");
    _builder.append(" )");
    return _builder;
  }
  
  protected CharSequence _generatePathFormulaCode(final UntilFormula f) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new Until<PrismState>( ");
    StateFormula _left = f.getLeft();
    CharSequence _generateStateFormulaCode = this.generateStateFormulaCode(_left);
    _builder.append(_generateStateFormulaCode, "");
    _builder.append(" ");
    {
      Bound _bound = f.getBound();
      boolean _notEquals = (!Objects.equal(_bound, null));
      if (_notEquals) {
        _builder.append(" , ");
        Bound _bound_1 = f.getBound();
        int _limit = _bound_1.getLimit();
        _builder.append(_limit, "");
        _builder.append(" ");
      }
    }
    _builder.append(" , ");
    StateFormula _right = f.getRight();
    CharSequence _generateStateFormulaCode_1 = this.generateStateFormulaCode(_right);
    _builder.append(_generateStateFormulaCode_1, "");
    _builder.append(") ");
    return _builder;
  }
  
  protected CharSequence _generateStateFormulaCode(final StateOr f) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new Or<PrismState>( ");
    StateFormula _left = f.getLeft();
    Object _generateStateFormulaCode = this.generateStateFormulaCode(_left);
    _builder.append(_generateStateFormulaCode, "");
    _builder.append(" , ");
    StateFormula _right = f.getRight();
    Object _generateStateFormulaCode_1 = this.generateStateFormulaCode(_right);
    _builder.append(_generateStateFormulaCode_1, "");
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generateStateFormulaCode(final StateAnd f) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new And<PrismState>( ");
    StateFormula _left = f.getLeft();
    Object _generateStateFormulaCode = this.generateStateFormulaCode(_left);
    _builder.append(_generateStateFormulaCode, "");
    _builder.append(" , ");
    StateFormula _right = f.getRight();
    Object _generateStateFormulaCode_1 = this.generateStateFormulaCode(_right);
    _builder.append(_generateStateFormulaCode_1, "");
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generateStateFormulaCode(final NegationFormula f) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new Not<PrismState>( ");
    StateFormula _argument = f.getArgument();
    Object _generateStateFormulaCode = this.generateStateFormulaCode(_argument);
    _builder.append(_generateStateFormulaCode, "");
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generateStateFormulaCode(final AtomicStateFormula f) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new Atomic<PrismState>( ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("new Predicate<PrismState>() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("//@Override");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("public boolean sat(PrismState state) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("BigInteger currentState = state.getState();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return ");
    Expression _exp = f.getExp();
    HashMap<String, String> _hashMap = new HashMap<String, String>();
    CharSequence _generateExpressionCode = this.generateExpressionCode(_exp, _hashMap, true);
    _builder.append(_generateExpressionCode, "\t\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("}\t\t\t\t\t\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _generateStateFormulaCode(final ProbabilityFormula f) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new PCheck<PrismState>( ");
    Relations _relation = f.getRelation();
    CharSequence _pCheckRelation = this.getPCheckRelation(_relation);
    _builder.append(_pCheckRelation, "");
    _builder.append(" , ");
    Expression _value = f.getValue();
    CharSequence _generateExpressionCode = this.generateExpressionCode(_value, null, false);
    _builder.append(_generateExpressionCode, "");
    _builder.append(" , ");
    PathFormula _path = f.getPath();
    Object _generatePathFormulaCode = this.generatePathFormulaCode(_path);
    _builder.append(_generatePathFormulaCode, "");
    _builder.append(")");
    return _builder;
  }
  
  public CharSequence getPCheckRelation(final Relations r) {
    CharSequence _switchResult = null;
    if (r != null) {
      switch (r) {
        case EQ:
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("Relation.EQ");
          _switchResult = _builder;
          break;
        case LEQ:
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("Relation.GEQ");
          _switchResult = _builder_1;
          break;
        case LSS:
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("Relation.LESS");
          _switchResult = _builder_2;
          break;
        case GEQ:
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("Relation.GEQ");
          _switchResult = _builder_3;
          break;
        case GTR:
          StringConcatenation _builder_4 = new StringConcatenation();
          _builder_4.append("Relation.GTR");
          _switchResult = _builder_4;
          break;
        default:
          break;
      }
    }
    return _switchResult;
  }
  
  public CharSequence getInitExpressionCode(final RenamedElement<Expression> e) {
    CharSequence _xifexpression = null;
    boolean _equals = Objects.equal(e, null);
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("0");
      _xifexpression = _builder;
    } else {
      Expression _element = e.getElement();
      Map<String, String> _renaming = e.getRenaming();
      _xifexpression = this.generateExpressionCode(_element, _renaming, false);
    }
    return _xifexpression;
  }
  
  public CharSequence defaultInstantiation(final Iterable<ModuleStructure> mlist) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("PrismSystem.alphabetisedParallelComposition( ");
    {
      boolean _hasElements = false;
      for(final ModuleStructure m : mlist) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        _builder.append("MODULE_");
        String _name = m.getName();
        _builder.append(_name, "");
      }
    }
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generateSystemInstantiationCode(final PrismSystem p) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _generateSystemInstantiationCode(final AlphabetisedParallelComposition p) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new AlphabetizedParalleSystem( ");
    PrismSystem _left = p.getLeft();
    Object _generateSystemInstantiationCode = this.generateSystemInstantiationCode(_left);
    _builder.append(_generateSystemInstantiationCode, "");
    _builder.append(" , ");
    PrismSystem _right = p.getRight();
    Object _generateSystemInstantiationCode_1 = this.generateSystemInstantiationCode(_right);
    _builder.append(_generateSystemInstantiationCode_1, "");
    _builder.append(" )");
    return _builder;
  }
  
  protected CharSequence _generateSystemInstantiationCode(final AsynchronousParallelComposition p) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new InterleavedParalleSystem( ");
    PrismSystem _left = p.getLeft();
    Object _generateSystemInstantiationCode = this.generateSystemInstantiationCode(_left);
    _builder.append(_generateSystemInstantiationCode, "");
    _builder.append(" , ");
    PrismSystem _right = p.getRight();
    Object _generateSystemInstantiationCode_1 = this.generateSystemInstantiationCode(_right);
    _builder.append(_generateSystemInstantiationCode_1, "");
    _builder.append(" )");
    return _builder;
  }
  
  protected CharSequence _generateSystemInstantiationCode(final RestrictedParallelComposition p) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new ControlledParallelSystem( ");
    PrismSystem _left = p.getLeft();
    Object _generateSystemInstantiationCode = this.generateSystemInstantiationCode(_left);
    _builder.append(_generateSystemInstantiationCode, "");
    _builder.append(" , ");
    EList<String> _actions = p.getActions();
    CharSequence _synchronizationArray = this.getSynchronizationArray(_actions);
    _builder.append(_synchronizationArray, "");
    _builder.append(" , ");
    PrismSystem _right = p.getRight();
    Object _generateSystemInstantiationCode_1 = this.generateSystemInstantiationCode(_right);
    _builder.append(_generateSystemInstantiationCode_1, "");
    _builder.append(" )");
    return _builder;
  }
  
  public CharSequence getSynchronizationArray(final Iterable<String> sync) {
    CharSequence _xblockexpression = null;
    {
      HashSet<String> _actions = this.symbolTable.getActions();
      final Function1<String, Boolean> _function = new Function1<String, Boolean>() {
        @Override
        public Boolean apply(final String a) {
          final Function1<String, Boolean> _function = new Function1<String, Boolean>() {
            @Override
            public Boolean apply(final String b) {
              return Boolean.valueOf(Objects.equal(a, b));
            }
          };
          return Boolean.valueOf(IterableExtensions.<String>exists(sync, _function));
        }
      };
      Iterable<Boolean> _map = IterableExtensions.<String, Boolean>map(_actions, _function);
      final Function1<Boolean, String> _function_1 = new Function1<Boolean, String>() {
        @Override
        public String apply(final Boolean b) {
          String _xifexpression = null;
          if ((b).booleanValue()) {
            _xifexpression = "true";
          } else {
            _xifexpression = "false";
          }
          return _xifexpression;
        }
      };
      Iterable<String> syncSet = IterableExtensions.<Boolean, String>map(_map, _function_1);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("new boolean[] { false , ");
      String _join = IterableExtensions.join(syncSet, ",");
      _builder.append(_join, "");
      _builder.append(" }");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateSystemInstantiationCode(final Hiding p) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new SystemHiding( ");
    EList<String> _actions = p.getActions();
    CharSequence _hideArray = this.getHideArray(_actions);
    _builder.append(_hideArray, "");
    _builder.append(" , ");
    PrismSystem _argument = p.getArgument();
    Object _generateSystemInstantiationCode = this.generateSystemInstantiationCode(_argument);
    _builder.append(_generateSystemInstantiationCode, "");
    _builder.append(" )");
    return _builder;
  }
  
  public CharSequence getHideArray(final Iterable<String> hide) {
    CharSequence _xblockexpression = null;
    {
      HashSet<String> _actions = this.symbolTable.getActions();
      final Function1<String, Boolean> _function = new Function1<String, Boolean>() {
        @Override
        public Boolean apply(final String a) {
          final Function1<String, Boolean> _function = new Function1<String, Boolean>() {
            @Override
            public Boolean apply(final String b) {
              return Boolean.valueOf(Objects.equal(a, b));
            }
          };
          return Boolean.valueOf(IterableExtensions.<String>exists(hide, _function));
        }
      };
      Iterable<Boolean> _map = IterableExtensions.<String, Boolean>map(_actions, _function);
      final Function1<Boolean, String> _function_1 = new Function1<Boolean, String>() {
        @Override
        public String apply(final Boolean b) {
          String _xifexpression = null;
          if ((b).booleanValue()) {
            _xifexpression = "true";
          } else {
            _xifexpression = "false";
          }
          return _xifexpression;
        }
      };
      Iterable<String> syncSet = IterableExtensions.<Boolean, String>map(_map, _function_1);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("new boolean[] { false , ");
      String _join = IterableExtensions.join(syncSet, ",");
      _builder.append(_join, "");
      _builder.append(" }");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateSystemInstantiationCode(final Renaming p) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new SysteSystemRenaming( ");
    EList<ActionRenaming> _renaming = p.getRenaming();
    CharSequence _mapArray = this.getMapArray(_renaming);
    _builder.append(_mapArray, "");
    _builder.append(" , ");
    PrismSystem _argument = p.getArgument();
    Object _generateSystemInstantiationCode = this.generateSystemInstantiationCode(_argument);
    _builder.append(_generateSystemInstantiationCode, "");
    _builder.append(" )");
    return _builder;
  }
  
  protected CharSequence _generateSystemInstantiationCode(final ModuleReference r) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("MODULE_");
    Module _module = r.getModule();
    String _name = _module.getName();
    _builder.append(_name, "");
    return _builder;
  }
  
  public CharSequence getMapArray(final Iterable<ActionRenaming> f) {
    CharSequence _xblockexpression = null;
    {
      HashSet<String> _actions = this.symbolTable.getActions();
      final Function1<String, String> _function = new Function1<String, String>() {
        @Override
        public String apply(final String a) {
          return ClassGenerator.this.apply(f, a);
        }
      };
      Iterable<String> _map = IterableExtensions.<String, String>map(_actions, _function);
      final Function1<String, String> _function_1 = new Function1<String, String>() {
        @Override
        public String apply(final String a) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("_LABEL_");
          _builder.append(a, "");
          return _builder.toString();
        }
      };
      Iterable<String> syncSet = IterableExtensions.<String, String>map(_map, _function_1);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("new int[] { 0 , ");
      String _join = IterableExtensions.join(syncSet, ",");
      _builder.append(_join, "");
      _builder.append(" }");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public String apply(final Iterable<ActionRenaming> f, final String a) {
    String _xblockexpression = null;
    {
      final Function1<ActionRenaming, Boolean> _function = new Function1<ActionRenaming, Boolean>() {
        @Override
        public Boolean apply(final ActionRenaming m) {
          String _source = m.getSource();
          return Boolean.valueOf(Objects.equal(_source, Integer.valueOf(0)));
        }
      };
      ActionRenaming m = IterableExtensions.<ActionRenaming>findFirst(f, _function);
      String _xifexpression = null;
      boolean _equals = Objects.equal(m, null);
      if (_equals) {
        _xifexpression = a;
      } else {
        _xifexpression = m.getTarget();
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public CharSequence generateCommand(final UpdateStructure structure) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("new Command( enumerator ) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("protected BigInteger doApply(BigInteger currentState , BigInteger nextState) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("//PrismState nextState = currentState;");
    _builder.newLine();
    {
      Iterable<RenamedElement<UpdateElement>> _updates = structure.getUpdates();
      for(final RenamedElement<UpdateElement> rc : _updates) {
        _builder.append("\t\t");
        UpdateElement _element = rc.getElement();
        Map<String, String> _renaming = rc.getRenaming();
        CharSequence _generateUpdateCode = this.generateUpdateCode(_element, _renaming);
        _builder.append(_generateUpdateCode, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("return nextState;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("protected double computeWeight(BigInteger currentState) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("double toReturn = 1.0;");
    _builder.newLine();
    {
      Iterable<RenamedElement<Expression>> _weight = structure.getWeight();
      final Function1<RenamedElement<Expression>, Boolean> _function = new Function1<RenamedElement<Expression>, Boolean>() {
        @Override
        public Boolean apply(final RenamedElement<Expression> re) {
          Expression _element = re.getElement();
          return Boolean.valueOf((!Objects.equal(_element, null)));
        }
      };
      Iterable<RenamedElement<Expression>> _filter = IterableExtensions.<RenamedElement<Expression>>filter(_weight, _function);
      for(final RenamedElement<Expression> re : _filter) {
        _builder.append("\t\t");
        _builder.append("toReturn = toReturn * (");
        Expression _element_1 = re.getElement();
        Map<String, String> _renaming_1 = re.getRenaming();
        CharSequence _generateExpressionCode = this.generateExpressionCode(_element_1, _renaming_1, false);
        _builder.append(_generateExpressionCode, "\t\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("return toReturn;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}\t\t");
    _builder.newLine();
    _builder.append("};");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence generateGuardFilter(final Iterable<RenamedElement<Expression>> guard) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("new Filter<BigInteger>() {");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public boolean eval(BigInteger currentState) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return true ");
    {
      for(final RenamedElement<Expression> er : guard) {
        _builder.append("&& ( ");
        Expression _element = er.getElement();
        Map<String, String> _renaming = er.getRenaming();
        CharSequence _generateExpressionCode = this.generateExpressionCode(_element, _renaming, true);
        _builder.append(_generateExpressionCode, "\t\t");
        _builder.append(")");
      }
    }
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _generateExpressionCode(final Expression e, final Map<String, String> renaming, final boolean isBoolean) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("UNSUPPORTED");
    return _builder;
  }
  
  protected CharSequence _generateExpressionCode(final IfThenElse e, final Map<String, String> renaming, final boolean isBoolean) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("(");
    Expression _guard = e.getGuard();
    Object _generateExpressionCode = this.generateExpressionCode(_guard, renaming, true);
    _builder.append(_generateExpressionCode, "");
    _builder.append("?");
    Expression _thenCase = e.getThenCase();
    Object _generateExpressionCode_1 = this.generateExpressionCode(_thenCase, renaming, isBoolean);
    _builder.append(_generateExpressionCode_1, "");
    _builder.append(":");
    Expression _elseCase = e.getElseCase();
    Object _generateExpressionCode_2 = this.generateExpressionCode(_elseCase, renaming, isBoolean);
    _builder.append(_generateExpressionCode_2, "");
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generateExpressionCode(final Implies e, final Map<String, String> renaming, final boolean isBoolean) {
    CharSequence _xblockexpression = null;
    {
      Expression _left = e.getLeft();
      Object str1 = this.generateExpressionCode(_left, renaming, true);
      Expression _right = e.getRight();
      Object str2 = this.generateExpressionCode(_right, renaming, true);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("!(");
      _builder.append(str1, "");
      _builder.append(")||(");
      _builder.append(str2, "");
      _builder.append(")");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateExpressionCode(final IfAndOnlyIf e, final Map<String, String> renaming, final boolean isBoolean) {
    CharSequence _xblockexpression = null;
    {
      Expression _left = e.getLeft();
      Object str1 = this.generateExpressionCode(_left, renaming, true);
      Expression _right = e.getRight();
      Object str2 = this.generateExpressionCode(_right, renaming, true);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("(!(");
      _builder.append(str1, "");
      _builder.append(")||(");
      _builder.append(str2, "");
      _builder.append("))&&(!(");
      _builder.append(str2, "");
      _builder.append(")||(");
      _builder.append(str1, "");
      _builder.append("))");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateExpressionCode(final OrExpression e, final Map<String, String> renaming, final boolean isBoolean) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("(");
    Expression _left = e.getLeft();
    Object _generateExpressionCode = this.generateExpressionCode(_left, renaming, true);
    _builder.append(_generateExpressionCode, "");
    _builder.append(")||(");
    Expression _right = e.getRight();
    Object _generateExpressionCode_1 = this.generateExpressionCode(_right, renaming, true);
    _builder.append(_generateExpressionCode_1, "");
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generateExpressionCode(final AndExpression e, final Map<String, String> renaming, final boolean isBoolean) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("(");
    Expression _left = e.getLeft();
    Object _generateExpressionCode = this.generateExpressionCode(_left, renaming, true);
    _builder.append(_generateExpressionCode, "");
    _builder.append(")&&(");
    Expression _right = e.getRight();
    Object _generateExpressionCode_1 = this.generateExpressionCode(_right, renaming, true);
    _builder.append(_generateExpressionCode_1, "");
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generateExpressionCode(final RelExpression e, final Map<String, String> renaming, final boolean isBoolean) {
    CharSequence _xifexpression = null;
    if (isBoolean) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("(");
      Expression _left = e.getLeft();
      Object _generateExpressionCode = this.generateExpressionCode(_left, renaming, false);
      _builder.append(_generateExpressionCode, "");
      _builder.append(")");
      Relations _relop = e.getRelop();
      String _relationSymbol = this.getRelationSymbol(_relop);
      _builder.append(_relationSymbol, "");
      _builder.append("(");
      Expression _right = e.getRight();
      Object _generateExpressionCode_1 = this.generateExpressionCode(_right, renaming, false);
      _builder.append(_generateExpressionCode_1, "");
      _builder.append(")");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("((");
      Expression _left_1 = e.getLeft();
      Object _generateExpressionCode_2 = this.generateExpressionCode(_left_1, renaming, true);
      _builder_1.append(_generateExpressionCode_2, "");
      _builder_1.append(")");
      Relations _relop_1 = e.getRelop();
      String _relationSymbol_1 = this.getRelationSymbol(_relop_1);
      _builder_1.append(_relationSymbol_1, "");
      _builder_1.append("(");
      Expression _right_1 = e.getRight();
      Object _generateExpressionCode_3 = this.generateExpressionCode(_right_1, renaming, true);
      _builder_1.append(_generateExpressionCode_3, "");
      _builder_1.append(")?1:0)");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public String getRelationSymbol(final Relations r) {
    String _xifexpression = null;
    boolean _equals = Objects.equal(r, Relations.EQ);
    if (_equals) {
      _xifexpression = "==";
    } else {
      _xifexpression = r.toString();
    }
    return _xifexpression;
  }
  
  protected CharSequence _generateExpressionCode(final SumExpression e, final Map<String, String> renaming, final boolean isBoolean) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("(");
    Expression _left = e.getLeft();
    Object _generateExpressionCode = this.generateExpressionCode(_left, renaming, false);
    _builder.append(_generateExpressionCode, "");
    _builder.append(")");
    String _op = e.getOp();
    _builder.append(_op, "");
    _builder.append("(");
    Expression _right = e.getRight();
    Object _generateExpressionCode_1 = this.generateExpressionCode(_right, renaming, false);
    _builder.append(_generateExpressionCode_1, "");
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generateExpressionCode(final MulExpression e, final Map<String, String> renaming, final boolean isBoolean) {
    CharSequence _xifexpression = null;
    String _op = e.getOp();
    boolean _equals = _op.equals("/");
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("((double) ");
      Expression _left = e.getLeft();
      Object _generateExpressionCode = this.generateExpressionCode(_left, renaming, false);
      _builder.append(_generateExpressionCode, "");
      _builder.append(")");
      String _op_1 = e.getOp();
      _builder.append(_op_1, "");
      _builder.append("((double) ");
      Expression _right = e.getRight();
      Object _generateExpressionCode_1 = this.generateExpressionCode(_right, renaming, false);
      _builder.append(_generateExpressionCode_1, "");
      _builder.append(")");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("(");
      Expression _left_1 = e.getLeft();
      Object _generateExpressionCode_2 = this.generateExpressionCode(_left_1, renaming, false);
      _builder_1.append(_generateExpressionCode_2, "");
      _builder_1.append(")");
      String _op_2 = e.getOp();
      _builder_1.append(_op_2, "");
      _builder_1.append("(");
      Expression _right_1 = e.getRight();
      Object _generateExpressionCode_3 = this.generateExpressionCode(_right_1, renaming, false);
      _builder_1.append(_generateExpressionCode_3, "");
      _builder_1.append(")");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generateExpressionCode(final DecimalLiteral e, final Map<String, String> renaming, final boolean isBoolean) {
    StringConcatenation _builder = new StringConcatenation();
    IntegerLiteral _integerPart = e.getIntegerPart();
    int _integerPart_1 = _integerPart.getIntegerPart();
    _builder.append(_integerPart_1, "");
    String _decimalPart = e.getDecimalPart();
    _builder.append(_decimalPart, "");
    return _builder;
  }
  
  protected CharSequence _generateExpressionCode(final IntegerLiteral e, final Map<String, String> renaming, final boolean isBoolean) {
    StringConcatenation _builder = new StringConcatenation();
    int _integerPart = e.getIntegerPart();
    _builder.append(_integerPart, "");
    return _builder;
  }
  
  protected CharSequence _generateExpressionCode(final Reference e, final Map<String, String> renaming, final boolean isBoolean) {
    CharSequence _xblockexpression = null;
    {
      String name = e.getReference();
      boolean _and = false;
      boolean _notEquals = (!Objects.equal(renaming, null));
      if (!_notEquals) {
        _and = false;
      } else {
        String _get = renaming.get(name);
        boolean _notEquals_1 = (!Objects.equal(_get, null));
        _and = _notEquals_1;
      }
      if (_and) {
        String _get_1 = renaming.get(name);
        name = _get_1;
      }
      _xblockexpression = this.symbolTable.getExpressionCode(name, isBoolean);
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateExpressionCode(final True e, final Map<String, String> renaming, final boolean isBoolean) {
    CharSequence _xifexpression = null;
    if (isBoolean) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("true");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("1");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generateExpressionCode(final False e, final Map<String, String> renaming, final boolean isBoolean) {
    CharSequence _xifexpression = null;
    if (isBoolean) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("false");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("0");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generateExpressionCode(final Negation e, final Map<String, String> renaming, final boolean isBoolean) {
    CharSequence _xifexpression = null;
    if (isBoolean) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("!(");
      Expression _arg = e.getArg();
      Object _generateExpressionCode = this.generateExpressionCode(_arg, renaming, true);
      _builder.append(_generateExpressionCode, "");
      _builder.append(")");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("(");
      Expression _arg_1 = e.getArg();
      Object _generateExpressionCode_1 = this.generateExpressionCode(_arg_1, renaming, true);
      _builder_1.append(_generateExpressionCode_1, "");
      _builder_1.append("?0:1)");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  /**
   * TO HANDLE!
   * | MinFunction
   * | MaxFunction
   * | PowFunction
   * | FloorFunction
   * | CeilFunction
   * | ModFunction
   * | LogFunction
   */
  public CharSequence generateUpdateCode(final UpdateElement u, final Map<String, String> map) {
    CharSequence _xblockexpression = null;
    {
      String _variable = u.getVariable();
      String target = _variable.replaceAll("\'", "");
      boolean _and = false;
      boolean _notEquals = (!Objects.equal(map, null));
      if (!_notEquals) {
        _and = false;
      } else {
        String _get = map.get(target);
        boolean _notEquals_1 = (!Objects.equal(_get, null));
        _and = _notEquals_1;
      }
      if (_and) {
        String _get_1 = map.get(target);
        target = _get_1;
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("nextState = enumerator.set( nextState , ");
      CharSequence _variableCode = this.symbolTable.getVariableCode(target);
      _builder.append(_variableCode, "");
      _builder.append(", ");
      Expression _expression = u.getExpression();
      CharSequence _generateExpressionCode = this.generateExpressionCode(_expression, map, false);
      _builder.append(_generateExpressionCode, "");
      _builder.append(");");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public Iterable<Pair<Integer, String>> generateLocalVariableIndexes(final Iterable<String> vList) {
    Iterable<Pair<Integer, String>> _xblockexpression = null;
    {
      Iterable<Pair<Integer, String>> res = this.<String>generateIndexes(vList, this.variableIndex);
      int _size = IterableExtensions.size(vList);
      int _plus = (this.variableIndex + _size);
      this.variableIndex = _plus;
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public <T extends Object> Iterable<Pair<Integer, T>> generateIndexes(final Iterable<T> list) {
    return this.<T>generateIndexes(list, 0);
  }
  
  public <T extends Object> Iterable<Pair<Integer, T>> generateIndexes(final Iterable<T> list, final int start) {
    Iterable<Pair<Integer, T>> _xblockexpression = null;
    {
      final Wrapper<Integer> i = Wrapper.<Integer>wrap(Integer.valueOf(start));
      final Function1<T, Pair<Integer, T>> _function = new Function1<T, Pair<Integer, T>>() {
        @Override
        public Pair<Integer, T> apply(final T r) {
          Pair<Integer, T> _xblockexpression = null;
          {
            Integer c = i.get();
            i.set(Integer.valueOf(((c).intValue() + 1)));
            _xblockexpression = Pair.<Integer, T>of(c, r);
          }
          return _xblockexpression;
        }
      };
      _xblockexpression = IterableExtensions.<T, Pair<Integer, T>>map(list, _function);
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateDeclarationCode(final Pair<Integer, Variable> p) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public static int _GLOBAL_");
    Variable _value = p.getValue();
    String _name = _value.getName();
    _builder.append(_name, "");
    _builder.append(" = ");
    Integer _key = p.getKey();
    _builder.append(_key, "");
    _builder.append(";");
    return _builder;
  }
  
  protected CharSequence _generateDeclarationCode(final Constant c) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public static ");
    ConstantType _type = c.getType();
    CharSequence _declarationType = this.getDeclarationType(_type);
    _builder.append(_declarationType, "");
    _builder.append(" _CONST_");
    String _name = c.getName();
    _builder.append(_name, "");
    _builder.append(" = ");
    Expression _exp = c.getExp();
    ConstantType _type_1 = c.getType();
    boolean _equals = Objects.equal(_type_1, ConstantType.CBOOL);
    CharSequence _generateExpressionCode = this.generateExpressionCode(_exp, null, _equals);
    _builder.append(_generateExpressionCode, "");
    _builder.append(";");
    return _builder;
  }
  
  public CharSequence getDeclarationType(final ConstantType t) {
    CharSequence _switchResult = null;
    if (t != null) {
      switch (t) {
        case CINT:
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("int");
          _switchResult = _builder;
          break;
        case CBOOL:
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("boolean");
          _switchResult = _builder_1;
          break;
        case CDOUBLE:
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("double");
          _switchResult = _builder_2;
          break;
        default:
          break;
      }
    }
    return _switchResult;
  }
  
  protected CharSequence _generateDeclarationCode(final Formula f) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public static int ");
    String _name = f.getName();
    CharSequence _formulaCode = this.symbolTable.getFormulaCode(_name);
    _builder.append(_formulaCode, "");
    _builder.append("( BigInteger currentState ) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    Expression _expression = f.getExpression();
    HashMap<String, String> _hashMap = new HashMap<String, String>();
    CharSequence _generateExpressionCode = this.generateExpressionCode(_expression, _hashMap, false);
    _builder.append(_generateExpressionCode, "\t");
    _builder.append(";\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence generateFormulaInizialization(final Element e) {
    if (e instanceof PathFormulaDeclaration) {
      return _generateFormulaInizialization((PathFormulaDeclaration)e);
    } else if (e instanceof StateFormulaDeclaration) {
      return _generateFormulaInizialization((StateFormulaDeclaration)e);
    } else if (e != null) {
      return _generateFormulaInizialization(e);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(e).toString());
    }
  }
  
  public CharSequence generatePathFormulaCode(final PathFormula f) {
    if (f instanceof NextFormula) {
      return _generatePathFormulaCode((NextFormula)f);
    } else if (f instanceof UntilFormula) {
      return _generatePathFormulaCode((UntilFormula)f);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(f).toString());
    }
  }
  
  public CharSequence generateStateFormulaCode(final StateFormula f) {
    if (f instanceof AtomicStateFormula) {
      return _generateStateFormulaCode((AtomicStateFormula)f);
    } else if (f instanceof NegationFormula) {
      return _generateStateFormulaCode((NegationFormula)f);
    } else if (f instanceof ProbabilityFormula) {
      return _generateStateFormulaCode((ProbabilityFormula)f);
    } else if (f instanceof StateAnd) {
      return _generateStateFormulaCode((StateAnd)f);
    } else if (f instanceof StateOr) {
      return _generateStateFormulaCode((StateOr)f);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(f).toString());
    }
  }
  
  public CharSequence generateSystemInstantiationCode(final PrismSystem p) {
    if (p instanceof AlphabetisedParallelComposition) {
      return _generateSystemInstantiationCode((AlphabetisedParallelComposition)p);
    } else if (p instanceof AsynchronousParallelComposition) {
      return _generateSystemInstantiationCode((AsynchronousParallelComposition)p);
    } else if (p instanceof Hiding) {
      return _generateSystemInstantiationCode((Hiding)p);
    } else if (p instanceof ModuleReference) {
      return _generateSystemInstantiationCode((ModuleReference)p);
    } else if (p instanceof Renaming) {
      return _generateSystemInstantiationCode((Renaming)p);
    } else if (p instanceof RestrictedParallelComposition) {
      return _generateSystemInstantiationCode((RestrictedParallelComposition)p);
    } else if (p != null) {
      return _generateSystemInstantiationCode(p);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(p).toString());
    }
  }
  
  public CharSequence generateExpressionCode(final Expression e, final Map<String, String> renaming, final boolean isBoolean) {
    if (e instanceof DecimalLiteral) {
      return _generateExpressionCode((DecimalLiteral)e, renaming, isBoolean);
    } else if (e instanceof IntegerLiteral) {
      return _generateExpressionCode((IntegerLiteral)e, renaming, isBoolean);
    } else if (e instanceof AndExpression) {
      return _generateExpressionCode((AndExpression)e, renaming, isBoolean);
    } else if (e instanceof False) {
      return _generateExpressionCode((False)e, renaming, isBoolean);
    } else if (e instanceof IfAndOnlyIf) {
      return _generateExpressionCode((IfAndOnlyIf)e, renaming, isBoolean);
    } else if (e instanceof IfThenElse) {
      return _generateExpressionCode((IfThenElse)e, renaming, isBoolean);
    } else if (e instanceof Implies) {
      return _generateExpressionCode((Implies)e, renaming, isBoolean);
    } else if (e instanceof MulExpression) {
      return _generateExpressionCode((MulExpression)e, renaming, isBoolean);
    } else if (e instanceof Negation) {
      return _generateExpressionCode((Negation)e, renaming, isBoolean);
    } else if (e instanceof OrExpression) {
      return _generateExpressionCode((OrExpression)e, renaming, isBoolean);
    } else if (e instanceof Reference) {
      return _generateExpressionCode((Reference)e, renaming, isBoolean);
    } else if (e instanceof RelExpression) {
      return _generateExpressionCode((RelExpression)e, renaming, isBoolean);
    } else if (e instanceof SumExpression) {
      return _generateExpressionCode((SumExpression)e, renaming, isBoolean);
    } else if (e instanceof True) {
      return _generateExpressionCode((True)e, renaming, isBoolean);
    } else if (e != null) {
      return _generateExpressionCode(e, renaming, isBoolean);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(e, renaming, isBoolean).toString());
    }
  }
  
  public CharSequence generateDeclarationCode(final Object c) {
    if (c instanceof Constant) {
      return _generateDeclarationCode((Constant)c);
    } else if (c instanceof Formula) {
      return _generateDeclarationCode((Formula)c);
    } else if (c instanceof Pair) {
      return _generateDeclarationCode((Pair<Integer, Variable>)c);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(c).toString());
    }
  }
}
