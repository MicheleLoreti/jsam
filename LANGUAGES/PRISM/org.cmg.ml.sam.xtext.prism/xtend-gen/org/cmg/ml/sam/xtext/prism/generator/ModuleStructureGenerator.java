package org.cmg.ml.sam.xtext.prism.generator;

import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.cmg.ml.sam.xtext.prism.prism.Command;
import org.cmg.ml.sam.xtext.prism.prism.Expression;
import org.cmg.ml.sam.xtext.prism.prism.Module;
import org.cmg.ml.sam.xtext.prism.prism.ModuleBody;
import org.cmg.ml.sam.xtext.prism.prism.ModuleBodyDeclaration;
import org.cmg.ml.sam.xtext.prism.prism.SymbolRenaming;
import org.cmg.ml.sam.xtext.prism.prism.Update;
import org.cmg.ml.sam.xtext.prism.prism.UpdateElement;
import org.cmg.ml.sam.xtext.prism.prism.Variable;
import org.cmg.ml.sam.xtext.prism.prism.VariableRenaming;
import org.cmg.ml.sam.xtext.prism.util.ModuleStructure;
import org.cmg.ml.sam.xtext.prism.util.RenamedElement;
import org.cmg.ml.sam.xtext.prism.util.RuleStructure;
import org.cmg.ml.sam.xtext.prism.util.UpdateStructure;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ModuleStructureGenerator {
  private Map<String, ModuleStructure> moduleCache = new HashMap<String, ModuleStructure>();
  
  protected ModuleStructure _generateStructure(final Module m) {
    ModuleStructure _xblockexpression = null;
    {
      String _name = m.getName();
      ModuleStructure structure = this.moduleCache.get(_name);
      boolean _equals = Objects.equal(structure, null);
      if (_equals) {
        ModuleBody _body = m.getBody();
        String _name_1 = m.getName();
        ModuleStructure _generateModuleStructure = this.generateModuleStructure(_body, _name_1);
        structure = _generateModuleStructure;
        String _name_2 = m.getName();
        this.moduleCache.put(_name_2, structure);
      }
      _xblockexpression = structure;
    }
    return _xblockexpression;
  }
  
  protected ModuleStructure _generateModuleStructure(final ModuleBodyDeclaration b, final String name) {
    EList<Variable> _variables = b.getVariables();
    final Function1<Variable, String> _function = new Function1<Variable, String>() {
      @Override
      public String apply(final Variable v) {
        return v.getName();
      }
    };
    List<String> _map = ListExtensions.<Variable, String>map(_variables, _function);
    EList<Command> _commands = b.getCommands();
    final Function1<Command, RuleStructure> _function_1 = new Function1<Command, RuleStructure>() {
      @Override
      public RuleStructure apply(final Command c) {
        String _act = c.getAct();
        Expression _guard = c.getGuard();
        RenamedElement<Expression> _renamedElement = new RenamedElement<Expression>(_guard);
        ArrayList<RenamedElement<Expression>> _newArrayList = CollectionLiterals.<RenamedElement<Expression>>newArrayList(_renamedElement);
        EList<Update> _updates = c.getUpdates();
        final Function1<Update, UpdateStructure> _function = new Function1<Update, UpdateStructure>() {
          @Override
          public UpdateStructure apply(final Update u) {
            Expression _weight = u.getWeight();
            EList<UpdateElement> _elements = u.getElements();
            return new UpdateStructure(_weight, _elements);
          }
        };
        List<UpdateStructure> _map = ListExtensions.<Update, UpdateStructure>map(_updates, _function);
        return new RuleStructure(_act, _newArrayList, _map);
      }
    };
    List<RuleStructure> _map_1 = ListExtensions.<Command, RuleStructure>map(_commands, _function_1);
    return new ModuleStructure(name, _map, _map_1);
  }
  
  protected ModuleStructure _generateModuleStructure(final VariableRenaming b, final String name) {
    ModuleStructure _xblockexpression = null;
    {
      final HashMap<String, String> map = new HashMap<String, String>();
      EList<SymbolRenaming> _renaming = b.getRenaming();
      final Consumer<SymbolRenaming> _function = new Consumer<SymbolRenaming>() {
        @Override
        public void accept(final SymbolRenaming s) {
          String _source = s.getSource();
          String _target = s.getTarget();
          map.put(_source, _target);
        }
      };
      _renaming.forEach(_function);
      Module _module = b.getModule();
      ModuleStructure _generateStructure = this.generateStructure(_module);
      _xblockexpression = _generateStructure.rename(name, map);
    }
    return _xblockexpression;
  }
  
  public ModuleStructure generateStructure(final Module m) {
    return _generateStructure(m);
  }
  
  public ModuleStructure generateModuleStructure(final ModuleBody b, final String name) {
    if (b instanceof ModuleBodyDeclaration) {
      return _generateModuleStructure((ModuleBodyDeclaration)b, name);
    } else if (b instanceof VariableRenaming) {
      return _generateModuleStructure((VariableRenaming)b, name);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(b, name).toString());
    }
  }
}
