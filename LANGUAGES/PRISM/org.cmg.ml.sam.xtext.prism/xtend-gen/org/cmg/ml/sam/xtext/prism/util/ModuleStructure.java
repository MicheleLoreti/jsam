package org.cmg.ml.sam.xtext.prism.util;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.Consumer;
import org.cmg.ml.sam.xtext.prism.util.RuleStructure;
import org.eclipse.xtext.util.Wrapper;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure2;

@SuppressWarnings("all")
public class ModuleStructure {
  private String name;
  
  private Iterable<String> variables;
  
  private Iterable<RuleStructure> rules;
  
  public ModuleStructure(final String name, final Iterable<String> variables, final Iterable<RuleStructure> rules) {
    this.name = name;
    this.rules = rules;
    this.variables = variables;
  }
  
  public ModuleStructure(final Iterable<String> variables, final Iterable<RuleStructure> rules) {
    this.name = "_UNNAMED_";
    this.rules = rules;
    this.variables = variables;
  }
  
  public ModuleStructure rename(final String name, final Map<String, String> map) {
    final Function1<String, String> _function = new Function1<String, String>() {
      @Override
      public String apply(final String s) {
        String _xifexpression = null;
        String _get = map.get(s);
        boolean _notEquals = (!Objects.equal(_get, null));
        if (_notEquals) {
          _xifexpression = map.get(s);
        } else {
          _xifexpression = s;
        }
        return _xifexpression;
      }
    };
    Iterable<String> _map = IterableExtensions.<String, String>map(this.variables, _function);
    final Function1<RuleStructure, RuleStructure> _function_1 = new Function1<RuleStructure, RuleStructure>() {
      @Override
      public RuleStructure apply(final RuleStructure r) {
        return r.rename(map);
      }
    };
    Iterable<RuleStructure> _map_1 = IterableExtensions.<RuleStructure, RuleStructure>map(this.rules, _function_1);
    return new ModuleStructure(name, _map, _map_1);
  }
  
  public ModuleStructure alphabetisedParallel(final ModuleStructure m) {
    ModuleStructure _xblockexpression = null;
    {
      final LinkedList<RuleStructure> list = new LinkedList<RuleStructure>();
      final Consumer<RuleStructure> _function = new Consumer<RuleStructure>() {
        @Override
        public void accept(final RuleStructure r1) {
          String _action = r1.getAction();
          boolean _notEquals = (!Objects.equal(_action, null));
          if (_notEquals) {
            final Consumer<RuleStructure> _function = new Consumer<RuleStructure>() {
              @Override
              public void accept(final RuleStructure r2) {
                RuleStructure r = r1.combine(r2);
                boolean _notEquals = (!Objects.equal(r, null));
                if (_notEquals) {
                  list.add(r);
                }
              }
            };
            m.rules.forEach(_function);
          } else {
            list.add(r1);
          }
        }
      };
      this.rules.forEach(_function);
      final Consumer<RuleStructure> _function_1 = new Consumer<RuleStructure>() {
        @Override
        public void accept(final RuleStructure r) {
          String _action = r.getAction();
          boolean _equals = Objects.equal(_action, null);
          if (_equals) {
            list.add(r);
          }
        }
      };
      m.rules.forEach(_function_1);
      Iterable<String> _plus = Iterables.<String>concat(this.variables, m.variables);
      _xblockexpression = new ModuleStructure(_plus, list);
    }
    return _xblockexpression;
  }
  
  public ModuleStructure asynchronousParallel(final ModuleStructure m) {
    Iterable<String> _plus = Iterables.<String>concat(this.variables, m.variables);
    Iterable<RuleStructure> _plus_1 = Iterables.<RuleStructure>concat(this.rules, m.rules);
    return new ModuleStructure(_plus, _plus_1);
  }
  
  public ModuleStructure restrictedParallel(final ModuleStructure m, final Iterable<String> actions) {
    ModuleStructure _xblockexpression = null;
    {
      final LinkedList<RuleStructure> list = new LinkedList<RuleStructure>();
      final Consumer<RuleStructure> _function = new Consumer<RuleStructure>() {
        @Override
        public void accept(final RuleStructure r1) {
          boolean _and = false;
          String _action = r1.getAction();
          boolean _notEquals = (!Objects.equal(_action, null));
          if (!_notEquals) {
            _and = false;
          } else {
            final Function1<String, Boolean> _function = new Function1<String, Boolean>() {
              @Override
              public Boolean apply(final String x) {
                String _action = r1.getAction();
                return Boolean.valueOf(_action.equals(x));
              }
            };
            boolean _exists = IterableExtensions.<String>exists(actions, _function);
            _and = _exists;
          }
          if (_and) {
            final Consumer<RuleStructure> _function_1 = new Consumer<RuleStructure>() {
              @Override
              public void accept(final RuleStructure r2) {
                RuleStructure r = r1.combine(r2);
                boolean _notEquals = (!Objects.equal(r, null));
                if (_notEquals) {
                  list.add(r);
                }
              }
            };
            m.rules.forEach(_function_1);
          } else {
            list.add(r1);
          }
        }
      };
      this.rules.forEach(_function);
      final Consumer<RuleStructure> _function_1 = new Consumer<RuleStructure>() {
        @Override
        public void accept(final RuleStructure r) {
          boolean _and = false;
          String _action = r.getAction();
          boolean _notEquals = (!Objects.equal(_action, null));
          if (!_notEquals) {
            _and = false;
          } else {
            final Function1<String, Boolean> _function = new Function1<String, Boolean>() {
              @Override
              public Boolean apply(final String x) {
                String _action = r.getAction();
                return Boolean.valueOf(_action.equals(x));
              }
            };
            boolean _exists = IterableExtensions.<String>exists(actions, _function);
            _and = _exists;
          }
          if (_and) {
            list.add(r);
          }
        }
      };
      m.rules.forEach(_function_1);
      Iterable<String> _plus = Iterables.<String>concat(this.variables, m.variables);
      _xblockexpression = new ModuleStructure(_plus, list);
    }
    return _xblockexpression;
  }
  
  public ModuleStructure hide(final Iterable<String> actions) {
    final Function1<RuleStructure, RuleStructure> _function = new Function1<RuleStructure, RuleStructure>() {
      @Override
      public RuleStructure apply(final RuleStructure r) {
        return r.hide(actions);
      }
    };
    Iterable<RuleStructure> _map = IterableExtensions.<RuleStructure, RuleStructure>map(this.rules, _function);
    return new ModuleStructure(this.variables, _map);
  }
  
  public Iterable<RuleStructure> getRules() {
    return this.rules;
  }
  
  public void generateIndexes() {
    final Procedure2<RuleStructure, Integer> _function = new Procedure2<RuleStructure, Integer>() {
      @Override
      public void apply(final RuleStructure r, final Integer i) {
        r.setIndex(i);
        r.generateIndexes();
      }
    };
    IterableExtensions.<RuleStructure>forEach(this.rules, _function);
  }
  
  @Override
  public String toString() {
    final Function1<RuleStructure, String> _function = new Function1<RuleStructure, String>() {
      @Override
      public String apply(final RuleStructure r) {
        return r.toString();
      }
    };
    Iterable<String> _map = IterableExtensions.<RuleStructure, String>map(this.rules, _function);
    final Function2<String, String, String> _function_1 = new Function2<String, String, String>() {
      @Override
      public String apply(final String x, final String y) {
        return ((x + "\n") + y);
      }
    };
    return IterableExtensions.<String>reduce(_map, _function_1);
  }
  
  public Iterable<String> getLocalVariables() {
    return this.variables;
  }
  
  public Iterable<Pair<Integer, RuleStructure>> getIndexedRules() {
    Iterable<Pair<Integer, RuleStructure>> _xblockexpression = null;
    {
      final Wrapper<Integer> i = Wrapper.<Integer>wrap(Integer.valueOf(0));
      final Function1<RuleStructure, Pair<Integer, RuleStructure>> _function = new Function1<RuleStructure, Pair<Integer, RuleStructure>>() {
        @Override
        public Pair<Integer, RuleStructure> apply(final RuleStructure r) {
          Pair<Integer, RuleStructure> _xblockexpression = null;
          {
            Integer c = i.get();
            i.set(Integer.valueOf(((c).intValue() + 1)));
            _xblockexpression = Pair.<Integer, RuleStructure>of(c, r);
          }
          return _xblockexpression;
        }
      };
      _xblockexpression = IterableExtensions.<RuleStructure, Pair<Integer, RuleStructure>>map(this.rules, _function);
    }
    return _xblockexpression;
  }
  
  public String getName() {
    return this.name;
  }
}
