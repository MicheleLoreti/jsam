/**
 * Michele Loreti, Concurrency and Mobility Group
 * Universit� di Firenze, Italy
 * (C) Copyright 2013.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Michele Loreti
 */
package org.cmg.ml.sam.xtext.prism.util;

import com.google.common.base.Objects;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

@SuppressWarnings("all")
public class RenamedElement<T extends Object> {
  private T element;
  
  private Map<String, String> renaming;
  
  public RenamedElement(final T element) {
    this(element, null);
  }
  
  public RenamedElement(final T element, final Map<String, String> renaming) {
    this.element = element;
    this.renaming = renaming;
  }
  
  public T getElement() {
    return this.element;
  }
  
  public Map<String, String> getRenaming() {
    return this.renaming;
  }
  
  public RenamedElement<T> apply(final Map<String, String> f) {
    RenamedElement<T> _xifexpression = null;
    boolean _notEquals = (!Objects.equal(this.renaming, null));
    if (_notEquals) {
      RenamedElement<T> _xblockexpression = null;
      {
        final HashMap<String, String> newF = new HashMap<String, String>();
        final BiConsumer<String, String> _function = new BiConsumer<String, String>() {
          @Override
          public void accept(final String src, final String trg) {
            String foo = f.get(trg);
            boolean _notEquals = (!Objects.equal(foo, null));
            if (_notEquals) {
              newF.put(src, foo);
            }
          }
        };
        this.renaming.forEach(_function);
        final BiConsumer<String, String> _function_1 = new BiConsumer<String, String>() {
          @Override
          public void accept(final String src, final String trg) {
            String foo = RenamedElement.this.renaming.get(src);
            boolean _equals = Objects.equal(foo, null);
            if (_equals) {
              newF.put(src, trg);
            }
          }
        };
        f.forEach(_function_1);
        _xblockexpression = new RenamedElement<T>(this.element, newF);
      }
      _xifexpression = _xblockexpression;
    } else {
      _xifexpression = new RenamedElement<T>(this.element, f);
    }
    return _xifexpression;
  }
  
  @Override
  public String toString() {
    String _xblockexpression = null;
    {
      String mapping = "";
      boolean _notEquals = (!Objects.equal(this.renaming, null));
      if (_notEquals) {
        mapping = "{";
        Set<String> _keySet = this.renaming.keySet();
        for (final String k : _keySet) {
          String _get = this.renaming.get(k);
          String _plus = ((k + "->") + _get);
          String _plus_1 = (mapping + _plus);
          mapping = _plus_1;
        }
        mapping = (mapping + "}");
      }
      String _string = this.element.toString();
      String _plus_2 = (_string + "{");
      String _plus_3 = (_plus_2 + mapping);
      _xblockexpression = (_plus_3 + "}");
    }
    return _xblockexpression;
  }
}
