package org.cmg.ml.sam.xtext.prism.util;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.Map;
import org.cmg.ml.sam.xtext.prism.prism.Expression;
import org.cmg.ml.sam.xtext.prism.util.RenamedElement;
import org.cmg.ml.sam.xtext.prism.util.UpdateStructure;
import org.eclipse.xtext.util.Wrapper;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure2;

@SuppressWarnings("all")
public class RuleStructure {
  private String action;
  
  private int index;
  
  private Iterable<RenamedElement<Expression>> guard;
  
  private Iterable<UpdateStructure> commands;
  
  public RuleStructure(final Iterable<RenamedElement<Expression>> guard, final Iterable<UpdateStructure> commands) {
    this(null, guard, commands);
  }
  
  public RuleStructure(final String action, final Iterable<RenamedElement<Expression>> guard, final Iterable<UpdateStructure> commands) {
    this.action = action;
    this.guard = guard;
    this.commands = commands;
  }
  
  public String getAction() {
    return this.action;
  }
  
  public Iterable<RenamedElement<Expression>> getGuard() {
    return this.guard;
  }
  
  public Iterable<UpdateStructure> getCommands() {
    return this.commands;
  }
  
  public int getIndex() {
    return this.index;
  }
  
  public int setIndex(final int i) {
    return this.index = i;
  }
  
  public RuleStructure rename(final Map<String, String> map) {
    RuleStructure _xblockexpression = null;
    {
      String newAct = this.action;
      boolean _notEquals = (!Objects.equal(this.action, null));
      if (_notEquals) {
        String _get = map.get(this.action);
        newAct = _get;
        boolean _equals = Objects.equal(newAct, null);
        if (_equals) {
          newAct = this.action;
        }
      }
      final Function1<RenamedElement<Expression>, RenamedElement<Expression>> _function = new Function1<RenamedElement<Expression>, RenamedElement<Expression>>() {
        @Override
        public RenamedElement<Expression> apply(final RenamedElement<Expression> e) {
          return e.apply(map);
        }
      };
      Iterable<RenamedElement<Expression>> _map = IterableExtensions.<RenamedElement<Expression>, RenamedElement<Expression>>map(this.guard, _function);
      final Function1<UpdateStructure, UpdateStructure> _function_1 = new Function1<UpdateStructure, UpdateStructure>() {
        @Override
        public UpdateStructure apply(final UpdateStructure u) {
          return u.rename(map);
        }
      };
      Iterable<UpdateStructure> _map_1 = IterableExtensions.<UpdateStructure, UpdateStructure>map(this.commands, _function_1);
      _xblockexpression = new RuleStructure(this.action, _map, _map_1);
    }
    return _xblockexpression;
  }
  
  public RuleStructure combine(final RuleStructure r) {
    RuleStructure _xifexpression = null;
    boolean _or = false;
    boolean _notEquals = (!Objects.equal(this.action, r.action));
    if (_notEquals) {
      _or = true;
    } else {
      boolean _equals = this.action.equals(r.action);
      boolean _not = (!_equals);
      _or = _not;
    }
    if (_or) {
      _xifexpression = null;
    } else {
      Iterable<RenamedElement<Expression>> _plus = Iterables.<RenamedElement<Expression>>concat(this.guard, r.guard);
      Iterable<UpdateStructure> _plus_1 = Iterables.<UpdateStructure>concat(this.commands, r.commands);
      _xifexpression = new RuleStructure(this.action, _plus, _plus_1);
    }
    return _xifexpression;
  }
  
  public RuleStructure hide(final Iterable<String> actions) {
    RuleStructure _xifexpression = null;
    boolean _and = false;
    boolean _notEquals = (!Objects.equal(this.action, null));
    if (!_notEquals) {
      _and = false;
    } else {
      final Function1<String, Boolean> _function = new Function1<String, Boolean>() {
        @Override
        public Boolean apply(final String x) {
          return Boolean.valueOf(RuleStructure.this.action.equals(x));
        }
      };
      boolean _exists = IterableExtensions.<String>exists(actions, _function);
      _and = _exists;
    }
    if (_and) {
      _xifexpression = new RuleStructure(this.guard, this.commands);
    } else {
      _xifexpression = this;
    }
    return _xifexpression;
  }
  
  public void generateIndexes() {
    final Procedure2<UpdateStructure, Integer> _function = new Procedure2<UpdateStructure, Integer>() {
      @Override
      public void apply(final UpdateStructure us, final Integer i) {
        us.setIndex(i);
      }
    };
    IterableExtensions.<UpdateStructure>forEach(this.commands, _function);
  }
  
  @Override
  public String toString() {
    final Function1<RenamedElement<Expression>, String> _function = new Function1<RenamedElement<Expression>, String>() {
      @Override
      public String apply(final RenamedElement<Expression> g) {
        return g.toString();
      }
    };
    Iterable<String> _map = IterableExtensions.<RenamedElement<Expression>, String>map(this.guard, _function);
    final Function2<String, String, String> _function_1 = new Function2<String, String, String>() {
      @Override
      public String apply(final String x, final String y) {
        return ((x + " , ") + y);
      }
    };
    String _reduce = IterableExtensions.<String>reduce(_map, _function_1);
    String _plus = ((("[" + this.action) + "]") + _reduce);
    String _plus_1 = (_plus + " -> ");
    final Function1<UpdateStructure, String> _function_2 = new Function1<UpdateStructure, String>() {
      @Override
      public String apply(final UpdateStructure c) {
        return c.toString();
      }
    };
    Iterable<String> _map_1 = IterableExtensions.<UpdateStructure, String>map(this.commands, _function_2);
    final Function2<String, String, String> _function_3 = new Function2<String, String, String>() {
      @Override
      public String apply(final String x, final String y) {
        return ((x + " , ") + y);
      }
    };
    String _reduce_1 = IterableExtensions.<String>reduce(_map_1, _function_3);
    return (_plus_1 + _reduce_1);
  }
  
  public Iterable<Pair<Integer, UpdateStructure>> getIndexedCommands() {
    Iterable<Pair<Integer, UpdateStructure>> _xblockexpression = null;
    {
      final Wrapper<Integer> i = Wrapper.<Integer>wrap(Integer.valueOf(0));
      final Function1<UpdateStructure, Pair<Integer, UpdateStructure>> _function = new Function1<UpdateStructure, Pair<Integer, UpdateStructure>>() {
        @Override
        public Pair<Integer, UpdateStructure> apply(final UpdateStructure r) {
          Pair<Integer, UpdateStructure> _xblockexpression = null;
          {
            Integer c = i.get();
            i.set(Integer.valueOf(((c).intValue() + 1)));
            _xblockexpression = Pair.<Integer, UpdateStructure>of(c, r);
          }
          return _xblockexpression;
        }
      };
      _xblockexpression = IterableExtensions.<UpdateStructure, Pair<Integer, UpdateStructure>>map(this.commands, _function);
    }
    return _xblockexpression;
  }
}
