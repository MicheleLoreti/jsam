package org.cmg.ml.sam.xtext.prism.util;

import com.google.common.base.Objects;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import org.cmg.ml.sam.xtext.prism.prism.BooleanType;
import org.cmg.ml.sam.xtext.prism.prism.Constant;
import org.cmg.ml.sam.xtext.prism.prism.ConstantType;
import org.cmg.ml.sam.xtext.prism.prism.Expression;
import org.cmg.ml.sam.xtext.prism.prism.Formula;
import org.cmg.ml.sam.xtext.prism.prism.IntervalType;
import org.cmg.ml.sam.xtext.prism.prism.SymbolRenaming;
import org.cmg.ml.sam.xtext.prism.prism.Type;
import org.cmg.ml.sam.xtext.prism.prism.Variable;
import org.cmg.ml.sam.xtext.prism.util.RenamedElement;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Pair;

@SuppressWarnings("all")
public class SymbolTable {
  private HashMap<String, Type> globalVariables = new HashMap<String, Type>();
  
  private HashMap<String, Type> localVariables = new HashMap<String, Type>();
  
  private HashMap<String, ConstantType> constants = new HashMap<String, ConstantType>();
  
  private HashSet<String> actions = new HashSet<String>();
  
  private HashSet<String> functions = new HashSet<String>();
  
  private HashMap<String, Pair<HashMap<String, Type>, HashSet<String>>> modules = new HashMap<String, Pair<HashMap<String, Type>, HashSet<String>>>();
  
  private HashSet<String> duplicated = new HashSet<String>();
  
  private HashMap<String, RenamedElement<Expression>> init = new HashMap<String, RenamedElement<Expression>>();
  
  public boolean isAGlobalVariable(final String id) {
    Type _get = this.globalVariables.get(id);
    return (!Objects.equal(_get, null));
  }
  
  public boolean isAnAction(final String id) {
    return this.actions.contains(id);
  }
  
  public boolean isAFunction(final String id) {
    return this.functions.contains(id);
  }
  
  public boolean isALocalVariable(final String id) {
    Type _get = this.localVariables.get(id);
    return (!Objects.equal(_get, null));
  }
  
  public boolean isConstant(final String id) {
    ConstantType _get = this.constants.get(id);
    return (!Objects.equal(_get, null));
  }
  
  public boolean isDefined(final String id) {
    boolean _or = false;
    boolean _or_1 = false;
    boolean _or_2 = false;
    boolean _or_3 = false;
    boolean _isAFunction = this.isAFunction(id);
    if (_isAFunction) {
      _or_3 = true;
    } else {
      boolean _isAGlobalVariable = this.isAGlobalVariable(id);
      _or_3 = _isAGlobalVariable;
    }
    if (_or_3) {
      _or_2 = true;
    } else {
      boolean _isALocalVariable = this.isALocalVariable(id);
      _or_2 = _isALocalVariable;
    }
    if (_or_2) {
      _or_1 = true;
    } else {
      boolean _isAnAction = this.isAnAction(id);
      _or_1 = _isAnAction;
    }
    if (_or_1) {
      _or = true;
    } else {
      boolean _isConstant = this.isConstant(id);
      _or = _isConstant;
    }
    return _or;
  }
  
  public RenamedElement<Expression> registerGlobalVariable(final Variable v) {
    RenamedElement<Expression> _xblockexpression = null;
    {
      String _name = v.getName();
      boolean _isDefined = this.isDefined(_name);
      if (_isDefined) {
        String _name_1 = v.getName();
        this.duplicated.add(_name_1);
      }
      String _name_2 = v.getName();
      Type _type = v.getType();
      this.globalVariables.put(_name_2, _type);
      Expression foo = this.getInitExpression(v);
      RenamedElement<Expression> _xifexpression = null;
      boolean _notEquals = (!Objects.equal(foo, null));
      if (_notEquals) {
        String _name_3 = v.getName();
        RenamedElement<Expression> _renamedElement = new RenamedElement<Expression>(foo);
        _xifexpression = this.init.put(_name_3, _renamedElement);
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public RenamedElement<Expression> registerLocalVariable(final Variable v) {
    RenamedElement<Expression> _xblockexpression = null;
    {
      String _name = v.getName();
      Type _type = v.getType();
      this.registerLocalVariable(_name, _type);
      Expression foo = this.getInitExpression(v);
      RenamedElement<Expression> _xifexpression = null;
      boolean _notEquals = (!Objects.equal(foo, null));
      if (_notEquals) {
        String _name_1 = v.getName();
        RenamedElement<Expression> _renamedElement = new RenamedElement<Expression>(foo);
        _xifexpression = this.init.put(_name_1, _renamedElement);
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public Type registerLocalVariable(final String name, final Type type) {
    Type _xblockexpression = null;
    {
      boolean _isDefined = this.isDefined(name);
      if (_isDefined) {
        this.duplicated.add(name);
      }
      _xblockexpression = this.localVariables.put(name, type);
    }
    return _xblockexpression;
  }
  
  public boolean registerAction(final String a) {
    boolean _xifexpression = false;
    boolean _contains = this.actions.contains(a);
    boolean _not = (!_contains);
    if (_not) {
      boolean _xblockexpression = false;
      {
        boolean _isDefined = this.isDefined(a);
        if (_isDefined) {
          this.duplicated.add(a);
        }
        _xblockexpression = this.actions.add(a);
      }
      _xifexpression = _xblockexpression;
    }
    return _xifexpression;
  }
  
  public ConstantType registerConstant(final Constant c) {
    ConstantType _xblockexpression = null;
    {
      String _name = c.getName();
      boolean _isDefined = this.isDefined(_name);
      if (_isDefined) {
        String _name_1 = c.getName();
        this.duplicated.add(_name_1);
      }
      String _name_2 = c.getName();
      ConstantType _type = c.getType();
      _xblockexpression = this.constants.put(_name_2, _type);
    }
    return _xblockexpression;
  }
  
  public boolean registerFormula(final Formula f) {
    boolean _xblockexpression = false;
    {
      String _name = f.getName();
      boolean _isDefined = this.isDefined(_name);
      if (_isDefined) {
        String _name_1 = f.getName();
        this.duplicated.add(_name_1);
      }
      String _name_2 = f.getName();
      _xblockexpression = this.functions.add(_name_2);
    }
    return _xblockexpression;
  }
  
  public boolean isModuleRegistered(final String name) {
    return this.modules.containsKey(name);
  }
  
  public Pair<HashMap<String, Type>, HashSet<String>> registerModule(final String name, final Iterable<Variable> variables, final Iterable<String> actions) {
    Pair<HashMap<String, Type>, HashSet<String>> _xblockexpression = null;
    {
      HashMap<String, Type> _hashMap = new HashMap<String, Type>();
      HashSet<String> _hashSet = new HashSet<String>();
      final Pair<HashMap<String, Type>, HashSet<String>> data = Pair.<HashMap<String, Type>, HashSet<String>>of(_hashMap, _hashSet);
      final Consumer<Variable> _function = new Consumer<Variable>() {
        @Override
        public void accept(final Variable v) {
          SymbolTable.this.registerLocalVariable(v);
          HashMap<String, Type> _key = data.getKey();
          String _name = v.getName();
          Type _type = v.getType();
          _key.put(_name, _type);
        }
      };
      variables.forEach(_function);
      final Consumer<String> _function_1 = new Consumer<String>() {
        @Override
        public void accept(final String a) {
          SymbolTable.this.registerAction(a);
          HashSet<String> _value = data.getValue();
          _value.add(a);
        }
      };
      actions.forEach(_function_1);
      _xblockexpression = this.modules.put(name, data);
    }
    return _xblockexpression;
  }
  
  public void registerModule(final String name, final String src, final Iterable<SymbolRenaming> renaming) {
    final HashMap<String, String> map = new HashMap<String, String>();
    final Consumer<SymbolRenaming> _function = new Consumer<SymbolRenaming>() {
      @Override
      public void accept(final SymbolRenaming f) {
        String _source = f.getSource();
        String _target = f.getTarget();
        map.put(_source, _target);
      }
    };
    renaming.forEach(_function);
    final Pair<HashMap<String, Type>, HashSet<String>> data = this.modules.get(src);
    HashMap<String, Type> _hashMap = new HashMap<String, Type>();
    HashSet<String> _hashSet = new HashSet<String>();
    final Pair<HashMap<String, Type>, HashSet<String>> newData = Pair.<HashMap<String, Type>, HashSet<String>>of(_hashMap, _hashSet);
    boolean _notEquals = (!Objects.equal(data, null));
    if (_notEquals) {
      HashMap<String, Type> _key = data.getKey();
      final BiConsumer<String, Type> _function_1 = new BiConsumer<String, Type>() {
        @Override
        public void accept(final String v, final Type t) {
          String renamed = map.get(v);
          boolean _equals = Objects.equal(renamed, null);
          if (_equals) {
            renamed = v;
          }
          SymbolTable.this.registerLocalVariable(renamed, t);
          RenamedElement<Expression> start = SymbolTable.this.init.get(v);
          boolean _notEquals = (!Objects.equal(start, null));
          if (_notEquals) {
            RenamedElement<Expression> _apply = start.apply(map);
            SymbolTable.this.init.put(renamed, _apply);
          }
          HashMap<String, Type> _key = newData.getKey();
          _key.put(renamed, t);
        }
      };
      _key.forEach(_function_1);
      HashSet<String> _value = data.getValue();
      final Consumer<String> _function_2 = new Consumer<String>() {
        @Override
        public void accept(final String a) {
          String renamed = map.get(a);
          boolean _equals = Objects.equal(renamed, null);
          if (_equals) {
            renamed = a;
          }
          boolean _contains = SymbolTable.this.actions.contains(renamed);
          boolean _not = (!_contains);
          if (_not) {
            SymbolTable.this.registerAction(renamed);
          }
          HashSet<String> _value = newData.getValue();
          _value.add(renamed);
        }
      };
      _value.forEach(_function_2);
    }
  }
  
  public CharSequence getExpressionCode(final String id, final boolean isBoolean) {
    CharSequence _xifexpression = null;
    boolean _isAGlobalVariable = this.isAGlobalVariable(id);
    if (_isAGlobalVariable) {
      CharSequence _globalVariableCode = this.getGlobalVariableCode(id);
      Type _get = this.globalVariables.get(id);
      _xifexpression = this.getVariableExpression(_globalVariableCode, _get, isBoolean);
    } else {
      CharSequence _xifexpression_1 = null;
      boolean _isALocalVariable = this.isALocalVariable(id);
      if (_isALocalVariable) {
        CharSequence _localVariableCode = this.getLocalVariableCode(id);
        Type _get_1 = this.localVariables.get(id);
        _xifexpression_1 = this.getVariableExpression(_localVariableCode, _get_1, isBoolean);
      } else {
        CharSequence _xifexpression_2 = null;
        boolean _isConstant = this.isConstant(id);
        if (_isConstant) {
          _xifexpression_2 = this.getConstantCode(id);
        } else {
          CharSequence _xifexpression_3 = null;
          boolean _isAFunction = this.isAFunction(id);
          if (_isAFunction) {
            StringConcatenation _builder = new StringConcatenation();
            CharSequence _formulaCode = this.getFormulaCode(id);
            _builder.append(_formulaCode, "");
            _builder.append("(currentState)");
            _xifexpression_3 = _builder;
          } else {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("ERROR");
            _xifexpression_3 = _builder_1;
          }
          _xifexpression_2 = _xifexpression_3;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public CharSequence getGlobalVariableCode(final String id) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("_GLOBAL_");
    _builder.append(id, "");
    return _builder;
  }
  
  public CharSequence getLocalVariableCode(final String id) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("_LOCAL_");
    _builder.append(id, "");
    return _builder;
  }
  
  public CharSequence getActionCode(final String id) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("_ACTION_");
    _builder.append(id, "");
    return _builder;
  }
  
  public CharSequence getConstantCode(final String id) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("_CONST_");
    _builder.append(id, "");
    return _builder;
  }
  
  public CharSequence getFormulaCode(final String id) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("_FORMULA_");
    _builder.append(id, "");
    return _builder;
  }
  
  public CharSequence getVariableCode(final String id) {
    CharSequence _xifexpression = null;
    boolean _containsKey = this.globalVariables.containsKey(id);
    if (_containsKey) {
      _xifexpression = this.getGlobalVariableCode(id);
    } else {
      _xifexpression = this.getLocalVariableCode(id);
    }
    return _xifexpression;
  }
  
  public CharSequence getVariableExpression(final CharSequence evaluatingCode, final Type type, final boolean isBoolean) {
    CharSequence _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (type instanceof BooleanType) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(enumerator.get(currentState,");
        _builder.append(evaluatingCode, "");
        _builder.append(")");
        {
          if (isBoolean) {
            _builder.append("==1");
          }
        }
        _builder.append(")");
        _switchResult = _builder;
      }
    }
    if (!_matched) {
      if (type instanceof IntervalType) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("enumerator.get(currentState,");
        _builder.append(evaluatingCode, "");
        _builder.append(")");
        _switchResult = _builder;
      }
    }
    return _switchResult;
  }
  
  public HashSet<String> getActions() {
    return this.actions;
  }
  
  public Expression getInitExpression(final Variable v) {
    Expression _xifexpression = null;
    Expression _init = v.getInit();
    boolean _notEquals = (!Objects.equal(_init, null));
    if (_notEquals) {
      _xifexpression = v.getInit();
    } else {
      Expression _xblockexpression = null;
      {
        Type t = v.getType();
        Expression _switchResult = null;
        boolean _matched = false;
        if (!_matched) {
          if (t instanceof IntervalType) {
            _matched=true;
            _switchResult = ((IntervalType)t).getMin();
          }
        }
        if (!_matched) {
          if (t instanceof BooleanType) {
            _matched=true;
            _switchResult = null;
          }
        }
        _xblockexpression = _switchResult;
      }
      _xifexpression = _xblockexpression;
    }
    return _xifexpression;
  }
  
  public int getNumberOfVariables() {
    int _size = this.globalVariables.size();
    int _size_1 = this.localVariables.size();
    return (_size + _size_1);
  }
  
  public Set<String> getGlobalVariables() {
    return this.globalVariables.keySet();
  }
  
  public Set<String> getLocalVariables() {
    return this.localVariables.keySet();
  }
  
  public RenamedElement<Expression> getInitExpression(final String id) {
    RenamedElement<Expression> _xblockexpression = null;
    {
      RenamedElement<Expression> v = this.init.get(id);
      _xblockexpression = v;
    }
    return _xblockexpression;
  }
  
  public Type getTypeOf(final String id) {
    Type _xblockexpression = null;
    {
      Type result = this.globalVariables.get(id);
      Type _xifexpression = null;
      boolean _notEquals = (!Objects.equal(result, null));
      if (_notEquals) {
        _xifexpression = result;
      } else {
        _xifexpression = this.localVariables.get(id);
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
}
