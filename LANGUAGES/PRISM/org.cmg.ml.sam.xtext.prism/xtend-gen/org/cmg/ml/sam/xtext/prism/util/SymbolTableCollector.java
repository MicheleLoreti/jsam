/**
 * Michele Loreti, Concurrency and Mobility Group
 * Universit� di Firenze, Italy
 * (C) Copyright 2013.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Michele Loreti
 */
package org.cmg.ml.sam.xtext.prism.util;

import com.google.common.base.Objects;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.cmg.ml.sam.xtext.prism.prism.Command;
import org.cmg.ml.sam.xtext.prism.prism.Constant;
import org.cmg.ml.sam.xtext.prism.prism.Element;
import org.cmg.ml.sam.xtext.prism.prism.Formula;
import org.cmg.ml.sam.xtext.prism.prism.Model;
import org.cmg.ml.sam.xtext.prism.prism.Module;
import org.cmg.ml.sam.xtext.prism.prism.ModuleBody;
import org.cmg.ml.sam.xtext.prism.prism.ModuleBodyDeclaration;
import org.cmg.ml.sam.xtext.prism.prism.SymbolRenaming;
import org.cmg.ml.sam.xtext.prism.prism.Variable;
import org.cmg.ml.sam.xtext.prism.prism.VariableRenaming;
import org.cmg.ml.sam.xtext.prism.util.SymbolTable;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class SymbolTableCollector {
  private HashMap<String, Boolean> moduleGenerationTable = new HashMap<String, Boolean>();
  
  public SymbolTable collecSymbols(final Model model) {
    SymbolTable _xblockexpression = null;
    {
      SymbolTable st = new SymbolTable();
      EList<Element> _elements = model.getElements();
      for (final Element e : _elements) {
        this.populateSymbolTable(e, st);
      }
      _xblockexpression = st;
    }
    return _xblockexpression;
  }
  
  protected SymbolTable _populateSymbolTable(final Element e, final SymbolTable st) {
    return st;
  }
  
  protected SymbolTable _populateSymbolTable(final Variable v, final SymbolTable st) {
    SymbolTable _xblockexpression = null;
    {
      st.registerGlobalVariable(v);
      _xblockexpression = st;
    }
    return _xblockexpression;
  }
  
  protected SymbolTable _populateSymbolTable(final Constant c, final SymbolTable st) {
    SymbolTable _xblockexpression = null;
    {
      st.registerConstant(c);
      _xblockexpression = st;
    }
    return _xblockexpression;
  }
  
  protected SymbolTable _populateSymbolTable(final Formula f, final SymbolTable st) {
    SymbolTable _xblockexpression = null;
    {
      st.registerFormula(f);
      _xblockexpression = st;
    }
    return _xblockexpression;
  }
  
  protected SymbolTable _populateSymbolTable(final Module m, final SymbolTable st) {
    ModuleBody _body = m.getBody();
    String _name = m.getName();
    return this.populateSymbolTable(_body, _name, st);
  }
  
  protected SymbolTable _populateSymbolTable(final ModuleBodyDeclaration m, final String name, final SymbolTable st) {
    SymbolTable _xblockexpression = null;
    {
      EList<Variable> _variables = m.getVariables();
      EList<Command> _commands = m.getCommands();
      final Function1<Command, String> _function = new Function1<Command, String>() {
        @Override
        public String apply(final Command c) {
          return c.getAct();
        }
      };
      List<String> _map = ListExtensions.<Command, String>map(_commands, _function);
      Iterable<String> _filterNull = IterableExtensions.<String>filterNull(_map);
      st.registerModule(name, _variables, _filterNull);
      this.moduleGenerationTable.put(name, Boolean.valueOf(true));
      _xblockexpression = st;
    }
    return _xblockexpression;
  }
  
  protected SymbolTable _populateSymbolTable(final VariableRenaming m, final String name, final SymbolTable st) {
    SymbolTable _xblockexpression = null;
    {
      Module renamed = m.getModule();
      String _name = renamed.getName();
      Boolean _get = this.moduleGenerationTable.get(_name);
      boolean _equals = Objects.equal(_get, null);
      if (_equals) {
        this.moduleGenerationTable.put(name, Boolean.valueOf(false));
        this.populateSymbolTable(renamed, st);
      }
      String _name_1 = renamed.getName();
      EList<SymbolRenaming> _renaming = m.getRenaming();
      st.registerModule(name, _name_1, _renaming);
      _xblockexpression = st;
    }
    return _xblockexpression;
  }
  
  public HashMap<String, String> getRenamingFunction(final Iterable<SymbolRenaming> renaming) {
    HashMap<String, String> _xblockexpression = null;
    {
      HashMap<String, String> map = new HashMap<String, String>();
      for (final SymbolRenaming r : renaming) {
        String _source = r.getSource();
        String _target = r.getTarget();
        map.put(_source, _target);
      }
      _xblockexpression = map;
    }
    return _xblockexpression;
  }
  
  public SymbolTable populateSymbolTable(final Element c, final SymbolTable st) {
    if (c instanceof Constant) {
      return _populateSymbolTable((Constant)c, st);
    } else if (c instanceof Formula) {
      return _populateSymbolTable((Formula)c, st);
    } else if (c instanceof Module) {
      return _populateSymbolTable((Module)c, st);
    } else if (c instanceof Variable) {
      return _populateSymbolTable((Variable)c, st);
    } else if (c != null) {
      return _populateSymbolTable(c, st);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(c, st).toString());
    }
  }
  
  public SymbolTable populateSymbolTable(final ModuleBody m, final String name, final SymbolTable st) {
    if (m instanceof ModuleBodyDeclaration) {
      return _populateSymbolTable((ModuleBodyDeclaration)m, name, st);
    } else if (m instanceof VariableRenaming) {
      return _populateSymbolTable((VariableRenaming)m, name, st);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(m, name, st).toString());
    }
  }
}
