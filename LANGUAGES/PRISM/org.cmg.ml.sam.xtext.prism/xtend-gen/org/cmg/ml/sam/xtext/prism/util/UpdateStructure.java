package org.cmg.ml.sam.xtext.prism.util;

import com.google.common.collect.Iterables;
import java.util.Map;
import org.cmg.ml.sam.xtext.prism.prism.Expression;
import org.cmg.ml.sam.xtext.prism.prism.UpdateElement;
import org.cmg.ml.sam.xtext.prism.util.RenamedElement;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class UpdateStructure {
  private Iterable<RenamedElement<Expression>> weight;
  
  private Iterable<RenamedElement<UpdateElement>> updates;
  
  private int index;
  
  public UpdateStructure(final Iterable<RenamedElement<Expression>> weight, final Iterable<RenamedElement<UpdateElement>> updates) {
    this.weight = weight;
    this.updates = updates;
  }
  
  public UpdateStructure(final Expression weight, final Iterable<UpdateElement> updates) {
    this(CollectionLiterals.<RenamedElement<Expression>>newArrayList(new RenamedElement<Expression>(weight)), 
      IterableExtensions.<UpdateElement, RenamedElement<UpdateElement>>map(updates, new Function1<UpdateElement, RenamedElement<UpdateElement>>() {
        @Override
        public RenamedElement<UpdateElement> apply(final UpdateElement u) {
          return new RenamedElement<UpdateElement>(u);
        }
      }));
  }
  
  public Iterable<RenamedElement<Expression>> getWeight() {
    return this.weight;
  }
  
  public Iterable<RenamedElement<UpdateElement>> getUpdates() {
    return this.updates;
  }
  
  public int getIndex() {
    return this.index;
  }
  
  public int setIndex(final int i) {
    return this.index = i;
  }
  
  public UpdateStructure combine(final UpdateStructure str) {
    Iterable<RenamedElement<Expression>> _plus = Iterables.<RenamedElement<Expression>>concat(this.weight, str.weight);
    Iterable<RenamedElement<UpdateElement>> _plus_1 = Iterables.<RenamedElement<UpdateElement>>concat(this.updates, str.updates);
    return new UpdateStructure(_plus, _plus_1);
  }
  
  public UpdateStructure rename(final Map<String, String> map) {
    final Function1<RenamedElement<Expression>, RenamedElement<Expression>> _function = new Function1<RenamedElement<Expression>, RenamedElement<Expression>>() {
      @Override
      public RenamedElement<Expression> apply(final RenamedElement<Expression> e) {
        return e.apply(map);
      }
    };
    Iterable<RenamedElement<Expression>> _map = IterableExtensions.<RenamedElement<Expression>, RenamedElement<Expression>>map(this.weight, _function);
    final Function1<RenamedElement<UpdateElement>, RenamedElement<UpdateElement>> _function_1 = new Function1<RenamedElement<UpdateElement>, RenamedElement<UpdateElement>>() {
      @Override
      public RenamedElement<UpdateElement> apply(final RenamedElement<UpdateElement> u) {
        return u.apply(map);
      }
    };
    Iterable<RenamedElement<UpdateElement>> _map_1 = IterableExtensions.<RenamedElement<UpdateElement>, RenamedElement<UpdateElement>>map(this.updates, _function_1);
    return new UpdateStructure(_map, _map_1);
  }
}
