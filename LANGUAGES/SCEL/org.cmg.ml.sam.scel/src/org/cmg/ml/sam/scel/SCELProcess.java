/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel;

import org.cmg.ml.sam.scel.commands.SCELCommand;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public class SCELProcess {

	protected SCELCommand command;
	protected String name;
	
	public SCELProcess( String name ) {
		this.name = name;
	}
	
	public WeightedStructure<Activity> getActivities(SCELSystem system ,SCELComponent component, SCELRateFunction rateFunction) {
		if (command == null) {
			return null;
		}
		return command.getActivities( system , component, this, rateFunction ); 
	}


	public void setCommand(SCELCommand command) {
		this.command = command;
	}
	
	public boolean completed() {
		return command == null;
	}

}
