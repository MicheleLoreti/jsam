/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel;

import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.scel.comp.SCELPredicate;
import org.cmg.ml.sam.sim.ds.Template;
import org.cmg.ml.sam.sim.ds.Tuple;

/**
 * @author loreti
 *
 */
public interface SCELRateFunction {
	
	public double getGetRate( SCELComponent src , Template template , String id );

	public double getGetRate( SCELComponent src , Template template , SCELPredicate predicate );

	public double getGetRate( SCELComponent src , Template template );

	public double getQueryRate( SCELComponent src , Template template , String id );

	public double getQueryRate( SCELComponent src , Template template , SCELPredicate predicate );

	public double getQueryRate( SCELComponent src , Template template );
	
	public double getPutRate( SCELComponent src , Tuple tuple , String id );

	public double getPutRate( SCELComponent src , Tuple tuple , SCELPredicate predicate );

	public double getPutRate( SCELComponent src , Tuple tuple );
	
}
