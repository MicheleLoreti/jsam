/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel;

import org.cmg.ml.sam.scel.comp.SCELActuator;
import org.cmg.ml.sam.scel.comp.SCELSensor;

/**
 * @author loreti
 *
 */
public interface SCELScenario {

	public void timeStep(double dt);
	
	public SCELSensor getSensor( String id , String name );
	
	public SCELActuator getActuator( String id , String name ); 
	
}
