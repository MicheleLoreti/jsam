/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel;

import java.util.LinkedList;

import org.apache.commons.math3.random.RandomGenerator;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.scel.comp.SCELPredicate;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.ModelI;
import org.cmg.ml.sam.sim.ds.GetActivity;
import org.cmg.ml.sam.sim.ds.Template;
import org.cmg.ml.sam.sim.ds.Tuple;
import org.cmg.ml.sam.sim.util.ComposedWeightedStructure;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public class SCELSystem implements ModelI {

	private SCELRateFunction rateFunction;
	private LinkedList<SCELComponent> components;
	private SCELScenario	scenario;

	
	public SCELSystem( SCELRateFunction rateFunction , SCELComponent ... components ) {
		this.rateFunction = rateFunction;
		this.components = new LinkedList<SCELComponent>();
		for (int i = 0; i < components.length; i++) {
			this.components.add(components[i]);
		}
	}
	
	public void gPut( SCELComponent src , Tuple t , SCELPredicate p ) {
		for (SCELComponent trg : components) {
			if ((src != trg)&&(p.sat(trg))) {
				trg.put(t);
			}
		}		
	}

	public Tuple gGet( RandomGenerator r , SCELComponent src , Template t , SCELPredicate p ) {
		WeightedStructure<GetActivity> enabled = new ComposedWeightedStructure<GetActivity>();
		for (SCELComponent scelComponent : components) {
			if ((src != scelComponent)&&(p.sat(scelComponent))) {
				enabled.add(scelComponent.get(t));
			}
		}
		double total = enabled.getTotalWeight();
		if (total == 0.0) {
			return null;
		}
		GetActivity act = enabled.select(total*r.nextDouble()).getElement();
		if (act.execute(r)) {
			return act.getTuple();
		}
		return null;
	}

	public Tuple gQuery( RandomGenerator r , SCELComponent src , Template t , SCELPredicate p ) {
		WeightedStructure<Tuple> enabled = new ComposedWeightedStructure<Tuple>();
		for (SCELComponent scelComponent : components) {
			if ((src != scelComponent)&&(p.sat(scelComponent))) {
				enabled.add(scelComponent.query(t));
			}
		}
		double total = enabled.getTotalWeight();
		if (total == 0.0) {
			return null;
		}
		return enabled.select(total*r.nextDouble()).getElement();
	}

	public boolean put( RandomGenerator r , SCELComponent src , Tuple t , String id ) {
		WeightedStructure<SCELComponent> select = new ComposedWeightedStructure<SCELComponent>();
 		for (SCELComponent trg : components) {
			if ((src != trg)&&(trg.getId().equals(id))) {
				trg.put(t);
			}
		}		
 		double total = select.getTotalWeight();
 		if ( total == 0.0 ) {
 			return false;
 		}
 		return select.select(total*r.nextDouble()).getElement().put(t);
	}

	public Tuple query( RandomGenerator r , SCELComponent src , Template t , String id ) {
		WeightedStructure<Tuple> enabled = new ComposedWeightedStructure<Tuple>();
		for (SCELComponent scelComponent : components) {
			if ((src != scelComponent)&&(scelComponent.getId().equals(id))) {
				enabled.add(scelComponent.query(t));
			}
		}
		double total = enabled.getTotalWeight();
		if (total == 0.0) {
			return null;
		}
		return enabled.select(total*r.nextDouble()).getElement();
	}

	public Tuple get( RandomGenerator r , SCELComponent src , Template t , String id ) {
		WeightedStructure<GetActivity> enabled = new ComposedWeightedStructure<GetActivity>();
		for (SCELComponent scelComponent : components) {
			if ((src != scelComponent)&&(scelComponent.getId().equals(id))) {
				enabled.add(scelComponent.get(t));
			}
		}
		double total = enabled.getTotalWeight();
		if (total == 0.0) {
			return null;
		}
		GetActivity act = enabled.select(total*r.nextDouble()).getElement();
		if (act.execute(r)) {
			return act.getTuple();
		}
		return null;
	}
	
	@Override
	public WeightedStructure<Activity> getActivities() {
		WeightedStructure<Activity> activities = new ComposedWeightedStructure<Activity>();
		for (SCELComponent scelComponent : components) {
			activities.add(scelComponent.getActivities( this , rateFunction ));
		}
		return activities;
	}

	@Override
	public void timeStep(double dt) {
		if (scenario != null) {
			this.scenario.timeStep( dt );
		}
	}

}
