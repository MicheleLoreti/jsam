/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public abstract class SCELAssignment extends SCELCommand {

	@Override
	protected WeightedStructure<Activity> getCommandActivities(
			SCELSystem system, SCELComponent component, SCELProcess process,
			SCELRateFunction rateFunction) {
		updateStore();
		return getActivitiesOfContinuation(system, component, process, rateFunction,true);
	}

	protected abstract void updateStore();

}
