/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public class SCELCall extends SCELCommand {
	
	private SCELProcess process;

	public SCELCall( SCELProcess process ) {
		this.process = process;
	}

	@Override
	protected WeightedStructure<Activity> getCommandActivities(
			SCELSystem system, SCELComponent component, SCELProcess process,
			SCELRateFunction rateFunction) {
		if (this.process.completed()) {
			return getActivitiesOfContinuation(system, component, process, rateFunction,true);
		} else {
			return this.process.getActivities(system, component, rateFunction);
		}
	}

}
