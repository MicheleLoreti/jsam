/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import java.util.LinkedList;

import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.util.ComposedWeightedStructure;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public class SCELChoice extends SCELCommand {

	
	private LinkedList<SCELCommand> commands;
	
	public SCELChoice() {
		this.commands = new LinkedList<SCELCommand>();
	}
	
	public void addBranch( SCELCommand command ) {
		this.commands.add(command);
		command.setContinuation(continuation);
	}
	
	
	@Override
	protected WeightedStructure<Activity> getCommandActivities(
			SCELSystem system, SCELComponent component, SCELProcess process,
			SCELRateFunction rateFunction) {
		if (this.commands.isEmpty()) {
			return getActivitiesOfContinuation(system, component, process, rateFunction,true);
		}
		WeightedStructure<Activity> toReturn = new ComposedWeightedStructure<Activity>();
		for (SCELCommand scelCommand : commands) {
			toReturn = toReturn.add(scelCommand.getActivities(system, component, process, rateFunction));
		}
		return toReturn;
	}

	/* (non-Javadoc)
	 * @see org.cmg.ml.sam.scel.SCELCommand#setContinuation(org.cmg.ml.sam.scel.SCELCommand)
	 */
	@Override
	public void setContinuation(SCELCommand continuation) {
		super.setContinuation(continuation);
		for (SCELCommand scelCommand : commands) {
			scelCommand.setContinuation(continuation);
		}		
	}

}
