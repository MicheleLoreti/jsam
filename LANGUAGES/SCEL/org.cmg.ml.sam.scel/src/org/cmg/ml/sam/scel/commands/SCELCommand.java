/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.util.ComposedWeightedStructure;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public abstract class SCELCommand {

	protected SCELCommand continuation;
	
	protected abstract WeightedStructure<Activity> getCommandActivities(SCELSystem system, SCELComponent component, SCELProcess process, SCELRateFunction rateFunction);
	
	public final WeightedStructure<Activity> getActivities(SCELSystem system , SCELComponent component, SCELProcess process, SCELRateFunction rateFunction) {
		if (process == null) {
			return null;
		}
		return getCommandActivities( system, component, process, rateFunction );
	}

	
	public void setContinuation( SCELCommand continuation ) {
		this.continuation = continuation;
	}
	
	protected WeightedStructure<Activity> getActivitiesOfContinuation(SCELSystem system , SCELComponent component, SCELProcess process, SCELRateFunction rateFunction, boolean updateCommand ) {
		if (updateCommand) {
			process.setCommand(continuation);
		}
		if (continuation == null) {
			return new ComposedWeightedStructure<Activity>();
		}
		return continuation.getActivities(system,component, process, rateFunction);
	}
	
}
