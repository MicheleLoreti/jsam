/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import org.apache.commons.math3.random.RandomGenerator;
import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.scel.comp.SCELPredicate;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.ds.Template;
import org.cmg.ml.sam.sim.ds.Tuple;
import org.cmg.ml.sam.sim.util.WeightedElement;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public abstract class SCELGroupGetAction extends SCELCommand {

	protected abstract Template getTemplate();
	protected abstract void updateStore( Tuple tuple );
	protected abstract SCELPredicate getPredicate();
	
	/* (non-Javadoc)
	 * @see org.cmg.ml.sam.scel.SCELCommand#getCommandActivities()
	 */
	@Override
	protected WeightedStructure<Activity> getCommandActivities(final SCELSystem system, final SCELComponent component, final SCELProcess process, SCELRateFunction rateFunction) {
		final SCELPredicate target = getPredicate();
		final Template template = getTemplate();
		double weight = rateFunction.getGetRate(component, template, target);
		return new WeightedElement<Activity>(weight, 
				
				new Activity() {

					@Override
					public boolean execute(RandomGenerator r) {
						Tuple t = system.gGet(r, component, template, target);
						if (t != null) {
							updateStore(t);
							process.setCommand( continuation );
						}
						return true;
					}
			

				}				
				
		);
	}
	
}
