/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import org.apache.commons.math3.random.RandomGenerator;
import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.scel.comp.SCELPredicate;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.ds.GetActivity;
import org.cmg.ml.sam.sim.ds.Template;
import org.cmg.ml.sam.sim.ds.Tuple;
import org.cmg.ml.sam.sim.util.WeightedElement;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public abstract class SCELGroupPutAction extends SCELCommand {

	protected abstract Tuple getTuple();
	protected abstract SCELPredicate getPredicate();
	
	/* (non-Javadoc)
	 * @see org.cmg.ml.sam.scel.SCELCommand#getCommandActivities()
	 */
	@Override
	protected WeightedStructure<Activity> getCommandActivities(final SCELSystem system, final SCELComponent component, final SCELProcess process, SCELRateFunction rateFunction) {
		final SCELPredicate target = getPredicate();
		final Tuple tuple = getTuple();
		double weight = rateFunction.getPutRate(component, tuple, target);
		return new WeightedElement<Activity>(weight, 
				
				new Activity() {

					@Override
					public boolean execute(RandomGenerator r) {
						system.gPut(component, tuple, target);
						process.setCommand( continuation );
						return true;
					}
			

				}				
				
		);
	}
	
}
