/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public abstract class SCELIfThenElse extends SCELCommand {

	protected SCELCommand thenBranch;
	protected SCELCommand elseBranch;
	
	protected abstract boolean evalGuard();

	/* (non-Javadoc)
	 * @see org.cmg.ml.sam.scel.SCELCommand#getActivities()
	 */
	@Override
	public WeightedStructure<Activity> getCommandActivities(SCELSystem system, SCELComponent component, SCELProcess process, SCELRateFunction rateFunction) {
		if (evalGuard()) {
			if (thenBranch == null) {
				return getActivitiesOfContinuation(system,component,process, rateFunction,false);
			} else {
				return thenBranch.getCommandActivities(system, component, process, rateFunction);
			}
		} else {
			if (elseBranch == null) {
				return getActivitiesOfContinuation(system,component,process, rateFunction,false);
			} else {
				return elseBranch.getCommandActivities(system, component, process, rateFunction);
			}
		}
	}

	public void setThenBranch( SCELCommand command ) {
		this.thenBranch = command;
		this.thenBranch.setContinuation(continuation);
	}

	public void setElseBranch( SCELCommand command ) {
		this.elseBranch = command;
		this.elseBranch.setContinuation(continuation);
	}
	
	/* (non-Javadoc)
	 * @see org.cmg.ml.sam.scel.SCELCommand#setContinuation(org.cmg.ml.sam.scel.SCELCommand)
	 */
	@Override
	public void setContinuation(SCELCommand continuation) {
		super.setContinuation(this);
		this.thenBranch.setContinuation(continuation);
		this.elseBranch.setContinuation(continuation);
	}
	

	
	
	
}
