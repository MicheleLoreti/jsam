/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import org.apache.commons.math3.random.RandomGenerator;
import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.ds.GetActivity;
import org.cmg.ml.sam.sim.ds.Template;
import org.cmg.ml.sam.sim.ds.Tuple;
import org.cmg.ml.sam.sim.util.WeightedElement;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public abstract class SCELQueryAction extends SCELCommand {

	protected abstract Template getTemplate();
	protected abstract void updateStore( Tuple tuple );
	protected abstract String getTargetId();
	
	/* (non-Javadoc)
	 * @see org.cmg.ml.sam.scel.SCELCommand#getCommandActivities()
	 */
	@Override
	protected WeightedStructure<Activity> getCommandActivities(final SCELSystem system, final SCELComponent component, final SCELProcess process, SCELRateFunction rateFunction) {
		final String id = getTargetId();
		final Template template = getTemplate();
		double weight = 0.0;
		if (id == null) {
			weight = rateFunction.getQueryRate(component, template);
		} else {
			weight = rateFunction.getQueryRate(component, template, id);
		}
		return new WeightedElement<Activity>(weight, 
				
				new Activity() {

					@Override
					public boolean execute(RandomGenerator r) {
						if (id == null) {
							WeightedStructure<Tuple> wt = component.query(template);
							double total = wt.getTotalWeight();
							if (total > 0.0 ) {
								Tuple t = wt.select(total*r.nextDouble()).getElement();
								updateStore(t);
								process.setCommand( continuation );
							}
						} else {
							Tuple t = system.query(r, component, template, id);
							if (t != null) {
								updateStore(t);
								process.setCommand( continuation );
							}
						}
						return true;
					}
			

				}				
				
		);
	}
	
}
