/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.commands;

import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.scel.comp.SCELComponent;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public abstract class SCELWhile extends SCELCommand {

	protected SCELCommand body;
	
	protected abstract boolean evalCondition();

	/* (non-Javadoc)
	 * @see org.cmg.ml.sam.scel.SCELCommand#getActivities()
	 */
	@Override
	public WeightedStructure<Activity> getCommandActivities(SCELSystem system, SCELComponent component, SCELProcess process, SCELRateFunction rateFunction) {
		if (evalCondition()) {
			if (body == null) {
				return null;
			}
			return body.getCommandActivities(system, component, process, rateFunction);
		}
		return getActivitiesOfContinuation(system,component,process, rateFunction,false);
	}
	
	protected void setBody( SCELCommand body ) {
		this.body = body;
		this.body.setContinuation(this);
	}
}
