/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
package org.cmg.ml.sam.scel.comp;

import org.cmg.ml.sam.sim.ds.Template;
import org.cmg.ml.sam.sim.ds.Tuple;

/**
 * @author loreti
 *
 */
public abstract class SCELActuator {
	
	private Template template;
	
	public SCELActuator( Template template ) {
		this.template = template;
	}

	protected abstract boolean doSend( Tuple t );
	
	public boolean send( Tuple t ) {
		if (template.match(t)) {
			return doSend( t );
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public boolean isAccepted( Tuple t ) {
		return template.match(t);
	}
	
}
