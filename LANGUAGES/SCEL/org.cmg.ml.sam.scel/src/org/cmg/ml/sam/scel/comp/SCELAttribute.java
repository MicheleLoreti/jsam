/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.comp;

/**
 * @author loreti
 *
 */
public abstract class SCELAttribute {

	private String name;

	public SCELAttribute( String name ) {
		this.name = name;
	}
	
	public abstract <T> T eval( Class<T> clazz , SCELComponent component );

	public String getName() {
		return name;
	}
	
}
