/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.comp;

import java.util.LinkedList;

import org.cmg.ml.sam.scel.SCELProcess;
import org.cmg.ml.sam.scel.SCELRateFunction;
import org.cmg.ml.sam.scel.SCELSystem;
import org.cmg.ml.sam.sim.Activity;
import org.cmg.ml.sam.sim.ds.GetActivity;
import org.cmg.ml.sam.sim.ds.Template;
import org.cmg.ml.sam.sim.ds.Tuple;
import org.cmg.ml.sam.sim.ds.TupleSpace;
import org.cmg.ml.sam.sim.util.ComposedWeightedStructure;
import org.cmg.ml.sam.sim.util.WeightedElement;
import org.cmg.ml.sam.sim.util.WeightedStructure;

/**
 * @author loreti
 *
 */
public class SCELComponent {
	
	private TupleSpace knowledge; 
	private SCELInterface  componentInterface;
	private LinkedList<SCELSensor> sensors;
	private LinkedList<SCELActuator> actuators;
	private LinkedList<SCELProcess> processes;
	private String id;
	
	public SCELComponent( String id , SCELInterface componentInterface ) {
		this.componentInterface = componentInterface;
		this.componentInterface.setAttribute( new SCELAttribute("id") {
			
			@Override
			public <T> T eval(Class<T> clazz, SCELComponent component) {
				if (clazz.isAssignableFrom(String.class)) {
					return clazz.cast( component.getId() );
				}
				return null;
			}
			
		});
		this.processes = new LinkedList<SCELProcess>();
		this.knowledge = new TupleSpace();
		this.id = id;
	}
	
	protected SCELSensor getSensor( Template t ) {
		for (SCELSensor scelSensor : sensors) {
			if (scelSensor.doHandle(t)) {
				return scelSensor;
			}
		}
		return null;
	}
	
	protected SCELActuator getActuator( Tuple t ) {
		for (SCELActuator scelActuator : actuators) {
			if (scelActuator.isAccepted(t)) {
				return scelActuator;
			}
		}
		return null;
	}
	
	public void addProcess( SCELProcess p ) {
		this.processes.add(p);
	}

	public boolean put( Tuple t ) {
		SCELActuator actuator = getActuator(t);
		if (actuator != null) {
			return actuator.send(t);
		}
		return (knowledge.put(t));
	}
	
	public WeightedStructure<Tuple> query( Template t ) {
		SCELSensor sensor = getSensor( t );
		if (sensor != null) {
			Tuple tuple = sensor.getValue();
			if (tuple != null) {
				return new WeightedElement<Tuple>(1.0, tuple);
			} else {
				return new ComposedWeightedStructure<Tuple>();
			}
		}		
		return knowledge.query(t);
	}
	
	public WeightedStructure<GetActivity> get( Template t ) {
		return knowledge.get(t);				
	}
	
	public String getId() {
		return id;
	}
	
	public <T> T getAttributeValue( String name , Class<T> clazz ) {
		return componentInterface.getAttributeValue( name , clazz , this );
	}

	public WeightedStructure<Activity> getActivities(SCELSystem system,SCELRateFunction rateFunction) {
		WeightedStructure<Activity> activities = new ComposedWeightedStructure<Activity>();
		for (SCELProcess scelProcess : processes) {
			activities.add(scelProcess.getActivities( system,this , rateFunction ));
		}
		return activities;
	}
	
}
