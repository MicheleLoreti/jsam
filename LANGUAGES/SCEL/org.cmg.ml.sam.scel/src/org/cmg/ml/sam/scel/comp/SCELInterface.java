/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.comp;

import java.util.HashMap;

/**
 * @author loreti
 *
 */
public class SCELInterface {

	private HashMap<String,SCELAttribute> attributes;
	
	public SCELInterface( ) {
		this.attributes = new HashMap<String,SCELAttribute>( );
	}
	
	
	public void setAttribute( SCELAttribute attribute ) {
		this.attributes.put(attribute.getName(), attribute);
	}
	
	public <T> T getAttributeValue( String name , Class<T> clazz , SCELComponent c ) {
		SCELAttribute attribute = this.attributes.get(name);
		if (attribute == null) {
			return null;
		}
		return attribute.eval(clazz, c);
	}

}
