/*******************************************************************************
 * Copyright (c) 2015.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michele Loreti (University of Firenze) - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package org.cmg.ml.sam.scel.comp;

import org.cmg.ml.sam.sim.ds.Template;
import org.cmg.ml.sam.sim.ds.Tuple;

/**
 * @author loreti
 *
 */
public abstract class SCELSensor {
	
	protected Template sensorTuple;
	
	protected abstract Tuple getValue();
	
	public boolean doHandle( Template t ) {
		return sensorTuple.implies(t);				
	}
	
	public Tuple getValue( Template t ) {
		if (doHandle(t)) {
			return getValue();
		}
		return null;
	}

}
