/**
 * 
 */
package org.cmg.cml.smuc.examples;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author loreti
 *
 */
public class MainPanel extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MainPanel( ) {
		super("SMUC simulator...");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		init();
		setVisible(true);
	}
	
	private void init() {
		JPanel jp = new JPanel();
		jp.setLayout(new BorderLayout());
		jp.add(new JLabel("Porca Paletta!"),BorderLayout.CENTER);
		this.setContentPane(jp);
		pack();
		System.out.println("Proca paletta!!!!");
	}

	public static void main( String[] argv ) {
		new MainPanel();
		JOptionPane.showMessageDialog(null, "Porca palett!!!!");
	}

}
