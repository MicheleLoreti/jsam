/**
 * 
 */
package org.cmg.cml.smuc.examples;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.Random;

/**
 * @author loreti
 *
 */
public class Scenario {

	private int width;
	private int height;
	
	private int size;
	
	private double communication_range;
	
	private Point2D[] points;
	
	public Scenario( int width , int height , int size , double communication_range ) {
		this.width = width;
		this.height = height;
		this.size = size;
		this.communication_range = communication_range;
	}
	
	
	public void init( Random r ) {
		points = new Point2D.Double[size];
		for( int i=0 ; i<size ; i++ ) {
			points[i] = new Point2D.Double( width*r.nextDouble() , height*r.nextDouble() );
		}
	}
	
	
	public int size() {
		return size;
	}
	
	public Point2D getPoint( int i ) {
		return points[i];
	}
	
	public double getCommunicationRange() {
		return communication_range;
	}


	public int getWidth() {
		return width;
	}


	public int getHeight() {
		return height;
	}
}
